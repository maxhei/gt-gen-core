########################################################################################################################
## Common bazel build configurations
########################################################################################################################
build:core --cxxopt=-std=c++17
build:core --compilation_mode=dbg
build:core --strip=never                     # Never strip debug information
build:core --linkopt=-ldl
build:core --features=treat_warnings_as_errors
# disables warnings (adds -w and -Wno-error to compiler flags) for external dependencies
build:core --per_file_copt=+^external/.*,+.*\.pb\.cc,+^src/google/protobuf/.*@-w,-Wno-error
# disables warnings in gt-gen-core when including external dependency headers by using -isystem instead of -iquote
build:core --features=external_include_paths
build:core --config=default_warnings
build:core --config=strict_warnings

build:default_warnings --cxxopt="-Wall"
build:default_warnings --cxxopt="-Wold-style-cast"
build:default_warnings --cxxopt="-Wformat"

build:strict_warnings --cxxopt="-Wextra"
build:strict_warnings --cxxopt="-Wpedantic"
build:strict_warnings --cxxopt="-Wconversion"
build:strict_warnings --cxxopt="-Wfloat-equal"
build:strict_warnings --cxxopt="-Wformat-security"
# build:strict_warnings --cxxopt="-Wshadow"
# build:strict_warnings --cxxopt="-Wsign-conversion"


########################################################################################################################
# Common bazel test options
########################################################################################################################
test:core --test_output=errors
test:core --build_tests_only
test:core --test_verbose_timeout_warnings
test:core --test_strategy=standalone
test:core --test_env="GTEST_COLOR=1"  # Enable GoogleTest color output
test:core --trim_test_configuration
test:core --keep_going
test:core --define test_build=true

########################################################################################################################
# Release build config
########################################################################################################################
build:core_release --config=core
build:core_release --compilation_mode=opt
build:core_release --strip=always


########################################################################################################################
# Sanitizer build configs
########################################################################################################################
build:clang --cxxopt=-std=c++17
build:clang --experimental_guard_against_concurrent_changes
build:clang --incompatible_enable_cc_toolchain_resolution
build:clang --force_pic
build:clang --features=third_party_warnings
build:clang --features=no_legacy_features
build:clang --features=treat_warnings_as_errors
build:clang --linkopt=-ldl

test:clang  --define test_build=true

build:core_san --config=clang
build:core_san --compilation_mode=dbg
build:core_san --features=sanitizer
build:core_san --copt=-fsanitize=address
build:core_san --copt=-fno-omit-frame-pointer
build:core_san --linkopt=-fsanitize=address
build:core_san --action_env=UBSAN_OPTIONS=verbosity=1:print_stacktrace=1

########################################################################################################################
# clang_tidy build config
########################################################################################################################
build:clang_tidy --config=clang
build:clang_tidy --aspects @bazel_clang_tidy//clang_tidy:clang_tidy.bzl%clang_tidy_aspect
build:clang_tidy --output_groups=report
build:clang_tidy --@bazel_clang_tidy//:clang_tidy_config=//clang_tidy:clang_tidy_config
build:clang_tidy --features=treat_clang_tidy_warnings_as_errors
build:clang_tidy --keep_going
