# Third Party Libraries in use


## Release build dependencies

### asio
- License: Boost Software License - Version 1.0
- Homepage: http://think-async.com/Asio
- Repository: https://github.com/chriskohlhoff/asio/
- Version: 1-24-0


### bazel
- License: Apache-2.0
- Repository: https://github.com/bazelbuild/bazel
- Version: 6.2.1

### bazel_rules
- License: Apache-2.0
- Repository: https://github.com/bazelbuild/rules_pkg
- Version: patch version #TODO check version

### boost (Boost)
- License: Boost License 1.0
- Repository: https://github.com/boostorg/boost/releases/tag/boost-1.71.1
- Version: 1.71.1

### fmt (libfmt)
- License: BSD 2-Clause "Simplified" License
- Homepage: http://fmtlib.net/latest/index.html
- Repository: https://github.com/fmtlib/fmt
- Version: 8.0.1

### Foxglove WebSocket Protocol
- License: MIT license
- Repository: https://github.com/foxglove/ws-protocol
- Version: 1.0.0

### glm
- License: The Happy Bunny License and MIT License
- Version: 0.9.9.7

### googletest (Google Test)
- License: BSD 3-Clause "New" or "Revised" License
- Repository: https://github.com/google/googletest
- Version: 1.10.0


### MantleAPI
- License: Eclipse Public License 2.0
- Repository: https://gitlab.eclipse.org/eclipse/openpass/mantle-api
- Commit: see https://gitlab.eclipse.org/eclipse/openpass/mantle-api/-/tags/v2.1.0

### MapSDK
- License: EPL 2.0
- Repository: https://gitlab.eclipse.org/eclipse/openpass/map-sdk
- Commit: see https://gitlab.eclipse.org/eclipse/openpass/map-sdk/-/tags/v0.1.0

### mINI (Ini file reader and writer)
- License: MIT License
- Repository: https://github.com/pulzed/mINI
- Version: 0.9.14

### nlohmann (JSON for Modern C++)
- License: MIT License
- Homepage: https://nlohmann.github.io/json/
- Repository: https://github.com/nlohmann/json
- Version: 3.9.1#

### OpenSSL
- License: Apache-2.0 license
- Homepage: https://www.openssl.org/
- Repository: https://github.com/openssl/openssl
- Version: 1.1.1

### protobuf
- License: BSD 3-Clause "New" or "Revised" License
- Repository: https://github.com/protocolbuffers/protobuf
- Version: 3.19.1

### spdlog
- License: MIT License
- Repository: https://github.com/gabime/spdlog
- Changes: removed include/spdlog/fmt/bundled
- Version: 1.11.0

### WebSocket++
- License: BSD 3-Clause License
- Homepage: www.zaphoyd.com/websocketpp
- Repository: https://github.com/zaphoyd/websocketpp
- Version: 0.8.2


### zlib
- Required for protobuf (versions >= 3.17.0)
- License: zlib
- Repository: https://github.com/madler/zlib
- Version: v1.2.12

## Development build dependencies


### tracy
- License: BSD 3-Clause
- Repository: https://bitbucket.org/wolfpld/tracy
- Version: 0.5
- Misc: Patch version can be downloaded from https://bitbucket.org/afrasson/tracy/get/update-port.zip

### Units
- License: MIT License
- Repository: https://github.com/nholthaus/units
- Version: 2.3.2

# Licenses in use

| License                                 |    Type     |                           Additional information/comment                           |
| --------------------------------------- | :---------: | :--------------------------------------------------------------------------------: |
| Apache-2.0                              | Open Source |                        free license, compatible with GPLv3                         |
| MIT License                             | Open Source |              free license, without copyleft, compatible with GNU GPL               |
| Boost License 1.0                       | Open Source |              free license, without copyleft, compatible with GNU GPL               |
| BSD 2-Clause "Simplified" License       | Open Source |              free license, without copyleft, compatible with GNU GPL               |
| BSD 3-Clause "New" or "Revised" License | Open Source |              free license, without copyleft, compatible with GNU GPL               |
| The Happy Bunny License and MIT License | Open Source |                     Happy Bunny License (modified MIT License)                     |
| Zlib License                            | Open Source |                           free license, without copyleft                           |
| Eclipse Public License 2.0              | Open Source | free license, **weak copyleft**, additions to the same module have to be published |
