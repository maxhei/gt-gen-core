load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_VERSION = "1.11.0"


def googletest():
    maybe(
        http_archive,
        name = "googletest",
        url = "https://github.com/google/googletest/archive/refs/tags/release-{}.tar.gz".format(_VERSION),
        sha256 = "b4870bf121ff7795ba20d20bcdd8627b8e088f2d1dab299a031c1034eddc93d5",
        strip_prefix = "googletest-release-{}".format(_VERSION),
        patches = [
            Label("//:third_party/googletest/build_config.patch"),
            # Gtest >= 1.11.0 is impacted by a GCC compiler bug regarding template deduction.
            # See: https://github.com/google/googletest/issues/3552
            # We backport the upstream fix to this until we reach a gtest version incorporating it by itself.
             Label("//:third_party/googletest/gcc_printer_patch.patch"),
        ],
        patch_args = ["-p1"],
    )
