"""
This module contains rule to pull nholthaus units
"""

load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_VERSION = "2.3.1"

def units_nhh():
    maybe(
        http_archive,
        name = "units_nhh",
        build_file = Label("//:third_party/units/units.BUILD"),
        url = "https://github.com/nholthaus/units/archive/refs/tags/v{version}.tar.gz".format(version = _VERSION),
        sha256 = "4a242a6e1b117f0234dffc2796c9133ca57d114f0fdf2c200754e6770db6bfd8",
        strip_prefix = "units-2.3.1",
    )
