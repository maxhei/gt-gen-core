load("@gt_gen_core//bazel/rules:artifactory_deb.bzl", "artifactory_deb_group")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

"""
This module contains rule to pull openssl library from ubuntu archives.
"""


_NAME = "openssl"
_VERSION = "1.1_1.1.1-1ubuntu2.1~18.04.23_amd64"
_COPYRIGHT = "Copyright (C) 1999-2024, OpenSSL Project Authors."

def openssl():
    maybe(
        artifactory_deb_group,
        name = "openssl",
        build_file = Label("//:third_party/openssl/openssl.BUILD"),
        repo = "http://archive.ubuntu.com/ubuntu",
        package_group = {
            "pool/main/o/openssl/libssl-dev_1.1.1-1ubuntu2.1~18.04.23_amd64.deb": "a9b6e93d93e33206836b30630ae8ad720b15bece3da8275184f076e7e97d4b2f",
            "pool/main/o/openssl/libssl1.1_1.1.1-1ubuntu2.1~18.04.23_amd64.deb": "986727f5a99088bef98971edfcd9567f5693b5e941aa241894fce1020c7015a0",
        },
    )
