# *******************************************************************************
# Copyright (C) 2024, ANSYS, Inc.
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# *******************************************************************************

#!/bin/bash

set -e

MYDIR="$(dirname "$(readlink -f $0)")"
BASEDIR=$(realpath "${MYDIR}/../../../..")
CACHEDIR=$(realpath "${BASEDIR}/.cache")
SITEDIR=${BASEDIR}/artifacts/_site

# This override the cache folder of bazel
export TEST_TMPDIR="${CACHEDIR}"
export BAZELISK_HOME="${CACHEDIR}"

# Navigate to repo folder
cd "${MYDIR}/../../.." || exit 1

git_tag=$(git tag --points-at HEAD)

if [ ! -d "${SITEDIR}" ]; then
    mkdir -p "${BASEDIR}/artifacts/_site"
else
    rm -f ${BASEDIR}/test_results.html
fi

# Only create code coverage if current branch is main or tagged
if [[ "$BRANCH_NAME" == "main" || -n "$git_tag" ]]; then

    echo "Run test and generate test report ..."
    "${MYDIR}"/build_and_test/generate_test_report.sh -t "//Core/..." -c "core" \
        -p "--local_cpu_resources=2 --noshow_progress --ui_event_filters=-info,-stderr"

    ls -l test_results.html
    cp test_results.html ${BASEDIR}/artifacts/_site/test_results.html

else
    echo "Skipping generating code coverage for branch $BRANCH_NAME. Only main branch or tagged commit will generate code coverage."
fi
