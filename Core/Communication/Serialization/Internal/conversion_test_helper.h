/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_COMMUNICATION_SERIALIZATION_INTERNAL_CONVERSIONTESTHELPER_H
#define GTGEN_CORE_COMMUNICATION_SERIALIZATION_INTERNAL_CONVERSIONTESTHELPER_H

#include "Core/Communication/Serialization/Internal/map_type_conversions.h"
#include "Core/Communication/Serialization/Internal/sign_type_conversions.h"
#include "Core/Communication/Serialization/Internal/traffic_light_bulb_type_conversions.h"
#include "Core/Environment/Map/GtGenMap/gtgen_map.h"
#include "gtgen_map.pb.h"

#include <MantleAPI/Common/vector.h>
#include <gtest/gtest.h>

namespace gtgen::core::communication
{

void CheckVector3(const mantle_api::Vec3<units::length::meter_t>& map_vector,
                  const messages::map::Vector3d& proto_vector);

void CheckDimension3(const mantle_api::Dimension3& gtgen_dimension, const messages::map::Dimension3d& proto_dimension);

void CheckOrientation3(const mantle_api::Orientation3<units::angle::radian_t>& gtgen_orientation,
                       const messages::map::Orientation3d& proto_orientation);

void CheckPose(const mantle_api::Pose& gtgen_pose, const messages::map::Pose& proto_pose);

void CheckRoadObject(const environment::map::RoadObject& gtgen_object, const messages::map::RoadObject& proto_object);

void CheckSupplementarySign(const environment::map::MountedSign::SupplementarySign& gtgen_sign,
                            const messages::map::TrafficSign& proto_sign);

void CheckTrafficSign(const environment::map::TrafficSign& gtgen_sign, const messages::map::TrafficSign& proto_sign);

void CheckMountedSign(const environment::map::MountedSign& gtgen_sign, const messages::map::MountedSign& proto_sign);

void CheckGroundSign(const environment::map::GroundSign& gtgen_sign, const messages::map::GroundSign& proto_sign);

void CheckTrafficLightBulb(const environment::map::TrafficLightBulb& gtgen_traffic_light_bulb,
                           const messages::map::TrafficLight_LightBulb& proto_traffic_light_bulb);

void CheckTrafficLight(const environment::map::TrafficLight& gtgen_traffic_light,
                       const messages::map::TrafficLight& proto_traffic_light);

void CheckLaneBoundary(const environment::map::LaneBoundary& gtgen_boundary,
                       const messages::map::LaneBoundary& proto_boundary);

void CheckLane(const environment::map::Lane& gtgen_lane, const messages::map::Lane& proto_lane);

void CheckLaneGroup(const environment::map::GtGenMap& gtgen_map,
                    const environment::map::LaneGroup& gtgen_group,
                    const messages::map::LaneGroup& proto_group);

}  // namespace gtgen::core::communication

#endif  // GTGEN_CORE_COMMUNICATION_SERIALIZATION_INTERNAL_CONVERSIONTESTHELPER_H
