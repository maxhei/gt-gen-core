cc_library(
    name = "connection_state",
    srcs = ["connection_state.cpp"],
    hdrs = ["connection_state.h"],
    visibility = ["//Core/Communication:__subpackages__"],
    deps = [
        "//Core/Service/Logging:logging",
        "@//third_party/boost:statechart",
    ],
)

cc_test(
    name = "connection_state_test",
    timeout = "short",
    srcs = ["connection_state_test.cpp"],
    deps = [
        ":connection_state",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "conversion_request_processor",
    srcs = ["conversion_request_processor.cpp"],
    hdrs = ["conversion_request_processor.h"],
    visibility = ["//Core/Communication:__subpackages__"],
    deps = [
        "//Core/Communication/Serialization:message_converter",
        "//Core/Environment/Map/GtGenMap:gtgenmap",
        "//Core/Environment/Map/LaneLocationProvider:lane_location_provider",
        "//Core/Service/Gui/ProtoMessageDefinitions/gui:osi_gui",
        "//Core/Service/Logging:logging",
        "@mantle_api",
    ],
)

cc_test(
    name = "conversion_request_processor_test",
    timeout = "short",
    srcs = ["conversion_request_processor_test.cpp"],
    deps = [
        ":conversion_request_processor",
        "//Core/Tests/TestUtils/MapUtils:gtgen_map_builder",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "dispatcher_factory",
    srcs = ["dispatcher_factory.cpp"],
    hdrs = ["dispatcher_factory.h"],
    visibility = ["//Core/Communication:__subpackages__"],
    deps = ["//Core/Communication/Common:i_dispatcher"],
)

cc_library(
    name = "simulation_control_dispatcher",
    srcs = ["simulation_control_dispatcher.cpp"],
    hdrs = ["simulation_control_dispatcher.h"],
    visibility = [
        "//Core/Communication:__subpackages__",
        "//Core/Simulation/Simulator:__subpackages__",
    ],
    deps = [
        "//Core/Communication/Common:i_dispatcher",
        "//Core/Communication/Serialization:message_converter",
        "//Core/Service/Logging:logging",
    ],
)

cc_test(
    name = "simulation_control_dispatcher_test",
    timeout = "short",
    srcs = ["simulation_control_dispatcher_test.cpp"],
    deps = [
        ":simulation_control_dispatcher",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "simulation_gui_dispatcher",
    srcs = ["simulation_gui_dispatcher.cpp"],
    hdrs = ["simulation_gui_dispatcher.h"],
    visibility = [
        "//:__pkg__",
        "//Core/Communication:__subpackages__",
        "//Core/Simulation:__subpackages__",
    ],
    deps = [
        ":connection_state",
        ":conversion_request_processor",
        "//Core/Communication/Common:i_dispatcher",
        "//Core/Communication/Serialization:message_converter",
        "//Core/Environment/Chunking:world_chunk",
        "//Core/Environment/GroundTruth:sensor_view_builder",
        "//Core/Service/FileSystem:file_system",
        "//Core/Service/Logging:logging",
        "@mantle_api",
    ],
)

cc_test(
    name = "simulation_gui_dispatcher_test",
    timeout = "short",
    srcs = ["simulation_gui_dispatcher_test.cpp"],
    deps = [
        ":simulation_gui_dispatcher",
        "//Core/Communication/Common:test_helper",
        "//Core/Environment/GtGenEnvironment:entity_repository",
        "//Core/Tests/TestUtils/MapCatalogue:map_catalogue",
        "//Core/Tests/TestUtils/MapUtils:gtgen_map_builder",
        "@googletest//:gtest_main",
        "@mantle_api//:test_utils",
    ],
)

cc_library(
    name = "thread_safe_ground_truth_wrapper",
    hdrs = ["thread_safe_ground_truth_wrapper.h"],
    visibility = [
        "//:__pkg__",
        "//Core/Simulation/Simulator:__subpackages__",
    ],
    deps = ["@//third_party/open_simulation_interface:open_simulation_interface"],
)
