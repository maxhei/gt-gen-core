/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Communication/Dispatchers/simulation_gui_dispatcher.h"

#include "Core/Communication/Dispatchers/connection_state.h"
#include "test_interface.pb.h"

namespace gtgen::core::communication
{

SimulationGuiDispatcher::SimulationGuiDispatcher(VisualizationData visualization_data,
                                                 std::shared_ptr<mantle_api::IEnvironment> environment)
    : visualization_data_{std::move(visualization_data)}, env_{std::move(environment)}
{
}

SimulationGuiDispatcher::~SimulationGuiDispatcher() = default;

void SimulationGuiDispatcher::Initialize(std::function<void(std::unique_ptr<MessageContainer>)> async_send)
{
    async_send_ = async_send;

    // only way of simple lifetime handling is wrapping in unique ptr
    connection_state_ = std::make_unique<statemachine::GuiConnectionState>();

    connection_state_->send_scenario_data = [this] { SendScenarioData(); };
    connection_state_->send_ground_truth = [this] { SendGtAndAdditionalUiData(); };
    connection_state_->core_ack = [this] { AcknowledgeConnection(); };
    connection_state_->ping = [this] { SendPing(); };

    connection_state_->initiate();

    map_conversion_request_processor_ =
        std::make_unique<ConversionRequestProcessor>(visualization_data_.map, async_send_);

    initialized_ = true;
}

bool SimulationGuiDispatcher::ProcessMessage(const messages::gui::MessageWrapper& received_message) noexcept
{
    std::scoped_lock lock(mutex_);

    if (!initialized_)
    {
        Warn("Tried to process message before dispatcher initialization or after shutdown! Will just return.");
        return false;
    }

    switch (received_message.message_type_identifier())
    {
        case messages::gui::MessageType::AREUCORE:
        {
            connection_state_->process_event(statemachine::ReceivedAreYouCore{});
            return true;
        }
        case messages::gui::MessageType::JUSTPING:
        {
            connection_state_->process_event(statemachine::ReceivedPing{});
            return true;
        }
        case messages::gui::MessageType::GIVESCEN:
        {
            connection_state_->process_event(statemachine::ReceivedScenarioDataRequest{});
            return true;
        }
        case messages::gui::MessageType::LETSVISU:
        {
            connection_state_->process_event(statemachine::ReceivedGTRequest{});
            return true;
        }
        case messages::gui::MessageType::SETVALUE:
        {
            return SetValue(received_message.test_interface().name(), received_message.test_interface().value());
        }
        case messages::gui::MessageType::SCENEDIT:
        {
            return HandleConversionRequest(received_message.editor_api());
        }
        default:
        {
            return false;
        }
    }
}

/// Actions

void SimulationGuiDispatcher::AcknowledgeConnection()
{
    const auto pid = getpid();
    auto message =
        message_converter_.CreateMessage<CoreConnectionAcknowledgement>(pid, visualization_data_.sim_control_port);
    async_send_(std::move(message));
}

void SimulationGuiDispatcher::SendPing()
{
    auto message = message_converter_.CreateMessage<JustPing>("yo");
    async_send_(std::move(message));
}

void SimulationGuiDispatcher::SendScenarioData()
{
    auto message = message_converter_.CreateMessage<ScenarioData>(visualization_data_.map,
                                                                  visualization_data_.settings,
                                                                  visualization_data_.user_settings_file_path,
                                                                  visualization_data_.repo);
    async_send_(std::move(message));
}

void SimulationGuiDispatcher::SendGtAndAdditionalUiData()
{
    visualization_data_.sensor_view_builder.gt_mutex.lock();
    auto message = message_converter_.CreateMessage<SimGuiData>(
        visualization_data_.sensor_view_builder.GetSensorView().global_ground_truth(), visualization_data_.ui_data);
    visualization_data_.sensor_view_builder.gt_mutex.unlock();

    async_send_(std::move(message));
}

bool SimulationGuiDispatcher::SetValue(const std::string& name, const std::string& value)
{
    if (env_ == nullptr)
    {
        Error(
            "Cannot process 'set value' message, since environment was not initialized! Please contact GTGen support "
            "for further assistance.");
        return false;
    }
    env_->SetUserDefinedValue(name, value);
    return true;
}

bool SimulationGuiDispatcher::HandleConversionRequest(const messages::editor::EditorApi& editor_request)
{
    if (editor_request.method_case() != messages::editor::EditorApi::kRequest)
    {
        Error(
            "Received an EditorApi message that didn't contain a request! Looks like an error in the "
            "ScenarioExecution.");
        return false;
    }

    const auto request_type = editor_request.request();
    switch (request_type)
    {
        case (messages::editor::EDITOR_REQUEST_GET_WORLD_POSE):
        {
            Error(
                "Received an EditorApi conversion request for a World position, but this is not supported. "
                "Looks like an error in ScenarioExecution.");
            return false;
        }
        case (messages::editor::EDITOR_REQUEST_GET_ODR_POSITION):
        {
            if (!visualization_data_.map.IsOpenDrive())
            {
                Error(
                    "Received an EditorApi conversion request for an OpenDrive position, but no OpenDrive map has been "
                    "loaded! Looks like an error in the ScenarioExecution.");
                return false;
            }
            return map_conversion_request_processor_->ProcessNativeMapPositionRequest(editor_request);
        }
        case (messages::editor::EDITOR_REQUEST_GET_NDS_POSITION):
        {
            if (!visualization_data_.map.IsNDS())
            {
                Error(
                    "Received an EditorApi conversion request for an NDS position, but no NDS map has been loaded! "
                    "Looks like an error in the ScenarioExecution.");
                return false;
            }
            return map_conversion_request_processor_->ProcessNativeMapPositionRequest(editor_request);
        }
        default:
        {
            return false;
        }
    }
}

void SimulationGuiDispatcher::ShutDown()
{
    std::scoped_lock lock(mutex_);
    initialized_ = false;
}

}  // namespace gtgen::core::communication
