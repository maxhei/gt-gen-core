/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_COMMUNICATION_FOXGLOVEWEBSOCKETSERVER_FOXGLOVEWEBSOCKETSERVER_H
#define GTGEN_CORE_COMMUNICATION_FOXGLOVEWEBSOCKETSERVER_FOXGLOVEWEBSOCKETSERVER_H

#include "osi_sensorview.pb.h"

#include <foxglove/websocket/websocket_notls.hpp>
#include <foxglove/websocket/websocket_server.hpp>

#include <mutex>

namespace gtgen::core::communication
{

class FoxgloveWebsocketServer
{
  public:
    FoxgloveWebsocketServer() = default;
    FoxgloveWebsocketServer(FoxgloveWebsocketServer&& other) = delete;
    FoxgloveWebsocketServer(const FoxgloveWebsocketServer&) = delete;
    FoxgloveWebsocketServer& operator=(const FoxgloveWebsocketServer&) = delete;
    FoxgloveWebsocketServer& operator=(FoxgloveWebsocketServer&& other) = delete;

    // NOLINTNEXTLINE(bugprone-exception-escape)
    virtual ~FoxgloveWebsocketServer();

    virtual void Start();
    virtual void Send(const osi3::SensorView& sensor_view);

  private:
    virtual void Stop();

    std::unique_ptr<foxglove::Server<foxglove::WebSocketNoTls>> server_{nullptr};
    std::mutex log_msg_mutex_;
    bool is_running_{false};
    std::uint32_t channel_id_{0};
};

}  // namespace gtgen::core::communication

#endif  // GTGEN_CORE_COMMUNICATION_FOXGLOVEWEBSOCKETSERVER_FOXGLOVEWEBSOCKETSERVER_H
