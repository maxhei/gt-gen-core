/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/TransitionDynamics/i_abstract_transition_dynamics.h"

#include "Core/Service/Logging/logging.h"
#include "Core/Service/Utility/math_utils.h"

namespace gtgen::core::environment::controller
{

IAbstractTransitionDynamics::IAbstractTransitionDynamics(
    double start_val,
    double target_val,
    const mantle_api::TransitionDynamics& transition_dynamics_config)
    : start_val_(start_val),
      target_val_(target_val),
      param_target_val_(transition_dynamics_config.value),
      dimension_(transition_dynamics_config.dimension)
{
    if (dimension_ == mantle_api::Dimension::kRate)
    {
        rate_ = transition_dynamics_config.value;
    }
}

void IAbstractTransitionDynamics::Init()
{
    if (dimension_ == mantle_api::Dimension::kRate)
    {
        rate_ = service::utility::GetSign(target_val_ - start_val_) * std::fabs(rate_);
        param_target_val_ = CalculateTargetParameterValByRate(rate_);
    }
    initialized_ = true;
}

bool IAbstractTransitionDynamics::Step(double delta_param_val)
{
    if (!initialized_)
    {
        Init();
    }

    if (IsTargetReached())
    {
        return false;
    }

    param_val_ += delta_param_val * scale_factor_;
    return true;
}

bool IAbstractTransitionDynamics::IsTargetReached() const
{
    return param_val_ >= param_target_val_;
}

void IAbstractTransitionDynamics::SetMaxRate(double max_rate)
{
    const auto peak_rate = (dimension_ == mantle_api::Dimension::kRate) ? rate_ : CalculatePeakRate();
    if (std::fabs(peak_rate) > std::fabs(max_rate))
    {
        scale_factor_ = std::fabs(max_rate) / std::fabs(service::utility::AvoidZero(peak_rate));
    }
    else
    {
        scale_factor_ = 1.0;
    }
}

}  // namespace gtgen::core::environment::controller
