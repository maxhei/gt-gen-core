cc_library(
    name = "controller_helpers",
    srcs = ["controller_helpers.cpp"],
    hdrs = ["controller_helpers.h"],
    visibility = ["//Core/Environment/Controller:__subpackages__"],
    deps = [
        "//Core/Environment/Controller/Internal/Utilities:find_path_utilities",
        "//Core/Environment/Controller/Internal/Utilities:spline_utilities",
        "//Core/Environment/Controller/Internal/Utilities:vector_utilities",
        "//Core/Environment/LaneFollowing:find_longest_path",
        "//Core/Environment/PathFinding:path_finder",
        "//Core/Service/Utility:position_utils",
    ],
)

cc_test(
    name = "controller_helpers_test",
    srcs = ["controller_helpers_test.cpp"],
    deps = [
        ":controller_helpers",
        "//Core/Environment/Entities:entities",
        "//Core/Tests/TestUtils:expect_extensions",
        "//Core/Tests/TestUtils/MapCatalogue:map_catalogue",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "find_path_utilities",
    visibility = ["//Core/Environment/Controller:__subpackages__"],
    deps = [
        ":path_spline_fitting",
        "//Core/Environment/LaneFollowing:find_longest_path",
        "//Core/Environment/LaneFollowing:line_follow_data_from_lanes",
        "//Core/Environment/LaneFollowing:line_follow_data_from_path",
        "//Core/Environment/LaneFollowing:line_follow_data_helper",
        "//Core/Environment/LaneFollowing:point_list_traverser",
    ],
)

cc_library(
    name = "path_spline_fitting",
    srcs = ["path_spline_fitting.cpp"],
    hdrs = ["path_spline_fitting.h"],
    deps = [
        "//Core/Environment/LaneFollowing:line_follow_data_helper",
        "@mantle_api",
    ],
)

cc_test(
    name = "path_spline_fitting_test",
    srcs = ["path_spline_fitting_test.cpp"],
    deps = [
        ":path_spline_fitting",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "spline_utilities",
    hdrs = ["spline_utilities.h"],
    visibility = ["//Core/Environment/Controller:__subpackages__"],
    deps = [
        "//Core/Service/Logging:logging",
        "@mantle_api",
    ],
)

cc_test(
    name = "spline_utilities_test",
    timeout = "short",
    srcs = ["spline_utilities_test.cpp"],
    deps = [
        ":spline_utilities",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "traffic_light_phase_state",
    srcs = ["traffic_light_phase_state.cpp"],
    hdrs = ["traffic_light_phase_state.h"],
    visibility = ["//Core/Environment/Controller:__subpackages__"],
    deps = ["//Core/Environment/Entities:traffic_light_entity"],
)

cc_test(
    name = "traffic_light_phase_state_test",
    timeout = "short",
    srcs = ["traffic_light_phase_state_test.cpp"],
    deps = [
        ":traffic_light_phase_state",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "traffic_light_state_machine",
    srcs = ["traffic_light_state_machine.cpp"],
    hdrs = ["traffic_light_state_machine.h"],
    visibility = ["//Core/Environment/Controller:__subpackages__"],
    deps = [
        ":traffic_light_phase_state",
        "//Core/Environment/Entities:traffic_light_entity",
    ],
)

cc_test(
    name = "traffic_light_state_machine_test",
    timeout = "short",
    srcs = ["traffic_light_state_machine_test.cpp"],
    deps = [
        ":traffic_light_state_machine",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "vector_utilities",
    hdrs = ["vector_utilities.h"],
    visibility = ["//Core/Environment/Controller:__subpackages__"],
    deps = [
        "//Core/Service/GlmWrapper:glm_wrapper",
        "@mantle_api",
    ],
)

cc_library(
    name = "wheel_contact",
    srcs = ["wheel_contact.cpp"],
    hdrs = ["wheel_contact.h"],
    visibility = ["//Core/Environment/Controller:__subpackages__"],
    deps = [
        "//Core/Environment/Entities:entities",
        "//Core/Service/GlmWrapper:glm_wrapper",
        "@mantle_api",
    ],
)

cc_test(
    name = "wheel_contact_test",
    timeout = "short",
    srcs = ["wheel_contact_test.cpp"],
    deps = [
        ":wheel_contact",
        "//Core/Tests/TestUtils:expect_extensions",
        "@googletest//:gtest_main",
    ],
)
