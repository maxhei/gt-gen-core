/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/Utilities/PlausibilityCheck/Internal/AttributePlausibilityCheck/plausibility_check_orientation_derivatives.h"

#include "Core/Environment/Controller/Internal/Utilities/PlausibilityCheck/Internal/Common/plausibility_check_base.h"
#include "Core/Service/Utility/derivative_utils.h"

namespace gtgen::core::environment::plausibility_check
{

using Angle = units::angle::radian_t;
using AngleRate = units::angular_velocity::radians_per_second_t;
using AngleAcceleration = units::angular_acceleration::radians_per_second_squared_t;

void PlausibilityCheckOrientationDerivatives::ProcessAttributes()
{
    current_external_orientation_ = entity_->GetOrientation();
    current_external_orientation_rate_ = entity_->GetOrientationRate();
    current_external_orientation_acceleration_ = entity_->GetOrientationAcceleration();

    if (previous_external_orientation_.has_value())
    {
        current_internal_orientation_rate_ = service::utility::GetOrientationDerivativeVector<Angle, AngleRate>(
            current_external_orientation_, previous_external_orientation_.value(), elapsed_time_);
    }

    if (previous_internal_orientation_rate_.has_value())
    {
        current_internal_orientation_acceleration_ =
            service::utility::GetOrientationDerivativeVector<AngleRate, AngleAcceleration>(
                current_internal_orientation_rate_.value(), previous_internal_orientation_rate_.value(), elapsed_time_);
    }
}

void PlausibilityCheckOrientationDerivatives::CompareInternalAndExternalAttributes() const
{
    if (current_internal_orientation_rate_.has_value())
    {
        CompareAndLog<AngleRate>(current_external_orientation_rate_.yaw,
                                 current_internal_orientation_rate_->yaw,
                                 &details::NotAlmostEqual<AngleRate>,
                                 log_messages_.yaw_rate_unexpected);
        CompareAndLog<AngleRate>(current_external_orientation_rate_.pitch,
                                 current_internal_orientation_rate_->pitch,
                                 &details::NotAlmostEqual<AngleRate>,
                                 log_messages_.pitch_rate_unexpected);
        CompareAndLog<AngleRate>(current_external_orientation_rate_.roll,
                                 current_internal_orientation_rate_->roll,
                                 &details::NotAlmostEqual<AngleRate>,
                                 log_messages_.roll_rate_unexpected);
    }

    if (current_internal_orientation_acceleration_.has_value())
    {
        CompareAndLog<AngleAcceleration>(current_external_orientation_acceleration_.yaw,
                                         current_internal_orientation_acceleration_->yaw,
                                         &details::NotAlmostEqual<AngleAcceleration>,
                                         log_messages_.yaw_acceleration_unexpected);
        CompareAndLog<AngleAcceleration>(current_external_orientation_acceleration_.pitch,
                                         current_internal_orientation_acceleration_->pitch,
                                         &details::NotAlmostEqual<AngleAcceleration>,
                                         log_messages_.pitch_acceleration_unexpected);
        CompareAndLog<AngleAcceleration>(current_external_orientation_acceleration_.roll,
                                         current_internal_orientation_acceleration_->roll,
                                         &details::NotAlmostEqual<AngleAcceleration>,
                                         log_messages_.roll_acceleration_unexpected);
    }
}

void PlausibilityCheckOrientationDerivatives::CheckExternalOrientationPlausibility() const
{
    CompareAndLog<Angle>(current_external_orientation_.pitch,
                         thresholds_.pitch_maximum,
                         std::greater<Angle>(),
                         log_messages_.pitch_above_threshold);
    CompareAndLog<Angle>(current_external_orientation_.pitch,
                         thresholds_.pitch_minimum,
                         std::less<Angle>(),
                         log_messages_.pitch_below_threshold);

    CompareAndLog<Angle>(current_external_orientation_.roll,
                         thresholds_.roll_maximum,
                         std::greater<Angle>(),
                         log_messages_.roll_above_threshold);
    CompareAndLog<Angle>(current_external_orientation_.roll,
                         thresholds_.roll_minimum,
                         std::less<Angle>(),
                         log_messages_.roll_below_threshold);
}

void PlausibilityCheckOrientationDerivatives::CheckExternalOrientationRatePlausibility() const
{
    CompareAndLog<AngleRate>(current_external_orientation_rate_.yaw,
                             thresholds_.orientation_rate_maximum,
                             std::greater<AngleRate>(),
                             log_messages_.yaw_rate_above_threshold);
    CompareAndLog<AngleRate>(current_external_orientation_rate_.yaw,
                             thresholds_.orientation_rate_minimum,
                             std::less<AngleRate>(),
                             log_messages_.yaw_rate_below_threshold);

    CompareAndLog<AngleRate>(current_external_orientation_rate_.pitch,
                             thresholds_.orientation_rate_maximum,
                             std::greater<AngleRate>(),
                             log_messages_.pitch_rate_above_threshold);
    CompareAndLog<AngleRate>(current_external_orientation_rate_.pitch,
                             thresholds_.orientation_rate_minimum,
                             std::less<AngleRate>(),
                             log_messages_.pitch_rate_below_threshold);

    CompareAndLog<AngleRate>(current_external_orientation_rate_.roll,
                             thresholds_.orientation_rate_maximum,
                             std::greater<AngleRate>(),
                             log_messages_.roll_rate_above_threshold);
    CompareAndLog<AngleRate>(current_external_orientation_rate_.roll,
                             thresholds_.orientation_rate_minimum,
                             std::less<AngleRate>(),
                             log_messages_.roll_rate_below_threshold);
}

void PlausibilityCheckOrientationDerivatives::CheckExternalOrientationAccelerationPlausibility() const
{
    CompareAndLog<AngleAcceleration>(current_external_orientation_acceleration_.yaw,
                                     thresholds_.orientation_acceleration_maximum,
                                     std::greater<AngleAcceleration>(),
                                     log_messages_.yaw_acceleration_above_threshold);
    CompareAndLog<AngleAcceleration>(current_external_orientation_acceleration_.yaw,
                                     thresholds_.orientation_acceleration_minimum,
                                     std::less<AngleAcceleration>(),
                                     log_messages_.yaw_acceleration_below_threshold);

    CompareAndLog<AngleAcceleration>(current_external_orientation_acceleration_.pitch,
                                     thresholds_.orientation_acceleration_maximum,
                                     std::greater<AngleAcceleration>(),
                                     log_messages_.pitch_acceleration_above_threshold);
    CompareAndLog<AngleAcceleration>(current_external_orientation_acceleration_.pitch,
                                     thresholds_.orientation_acceleration_minimum,
                                     std::less<AngleAcceleration>(),
                                     log_messages_.pitch_acceleration_below_threshold);

    CompareAndLog<AngleAcceleration>(current_external_orientation_acceleration_.roll,
                                     thresholds_.orientation_acceleration_maximum,
                                     std::greater<AngleAcceleration>(),
                                     log_messages_.roll_acceleration_above_threshold);
    CompareAndLog<AngleAcceleration>(current_external_orientation_acceleration_.roll,
                                     thresholds_.orientation_acceleration_minimum,
                                     std::less<AngleAcceleration>(),
                                     log_messages_.roll_acceleration_below_threshold);
}

void PlausibilityCheckOrientationDerivatives::CheckExternalAttributesPlausibility() const
{
    CheckExternalOrientationPlausibility();
    CheckExternalOrientationRatePlausibility();
    CheckExternalOrientationAccelerationPlausibility();
}

void PlausibilityCheckOrientationDerivatives::UpdatePreviousAttributes()
{
    previous_external_orientation_ = current_external_orientation_;

    if (current_internal_orientation_rate_.has_value())
    {
        previous_internal_orientation_rate_ = current_internal_orientation_rate_;
    }
}

}  // namespace gtgen::core::environment::plausibility_check
