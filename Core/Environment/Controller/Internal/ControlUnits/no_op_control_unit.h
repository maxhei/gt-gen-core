/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_NOOPCONTROLUNIT_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_NOOPCONTROLUNIT_H

#include "Core/Environment/Controller/Internal/ControlUnits/i_abstract_control_unit.h"

#include <MantleAPI/Common/time_utils.h>

namespace gtgen::core::environment::controller
{

class NoOpControlUnit : public IAbstractControlUnit
{
  public:
    std::unique_ptr<IControlUnit> Clone() const override;

    void StepControlUnit() override;
    bool HasFinished() const override;
};

}  // namespace gtgen::core::environment::controller

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_NOOPCONTROLUNIT_H
