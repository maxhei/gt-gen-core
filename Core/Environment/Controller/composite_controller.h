/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_COMPOSITECONTROLLER_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_COMPOSITECONTROLLER_H

#include "Core/Environment/Controller/Internal/ControlUnits/i_control_unit.h"

#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Common/time_utils.h>
#include <MantleAPI/Map/i_lane_location_query_service.h>
#include <MantleAPI/Traffic/default_routing_behavior.h>
#include <MantleAPI/Traffic/i_controller.h>
#include <MantleAPI/Traffic/i_controller_config.h>
#include <MantleAPI/Traffic/i_entity.h>

namespace gtgen::core::environment::controller
{

class CompositeController : public mantle_api::IController
{
  public:
    enum class Type
    {
        kUndefined = 0,
        kInternal,
        kExternal,
    };

    explicit CompositeController(mantle_api::UniqueId id,
                                 mantle_api::ILaneLocationQueryService* lane_location_query_service,
                                 Type type = Type::kUndefined);
    ~CompositeController() override = default;
    CompositeController(CompositeController const&) = delete;
    CompositeController& operator=(const CompositeController&) = delete;
    CompositeController(CompositeController&&) = default;
    CompositeController& operator=(CompositeController&&) = delete;

    virtual std::unique_ptr<CompositeController> Clone() const;
    mantle_api::UniqueId GetUniqueId() const override;
    void SetName(const std::string& name) override;
    const std::string& GetName() const override;
    virtual void SetEntity(mantle_api::IEntity& entity);
    virtual void Step(mantle_api::Time current_simulation_time);
    virtual bool HasFinished() const;

    virtual bool HasControlStrategyGoalBeenReached(std::uint64_t entity_id, mantle_api::ControlStrategyType type) const;

    virtual void AddControlUnit(std::unique_ptr<IControlUnit> control_unit);

    std::vector<std::unique_ptr<IControlUnit>>& GetControlUnits() { return control_units_; }
    virtual Type GetType() const;
    virtual void SetType(Type type);

    virtual void SetDefaultRoutingBehavior(mantle_api::DefaultRoutingBehavior default_routing_behavior);
    mantle_api::DefaultRoutingBehavior GetDefaultRoutingBehavior() const { return default_routing_behavior_; }

    void ChangeState(mantle_api::IController::LateralState lateral_state,
                     mantle_api::IController::LongitudinalState longitudinal_state) override;

    bool IsActive() const;

  protected:
    void ReplaceFinishedPathControlUnit();

    mantle_api::UniqueId id_;
    std::string name_{};
    mantle_api::IEntity* entity_{nullptr};
    std::vector<std::unique_ptr<IControlUnit>> control_units_;
    ControllerDataExchangeContainer controller_data_exchange_container_{};
    mantle_api::ILaneLocationQueryService* lane_location_query_service_{nullptr};
    Type type_{};
    mantle_api::DefaultRoutingBehavior default_routing_behavior_{mantle_api::DefaultRoutingBehavior::kStop};
    bool is_active_{false};
};

}  // namespace gtgen::core::environment::controller

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_COMPOSITECONTROLLER_H
