## How to build a composite controller
A composite controller consists of multiple control units, which each one designed to perform a specific operation on 
the entity assigned to it or update the shared data exchange container.

When a composite controller is created, several rules must be followed:
1. The world-space properties of the entity should be the last thing updated in the composite controller
2. A single property of the entity must not be changed by multiple control units (as to not violate the 
   single-responsibility principle)
3. The exchange of data between control units must be done via the ControllerDataExchangeContainer, not the entity

Any deviation outside of the rules listed above should be discussed with another developer. The same is true for adding
new rules to the list.

All composite controller have these properties in common:
1. They all contain a Lane Assignment controller
