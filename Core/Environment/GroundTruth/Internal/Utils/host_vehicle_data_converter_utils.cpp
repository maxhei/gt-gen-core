/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Exception/exception.h"
#include "osi_sensorview.pb.h"

#include <MantleAPI/Traffic/entity_properties.h>

namespace gtgen::core::environment::proto_groundtruth::utils
{

osi3::HostVehicleData::VehicleAutomatedDrivingFunction::State ConvertHadControlStateToOsiDrivingFunctionState(
    const mantle_api::ExternalControlState had_control_state)
{
    switch (had_control_state)
    {
        case mantle_api::ExternalControlState::kOff:
        {
            return osi3::HostVehicleData::VehicleAutomatedDrivingFunction::STATE_UNAVAILABLE;
        }
        case mantle_api::ExternalControlState::kLongitudinalOnly:
        case mantle_api::ExternalControlState::kLateralOnly:
        case mantle_api::ExternalControlState::kFull:
        {
            return osi3::HostVehicleData::VehicleAutomatedDrivingFunction::STATE_ACTIVE;
        }
        default:
            throw EnvironmentException("Unsupported HAD control state encountered: {}", had_control_state);
    }
}

}  // namespace gtgen::core::environment::proto_groundtruth::utils
