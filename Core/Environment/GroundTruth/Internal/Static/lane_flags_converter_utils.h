/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_STATIC_LANEFLAGSCONVERTERUTILS_H
#define GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_STATIC_LANEFLAGSCONVERTERUTILS_H

#include "Core/Environment/Map/GtGenMap/Internal/lane_flags.h"
#include "osi_lane.pb.h"

namespace gtgen::core::environment::proto_groundtruth
{
osi3::Lane_Classification_Type GetLaneFlagType(const map::LaneFlags& lane_flags);
osi3::Lane_Classification_Subtype GetLaneFlagSubType(const map::LaneFlags& lane_flags);
}  // namespace gtgen::core::environment::proto_groundtruth

#endif  // GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_STATIC_LANEFLAGSCONVERTERUTILS_H
