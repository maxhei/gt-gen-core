/*******************************************************************************
 * Copyright (c) 2021-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/StaticObjects/traffic_sign_properties_provider.h"

#include "Core/Service/Logging/logging.h"
#include "Core/Service/Utility/string_utils.h"

namespace gtgen::core::environment::static_objects
{
using units::literals::operator""_m;

std::unique_ptr<mantle_api::StaticObjectProperties> TrafficSignPropertiesProvider::Get(const std::string& identifier)
{
    if (identifier.empty())
    {
        return nullptr;
    }

    auto traffic_sign_properties = std::make_unique<mantle_ext::TrafficSignProperties>();
    traffic_sign_properties->type = mantle_api::EntityType::kStatic;
    traffic_sign_properties->bounding_box.dimension = {0.5_m, 0.5_m, 0.5_m};

    traffic_sign_properties->sign_type = GetOsiSignType(identifier);
    traffic_sign_properties->variability = osi::OsiTrafficSignVariability::kFixed;
    traffic_sign_properties->unit = GetSignValueUnit(identifier);
    traffic_sign_properties->direction = osi::OsiTrafficSignDirectionScope::kNoDirection;

    traffic_sign_properties->text = "";
    traffic_sign_properties->value = GetSignalValue(identifier);
    traffic_sign_properties->vertical_offset = 2.2_m;

    return traffic_sign_properties;
}

osi::OsiTrafficSignType TrafficSignPropertiesProvider::GetOsiSignType(const std::string& identifier)
{
    // first try to find the whole type
    // const auto& stvo_signs = GetStvoSigns();
    if (auto it = kStvoSignsToOsiSigns.find(identifier); it != kStvoSignsToOsiSigns.end())
    {
        return it->second;
    }

    // if that failed, only search for the type
    std::string type_identifier = GetTypeIdentifier(identifier);
    auto it = kStvoSignsToOsiSigns.find(type_identifier);
    if (it != kStvoSignsToOsiSigns.end())
    {
        return it->second;
    }

    Warn("Could not convert provided identifier '{}' to OSI sign type. It will be treated as TYPE_OTHER.", identifier);

    return osi::OsiTrafficSignType::kOther;
}

double TrafficSignPropertiesProvider::GetSignalValue(const std::string& identifier)
{
    // maps german traffic law (StVO) sign labels to values they are showing
    // when their subtype does not reflect the value provided as type-subtype
    const static std::map<std::string, double> special_stvo_sign_to_value = {{"274.1-", 30.0},
                                                                             {"274.1-40", 30.0},
                                                                             {"274.1-41", 20.0},
                                                                             {"274.2-", 30.0},
                                                                             {"274.2-20", 20.0},
                                                                             {"450-50", 100.0},
                                                                             {"450-51", 200.0},
                                                                             {"450-52", 300.0}};

    // Check the list of known values for certain special signs
    if (auto it = special_stvo_sign_to_value.find(identifier); it != special_stvo_sign_to_value.end())
    {
        return it->second;
    }

    // Now, try to derive the value from the subtype, because this is how most StVO signs work.
    // This is a very limited assumption, so we only convert if it's very clear. That means e.g. that
    // for now we only convert integers, so 314-50 will work, but 314-50.5 won't
    auto sub_type = GetSignalSubType(identifier);
    if (sub_type.has_value())
    {
        try
        {
            return std::stoi(sub_type.value());
        }
        catch (const std::invalid_argument& e)
        {
            Warn(
                "No traffic sign value information could be extracted from the provided identifier '{}'.The given "
                "identifier could not be translated into a number ",
                identifier);
        }
    }

    Warn("No traffic sign value information could be extracted from the provided identifier '{}'.", identifier);
    return -1;
}

osi::OsiTrafficSignValueUnit TrafficSignPropertiesProvider::GetSignValueUnit(const std::string& identifier)
{
    const static std::map<std::string, osi::OsiTrafficSignValueUnit> special_stvo_sign_to_unit = {
        {"274", osi::OsiTrafficSignValueUnit::kKilometerPerHour},
        {"274.1", osi::OsiTrafficSignValueUnit::kKilometerPerHour},
        {"274.2", osi::OsiTrafficSignValueUnit::kKilometerPerHour},
        {"275", osi::OsiTrafficSignValueUnit::kKilometerPerHour},
        {"278", osi::OsiTrafficSignValueUnit::kKilometerPerHour},
        {"279", osi::OsiTrafficSignValueUnit::kKilometerPerHour},
        {"450", osi::OsiTrafficSignValueUnit::kMeter},
        {"156", osi::OsiTrafficSignValueUnit::kMeter},
        {"157", osi::OsiTrafficSignValueUnit::kMeter},
        {"159", osi::OsiTrafficSignValueUnit::kMeter},
        {"162", osi::OsiTrafficSignValueUnit::kMeter},
        {"264", osi::OsiTrafficSignValueUnit::kMeter},
        {"265", osi::OsiTrafficSignValueUnit::kMeter},
        {"266", osi::OsiTrafficSignValueUnit::kMeter},
        {"273", osi::OsiTrafficSignValueUnit::kMeter},
        {"448", osi::OsiTrafficSignValueUnit::kMeter},
        {"108", osi::OsiTrafficSignValueUnit::kPercentage},
        {"110", osi::OsiTrafficSignValueUnit::kPercentage},
        {"262", osi::OsiTrafficSignValueUnit::kMetricTon},
        {"263", osi::OsiTrafficSignValueUnit::kMetricTon},
        {"625", osi::OsiTrafficSignValueUnit::kNoUnit}};

    // Check the list of known values for certain special signs
    std::string type_identifier = GetTypeIdentifier(identifier);
    if (auto it = special_stvo_sign_to_unit.find(type_identifier); it != special_stvo_sign_to_unit.end())
    {
        return it->second;
    }

    return osi::OsiTrafficSignValueUnit::kUnknown;
}

std::string TrafficSignPropertiesProvider::GetTypeIdentifier(const std::string& identifier)
{
    auto signal_type_information = service::utility::Split(identifier, '-');
    return signal_type_information.front();
}

std::optional<std::string> TrafficSignPropertiesProvider::GetSignalSubType(const std::string& identifier)
{
    auto signal_type_information = service::utility::Split(identifier, '-');
    if (signal_type_information.size() == 2)
    {
        return signal_type_information.back();
    }
    return std::nullopt;
}

// maps german traffic law (StVO) sign labels to OSI signs
// NOLINTNEXTLINE(cert-err58-cpp)
const TrafficSignPropertiesProvider::DataMap TrafficSignPropertiesProvider::kStvoSignsToOsiSigns{
    {"101", osi::OsiTrafficSignType::kDangerSpot},
    {"101-11", osi::OsiTrafficSignType::kZebraCrossing},
    {"101-21", osi::OsiTrafficSignType::kZebraCrossing},
    {"350-10", osi::OsiTrafficSignType::kZebraCrossing},
    {"350-20", osi::OsiTrafficSignType::kZebraCrossing},
    {"293", osi::OsiTrafficSignType::kZebraCrossing},
    {"101-10", osi::OsiTrafficSignType::kFlight},
    {"101-20", osi::OsiTrafficSignType::kFlight},
    {"101-12", osi::OsiTrafficSignType::kCattle},
    {"101-22", osi::OsiTrafficSignType::kCattle},
    {"101-13", osi::OsiTrafficSignType::kHorseRiders},
    {"101-23", osi::OsiTrafficSignType::kHorseRiders},
    {"101-14", osi::OsiTrafficSignType::kAmphibians},
    {"101-24", osi::OsiTrafficSignType::kAmphibians},
    {"101-15", osi::OsiTrafficSignType::kFallingRocks},
    {"101-25", osi::OsiTrafficSignType::kFallingRocks},
    {"101-51", osi::OsiTrafficSignType::kSnowOrIce},
    {"101-52", osi::OsiTrafficSignType::kLooseGravel},
    {"101-53", osi::OsiTrafficSignType::kWaterside},
    {"101-54", osi::OsiTrafficSignType::kClearance},
    {"101-55", osi::OsiTrafficSignType::kMovableBridge},
    {"102", osi::OsiTrafficSignType::kRightBeforeLeftNextIntersection},
    {"103-10", osi::OsiTrafficSignType::kTurnLeft},
    {"103-20", osi::OsiTrafficSignType::kTurnRight},
    {"105-10", osi::OsiTrafficSignType::kDoubleTurnLeft},
    {"105-20", osi::OsiTrafficSignType::kDoubleTurnRight},
    {"108", osi::OsiTrafficSignType::kHillDownwards},
    {"110", osi::OsiTrafficSignType::kHillUpwards},
    {"112", osi::OsiTrafficSignType::kUnevenRoad},
    {"114", osi::OsiTrafficSignType::kRoadSlipperyWetOrDirty},
    {"117-10", osi::OsiTrafficSignType::kSideWinds},
    {"117-20", osi::OsiTrafficSignType::kSideWinds},
    {"120", osi::OsiTrafficSignType::kRoadNarrowing},
    {"121-10", osi::OsiTrafficSignType::kRoadNarrowingRight},
    {"121-20", osi::OsiTrafficSignType::kRoadNarrowingLeft},
    {"123", osi::OsiTrafficSignType::kRoadWorks},
    {"124", osi::OsiTrafficSignType::kTrafficQueues},
    {"125", osi::OsiTrafficSignType::kTwoWayTraffic},
    {"131", osi::OsiTrafficSignType::kAttentionTrafficLight},
    {"133-10", osi::OsiTrafficSignType::kPedestrians},
    {"133-20", osi::OsiTrafficSignType::kPedestrians},
    {"136-10", osi::OsiTrafficSignType::kChildrenCrossing},
    {"136-20", osi::OsiTrafficSignType::kChildrenCrossing},
    {"138-10", osi::OsiTrafficSignType::kCycleRoute},
    {"138-20", osi::OsiTrafficSignType::kCycleRoute},
    {"142-10", osi::OsiTrafficSignType::kDeerCrossing},
    {"142-20", osi::OsiTrafficSignType::kDeerCrossing},
    {"151", osi::OsiTrafficSignType::kUngatedLevelCrossing},
    {"157-10", osi::OsiTrafficSignType::kLevelCrossingMarker},
    {"159-10", osi::OsiTrafficSignType::kLevelCrossingMarker},
    {"161-10", osi::OsiTrafficSignType::kLevelCrossingMarker},
    {"157-20", osi::OsiTrafficSignType::kLevelCrossingMarker},
    {"159-20", osi::OsiTrafficSignType::kLevelCrossingMarker},
    {"162-20", osi::OsiTrafficSignType::kLevelCrossingMarker},
    {"201-50", osi::OsiTrafficSignType::kRailwayTrafficPriority},
    {"201-52", osi::OsiTrafficSignType::kRailwayTrafficPriority},
    {"205", osi::OsiTrafficSignType::kGiveWay},
    {"341", osi::OsiTrafficSignType::kGiveWay},
    {"206", osi::OsiTrafficSignType::kStop},
    {"294", osi::OsiTrafficSignType::kStop},
    {"208", osi::OsiTrafficSignType::kPriorityToOppositeDirection},
    {"209-10", osi::OsiTrafficSignType::kPrescribedLeftTurn},
    {"209-20", osi::OsiTrafficSignType::kPrescribedRightTurn},
    {"209-30", osi::OsiTrafficSignType::kPrescribedStraight},
    {"211", osi::OsiTrafficSignType::kPrescribedRightWay},
    {"211-10", osi::OsiTrafficSignType::kPrescribedLeftWay},
    {"214", osi::OsiTrafficSignType::kPrescribedRightTurnAndStraight},
    {"214-10", osi::OsiTrafficSignType::kPrescribedLeftTurnAndStraight},
    {"214-30", osi::OsiTrafficSignType::kPrescribedLeftTurnAndRightTurn},
    {"215", osi::OsiTrafficSignType::kRoundabout},
    {"220-10", osi::OsiTrafficSignType::kOnewayLeft},
    {"220-20", osi::OsiTrafficSignType::kOnewayRight},
    {"222", osi::OsiTrafficSignType::kPassLeft},
    {"222-10", osi::OsiTrafficSignType::kPassRight},
    {"223.1-50", osi::OsiTrafficSignType::kSideLaneOpenForTraffic},
    {"223.1-51", osi::OsiTrafficSignType::kSideLaneOpenForTraffic},
    {"223.1-52", osi::OsiTrafficSignType::kSideLaneOpenForTraffic},
    {"223.2-50", osi::OsiTrafficSignType::kSideLaneClosedForTraffic},
    {"223.2-51", osi::OsiTrafficSignType::kSideLaneClosedForTraffic},
    {"223.2-52", osi::OsiTrafficSignType::kSideLaneClosedForTraffic},
    {"223.3-50", osi::OsiTrafficSignType::kSideLaneClosingForTraffic},
    {"223.3-51", osi::OsiTrafficSignType::kSideLaneClosingForTraffic},
    {"223.3-52", osi::OsiTrafficSignType::kSideLaneClosingForTraffic},
    {"224", osi::OsiTrafficSignType::kBusStop},
    {"237", osi::OsiTrafficSignType::kBicyclesOnly},
    {"238", osi::OsiTrafficSignType::kHorseRidersOnly},
    {"239", osi::OsiTrafficSignType::kPedestriansOnly},
    {"240", osi::OsiTrafficSignType::kBicyclesPedestriansSharedOnly},
    {"241-30", osi::OsiTrafficSignType::kBicyclesPedestriansSeparatedLeftOnly},
    {"241-31", osi::OsiTrafficSignType::kBicyclesPedestriansSeparatedRightOnly},
    {"242.1", osi::OsiTrafficSignType::kPedestrianZoneBegin},
    {"242.2", osi::OsiTrafficSignType::kPedestrianZoneEnd},
    {"244.1", osi::OsiTrafficSignType::kBicycleRoadBegin},
    {"244.2", osi::OsiTrafficSignType::kBicycleRoadEnd},
    {"245", osi::OsiTrafficSignType::kBusLane},
    {"250", osi::OsiTrafficSignType::kAllProhibited},
    {"251", osi::OsiTrafficSignType::kMotorizedMultitrackProhibited},
    {"253", osi::OsiTrafficSignType::kTrucksProhibited},
    {"254", osi::OsiTrafficSignType::kBicyclesProhibited},
    {"255", osi::OsiTrafficSignType::kMotorcyclesProhibited},
    {"257-50", osi::OsiTrafficSignType::kMopedsProhibited},
    {"257-51", osi::OsiTrafficSignType::kHorseRidersProhibited},
    {"257-52", osi::OsiTrafficSignType::kHorseCarriagesProhibited},
    {"257-53", osi::OsiTrafficSignType::kCattleProhibited},
    {"257-54", osi::OsiTrafficSignType::kBusesProhibited},
    {"257-55", osi::OsiTrafficSignType::kCarsProhibited},
    {"257-56", osi::OsiTrafficSignType::kCarsTrailersProhibited},
    {"257-57", osi::OsiTrafficSignType::kTrucksTrailersProhibited},
    {"257-58", osi::OsiTrafficSignType::kTractorsProhibited},
    {"259", osi::OsiTrafficSignType::kPedestriansProhibited},
    {"260", osi::OsiTrafficSignType::kMotorVehiclesProhibited},
    {"261", osi::OsiTrafficSignType::kHazardousGoodsVehiclesProhibited},
    {"262", osi::OsiTrafficSignType::kOverWeightVehiclesProhibited},
    {"263", osi::OsiTrafficSignType::kVehiclesAxleOverWeightProhibited},
    {"264", osi::OsiTrafficSignType::kVehiclesExcessWidthProhibited},
    {"265", osi::OsiTrafficSignType::kVehiclesExcessHeightProhibited},
    {"266", osi::OsiTrafficSignType::kVehiclesExcessLengthProhibited},
    {"267", osi::OsiTrafficSignType::kDoNotEnter},
    {"268", osi::OsiTrafficSignType::kSnowChainsRequired},
    {"269", osi::OsiTrafficSignType::kWaterPollutantVehiclesProhibited},
    {"270.1", osi::OsiTrafficSignType::kEnvironmentalZoneBegin},
    {"270.2", osi::OsiTrafficSignType::kEnvironmentalZoneEnd},
    {"272", osi::OsiTrafficSignType::kNoUTurnLeft},
    {"273", osi::OsiTrafficSignType::kMinimumDistanceForTrucks},
    {"274", osi::OsiTrafficSignType::kSpeedLimitBegin},
    {"274.1", osi::OsiTrafficSignType::kSpeedLimitZoneBegin},
    {"274.2", osi::OsiTrafficSignType::kSpeedLimitZoneEnd},
    {"275", osi::OsiTrafficSignType::kMinimumSpeedBegin},
    {"276", osi::OsiTrafficSignType::kOvertakingBanBegin},
    {"277", osi::OsiTrafficSignType::kOvertakingBanForTrucksBegin},
    {"278", osi::OsiTrafficSignType::kSpeedLimitEnd},
    {"279", osi::OsiTrafficSignType::kMinimumSpeedEnd},
    {"280", osi::OsiTrafficSignType::kOvertakingBanEnd},
    {"281", osi::OsiTrafficSignType::kOvertakingBanForTrucksEnd},
    {"282", osi::OsiTrafficSignType::kAllRestrictionsEnd},
    {"283", osi::OsiTrafficSignType::kNoStopping},
    {"286", osi::OsiTrafficSignType::kNoParking},
    {"299", osi::OsiTrafficSignType::kNoParking},
    {"290.1", osi::OsiTrafficSignType::kNoParkingZoneBegin},
    {"290.2", osi::OsiTrafficSignType::kNoParkingZoneEnd},
    {"301", osi::OsiTrafficSignType::kRightOfWayNextIntersection},
    {"306", osi::OsiTrafficSignType::kRightOfWayBegin},
    {"307", osi::OsiTrafficSignType::kRightOfWayEnd},
    {"308", osi::OsiTrafficSignType::kPriorityOverOppositeDirection},
    {"310", osi::OsiTrafficSignType::kTownBegin},
    {"311", osi::OsiTrafficSignType::kTownEnd},
    {"314", osi::OsiTrafficSignType::kCarParking},
    {"314-50", osi::OsiTrafficSignType::kCarParking},
    {"316", osi::OsiTrafficSignType::kCarParking},
    {"317", osi::OsiTrafficSignType::kCarParking},
    {"318", osi::OsiTrafficSignType::kCarParking},
    {"314-10", osi::OsiTrafficSignType::kCarParking},
    {"314-20", osi::OsiTrafficSignType::kCarParking},
    {"314-30", osi::OsiTrafficSignType::kCarParking},
    {"314.1", osi::OsiTrafficSignType::kCarParkingZoneBegin},
    {"314.2", osi::OsiTrafficSignType::kCarParkingZoneEnd},
    {"315-50", osi::OsiTrafficSignType::kSidewalkHalfParkingLeft},
    {"315-55", osi::OsiTrafficSignType::kSidewalkHalfParkingRight},
    {"315-60", osi::OsiTrafficSignType::kSidewalkParkingLeft},
    {"315-65", osi::OsiTrafficSignType::kSidewalkParkingRight},
    {"315-70", osi::OsiTrafficSignType::kSidewalkPerpendicularHalfParkingLeft},
    {"315-75", osi::OsiTrafficSignType::kSidewalkPerpendicularHalfParkingRight},
    {"315-80", osi::OsiTrafficSignType::kSidewalkPerpendicularParkingLeft},
    {"315-85", osi::OsiTrafficSignType::kSidewalkPerpendicularParkingRight},
    {"325.1", osi::OsiTrafficSignType::kLivingStreetBegin},
    {"325.2", osi::OsiTrafficSignType::kLivingStreetEnd},
    {"327", osi::OsiTrafficSignType::kTunnel},
    {"328", osi::OsiTrafficSignType::kEmergencyStoppingRight},
    {"330.1", osi::OsiTrafficSignType::kHighwayBegin},
    {"330.2", osi::OsiTrafficSignType::kHighwayEnd},
    {"331.1", osi::OsiTrafficSignType::kExpresswayBegin},
    {"331.2", osi::OsiTrafficSignType::kExpresswayEnd},
    {"332", osi::OsiTrafficSignType::kNamedHighwayExit},
    {"332.1", osi::OsiTrafficSignType::kNamedExpresswayExit},
    {"332.1-20", osi::OsiTrafficSignType::kNamedRoadExit},
    {"333", osi::OsiTrafficSignType::kHighwayExit},
    {"333.1", osi::OsiTrafficSignType::kExpresswayExit},
    {"353", osi::OsiTrafficSignType::kOnewayStreet},
    {"356", osi::OsiTrafficSignType::kCrossingGuards},
    {"357", osi::OsiTrafficSignType::kDeadend},
    {"357-50", osi::OsiTrafficSignType::kDeadendExcludingDesignatedActors},
    {"357-51", osi::OsiTrafficSignType::kDeadendExcludingDesignatedActors},
    {"357-52", osi::OsiTrafficSignType::kDeadendExcludingDesignatedActors},
    {"358", osi::OsiTrafficSignType::kFirstAidStation},
    {"363", osi::OsiTrafficSignType::kPoliceStation},
    {"365-50", osi::OsiTrafficSignType::kTelephone},
    {"365-51", osi::OsiTrafficSignType::kTelephone},
    {"365-52", osi::OsiTrafficSignType::kFillingStation},
    {"365-53", osi::OsiTrafficSignType::kFillingStation},
    {"365-54", osi::OsiTrafficSignType::kFillingStation},
    {"365-65", osi::OsiTrafficSignType::kFillingStation},
    {"365-66", osi::OsiTrafficSignType::kFillingStation},
    {"365-55", osi::OsiTrafficSignType::kHotel},
    {"365-56", osi::OsiTrafficSignType::kInn},
    {"365-57", osi::OsiTrafficSignType::kKiosk},
    {"365-58", osi::OsiTrafficSignType::kToilet},
    {"365-59", osi::OsiTrafficSignType::kChapel},
    {"365-61", osi::OsiTrafficSignType::kTouristInfo},
    {"365-62", osi::OsiTrafficSignType::kRepairService},
    {"365-63", osi::OsiTrafficSignType::kPedestrianUnderpass},
    {"365-64", osi::OsiTrafficSignType::kPedestrianBridge},
    {"365-67", osi::OsiTrafficSignType::kCamperPlace},
    {"365-68", osi::OsiTrafficSignType::kCamperPlace},
    {"380", osi::OsiTrafficSignType::kAdvisorySpeedLimitBegin},
    {"381", osi::OsiTrafficSignType::kAdvisorySpeedLimitEnd},
    {"385", osi::OsiTrafficSignType::kPlaceName},
    {"386.1", osi::OsiTrafficSignType::kTouristAttraction},
    {"386.2", osi::OsiTrafficSignType::kTouristRoute},
    {"386.3", osi::OsiTrafficSignType::kTouristArea},
    {"388", osi::OsiTrafficSignType::kShoulderNotPassableMotorVehicles},
    {"389", osi::OsiTrafficSignType::kShoulderUnsafeTrucksTractors},
    {"390", osi::OsiTrafficSignType::kTollBegin},
    {"390.2", osi::OsiTrafficSignType::kTollEnd},
    {"391", osi::OsiTrafficSignType::kTollRoad},
    {"392", osi::OsiTrafficSignType::kCustoms},
    {"393", osi::OsiTrafficSignType::kInternationalBorderInfo},
    {"401", osi::OsiTrafficSignType::kFederalHighwayRouteNumber},
    {"405", osi::OsiTrafficSignType::kHighwayRouteNumber},
    {"410", osi::OsiTrafficSignType::kEuropeanRouteNumber},
    {"415-10", osi::OsiTrafficSignType::kFederalHighwayDirectionLeft},
    {"415-20", osi::OsiTrafficSignType::kFederalHighwayDirectionRight},
    {"418-10", osi::OsiTrafficSignType::kPrimaryRoadDirectionLeft},
    {"418-20", osi::OsiTrafficSignType::kPrimaryRoadDirectionRight},
    {"419-10", osi::OsiTrafficSignType::kSecondaryRoadDirectionLeft},
    {"419-20", osi::OsiTrafficSignType::kSecondaryRoadDirectionRight},
    {"421-20", osi::OsiTrafficSignType::kDirectionDesignatedActorsRight},
    {"421-21", osi::OsiTrafficSignType::kDirectionDesignatedActorsRight},
    {"421-22", osi::OsiTrafficSignType::kDirectionDesignatedActorsRight},
    {"422-20", osi::OsiTrafficSignType::kRoutingDesignatedActors},
    {"422-22", osi::OsiTrafficSignType::kRoutingDesignatedActors},
    {"422-24", osi::OsiTrafficSignType::kRoutingDesignatedActors},
    {"422-26", osi::OsiTrafficSignType::kRoutingDesignatedActors},
    {"422-21", osi::OsiTrafficSignType::kRoutingDesignatedActors},
    {"422-23", osi::OsiTrafficSignType::kRoutingDesignatedActors},
    {"422-25", osi::OsiTrafficSignType::kRoutingDesignatedActors},
    {"422-27", osi::OsiTrafficSignType::kRoutingDesignatedActors},
    {"442-20", osi::OsiTrafficSignType::kRoutingDesignatedActors},
    {"442-22", osi::OsiTrafficSignType::kRoutingDesignatedActors},
    {"442-23", osi::OsiTrafficSignType::kRoutingDesignatedActors},
    {"430-10", osi::OsiTrafficSignType::kDirectionToHighwayLeft},
    {"430-20", osi::OsiTrafficSignType::kDirectionToHighwayRight},
    {"432-10", osi::OsiTrafficSignType::kDirectionToLocalDestinationLeft},
    {"432-20", osi::OsiTrafficSignType::kDirectionToLocalDestinationRight},
    {"437", osi::OsiTrafficSignType::kStreetName},
    {"438", osi::OsiTrafficSignType::kDirectionPreannouncement},
    {"439", osi::OsiTrafficSignType::kDirectionPreannouncementLaneConfig},
    {"440", osi::OsiTrafficSignType::kDirectionPreannouncementHighwayEntries},
    {"448", osi::OsiTrafficSignType::kHighwayAnnouncement},
    {"448-50", osi::OsiTrafficSignType::kOtherRoadAnnouncement},
    {"448.1", osi::OsiTrafficSignType::kHighwayAnnouncementTruckStop},
    {"449", osi::OsiTrafficSignType::kHighwayPreannouncementDirections},
    {"450", osi::OsiTrafficSignType::kPoleExit},
    {"454-10", osi::OsiTrafficSignType::kDetourLeft},
    {"454-20", osi::OsiTrafficSignType::kDetourRight},
    {"455.1", osi::OsiTrafficSignType::kNumberedDetour},
    {"457.1", osi::OsiTrafficSignType::kDetourBegin},
    {"457.2", osi::OsiTrafficSignType::kDetourEnd},
    {"458", osi::OsiTrafficSignType::kDetourRoutingBoard},
    {"460-50", osi::OsiTrafficSignType::kOptionalDetour},
    {"460-10", osi::OsiTrafficSignType::kOptionalDetour},
    {"460-11", osi::OsiTrafficSignType::kOptionalDetour},
    {"460-12", osi::OsiTrafficSignType::kOptionalDetour},
    {"460-20", osi::OsiTrafficSignType::kOptionalDetour},
    {"460-21", osi::OsiTrafficSignType::kOptionalDetour},
    {"460-22", osi::OsiTrafficSignType::kOptionalDetour},
    {"460-30", osi::OsiTrafficSignType::kOptionalDetour},
    {"466", osi::OsiTrafficSignType::kOptionalDetourRouting},
    {"467.1-10", osi::OsiTrafficSignType::kRouteRecommendation},
    {"467.1-20", osi::OsiTrafficSignType::kRouteRecommendation},
    {"467.2", osi::OsiTrafficSignType::kRouteRecommendationEnd},
    {"501-10", osi::OsiTrafficSignType::kAnnounceLaneTransitionLeft},
    {"501-11", osi::OsiTrafficSignType::kAnnounceLaneTransitionLeft},
    {"501-12", osi::OsiTrafficSignType::kAnnounceLaneTransitionLeft},
    {"501-20", osi::OsiTrafficSignType::kAnnounceLaneTransitionRight},
    {"501-21", osi::OsiTrafficSignType::kAnnounceLaneTransitionRight},
    {"501-22", osi::OsiTrafficSignType::kAnnounceLaneTransitionRight},
    {"531-10", osi::OsiTrafficSignType::kAnnounceRightLaneEnd},
    {"297.1-21", osi::OsiTrafficSignType::kAnnounceRightLaneEnd},
    {"531-20", osi::OsiTrafficSignType::kAnnounceLeftLaneEnd},
    {"551-20", osi::OsiTrafficSignType::kAnnounceLaneConsolidation},
    {"551-21", osi::OsiTrafficSignType::kAnnounceLaneConsolidation},
    {"551-22", osi::OsiTrafficSignType::kAnnounceLaneConsolidation},
    {"551-23", osi::OsiTrafficSignType::kAnnounceLaneConsolidation},
    {"551-24", osi::OsiTrafficSignType::kAnnounceLaneConsolidation},
    {"590-10", osi::OsiTrafficSignType::kDetourCityBlock},
    {"590-11", osi::OsiTrafficSignType::kDetourCityBlock},
    {"600", osi::OsiTrafficSignType::kGate},
    {"605", osi::OsiTrafficSignType::kPoleWarning},
    {"610", osi::OsiTrafficSignType::kTrafficCone},
    {"615", osi::OsiTrafficSignType::kMobileLaneClosure},
    {"616-30", osi::OsiTrafficSignType::kMobileLaneClosure},
    {"616-31", osi::OsiTrafficSignType::kMobileLaneClosure},
    {"620-40", osi::OsiTrafficSignType::kReflectorPost},
    {"621-40", osi::OsiTrafficSignType::kReflectorPost},
    {"625-1", osi::OsiTrafficSignType::kDirectionalBoardWarning},
    {"625-2", osi::OsiTrafficSignType::kDirectionalBoardWarning},
    {"626-10", osi::OsiTrafficSignType::kGuidingPlate},
    {"626-20", osi::OsiTrafficSignType::kGuidingPlate},
    {"626-30", osi::OsiTrafficSignType::kGuidingPlateWedges},
    {"630-10", osi::OsiTrafficSignType::kParkingHazard},
    {"630-20", osi::OsiTrafficSignType::kParkingHazard},
    {"720", osi::OsiTrafficSignType::kTrafficLightGreenArrow}};

}  // namespace gtgen::core::environment::static_objects
