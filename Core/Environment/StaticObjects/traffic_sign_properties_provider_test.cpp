/*******************************************************************************
 * Copyright (c) 2021-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/StaticObjects/traffic_sign_properties_provider.h"

#include <gtest/gtest.h>

namespace gtgen::core::environment::static_objects
{

TEST(TrafficSignPropertiesProviderTest, GivenEmptyIdentifier_WhenGet_ThenNullptrReturned)
{
    auto properties = TrafficSignPropertiesProvider::Get("");
    auto traffic_sign_properties = dynamic_cast<mantle_ext::TrafficSignProperties*>(properties.get());
    ASSERT_TRUE(traffic_sign_properties == nullptr);
}

TEST(TrafficSignPropertiesProviderTest, GivenAnyTrafficSignIdentifier_WhenGet_ThenSomeTrafficSignValuesAlwaysTheSame)
{
    auto properties = TrafficSignPropertiesProvider::Get("anything");
    auto traffic_sign_properties = dynamic_cast<mantle_ext::TrafficSignProperties*>(properties.get());
    ASSERT_TRUE(traffic_sign_properties != nullptr);

    EXPECT_EQ(mantle_api::EntityType::kStatic, traffic_sign_properties->type);
    EXPECT_EQ(0.5, traffic_sign_properties->bounding_box.dimension.length());
    EXPECT_EQ(0.5, traffic_sign_properties->bounding_box.dimension.width());
    EXPECT_EQ(0.5, traffic_sign_properties->bounding_box.dimension.height());
    EXPECT_EQ(osi::OsiTrafficSignVariability::kFixed, traffic_sign_properties->variability);
    EXPECT_EQ(osi::OsiTrafficSignValueUnit::kUnknown, traffic_sign_properties->unit);
    EXPECT_EQ(osi::OsiTrafficSignDirectionScope::kNoDirection, traffic_sign_properties->direction);
    EXPECT_TRUE(traffic_sign_properties->text.empty());
    EXPECT_EQ(2.2, traffic_sign_properties->vertical_offset());
}

TEST(TrafficSignPropertiesProviderTest, GivenNonTrafficSignIdentifier_WhenGet_ThenReturnOtherTrafficSign)
{
    auto properties = TrafficSignPropertiesProvider::Get("123456789");
    auto traffic_sign_properties = dynamic_cast<mantle_ext::TrafficSignProperties*>(properties.get());
    ASSERT_TRUE(traffic_sign_properties != nullptr);

    EXPECT_EQ(osi::OsiTrafficSignType::kOther, traffic_sign_properties->sign_type);
    EXPECT_EQ(-1.0, traffic_sign_properties->value);
}

TEST(TrafficSignPropertiesProviderTest, GivenSpeedLimit80Identifier_WhenGet_ThenPropertiesContainSpeedLimit80Infos)
{
    auto properties = TrafficSignPropertiesProvider::Get("274-80");
    auto traffic_sign_properties = dynamic_cast<mantle_ext::TrafficSignProperties*>(properties.get());
    ASSERT_TRUE(traffic_sign_properties != nullptr);

    EXPECT_EQ(osi::OsiTrafficSignType::kSpeedLimitBegin, traffic_sign_properties->sign_type);
    EXPECT_EQ(80.0, traffic_sign_properties->value);
    EXPECT_EQ(osi::OsiTrafficSignValueUnit::kKilometerPerHour, traffic_sign_properties->unit);
}

TEST(TrafficSignPropertiesProviderTest, GivenSpeedLimitIdentifier_WhenGet_ThenPropertiesSpeedLimitUnknown)
{
    auto properties = TrafficSignPropertiesProvider::Get("274");
    auto traffic_sign_properties = dynamic_cast<mantle_ext::TrafficSignProperties*>(properties.get());
    ASSERT_TRUE(traffic_sign_properties != nullptr);

    EXPECT_EQ(osi::OsiTrafficSignType::kSpeedLimitBegin, traffic_sign_properties->sign_type);
    EXPECT_EQ(-1.0, traffic_sign_properties->value);
}

TEST(TrafficSignPropertiesProviderTest, GivenInvalidSubType_WhenGet_ThenSpeedLimitValueIsUnknown)
{
    auto properties = TrafficSignPropertiesProvider::Get("274.1-AAAAAA");
    auto traffic_sign_properties = dynamic_cast<mantle_ext::TrafficSignProperties*>(properties.get());
    ASSERT_TRUE(traffic_sign_properties != nullptr);

    EXPECT_EQ(osi::OsiTrafficSignType::kSpeedLimitZoneBegin, traffic_sign_properties->sign_type);
    EXPECT_EQ(-1, traffic_sign_properties->value);
}

TEST(TrafficSignPropertiesProviderTest, GivenZoneSpeedLimitIdentifier_WhenGet_ThenPropertiesSpeedLimitCorrect)
{
    auto properties = TrafficSignPropertiesProvider::Get("274.1-");
    auto traffic_sign_properties = dynamic_cast<mantle_ext::TrafficSignProperties*>(properties.get());
    ASSERT_TRUE(traffic_sign_properties != nullptr);

    EXPECT_EQ(osi::OsiTrafficSignType::kSpeedLimitZoneBegin, traffic_sign_properties->sign_type);
    EXPECT_EQ(30.0, traffic_sign_properties->value);
    EXPECT_EQ(osi::OsiTrafficSignValueUnit::kKilometerPerHour, traffic_sign_properties->unit);
}

TEST(TrafficSignPropertiesProviderTest, GivenPoleExitIdentifier_WhenGet_ThenPropertiesPoleExitCorrect)
{
    auto properties = TrafficSignPropertiesProvider::Get("450-51");
    auto traffic_sign_properties = dynamic_cast<mantle_ext::TrafficSignProperties*>(properties.get());
    ASSERT_TRUE(traffic_sign_properties != nullptr);

    EXPECT_EQ(osi::OsiTrafficSignType::kPoleExit, traffic_sign_properties->sign_type);
    EXPECT_EQ(200.0, traffic_sign_properties->value);
    EXPECT_EQ(osi::OsiTrafficSignValueUnit::kMeter, traffic_sign_properties->unit);
}

class GetStvoSignsTypeTest : public testing::TestWithParam<std::tuple<std::string, osi::OsiTrafficSignType>>
{
};

INSTANTIATE_TEST_SUITE_P(AllStvoSigns,
                         GetStvoSignsTypeTest,
                         testing::ValuesIn(std::vector<std::tuple<std::string, osi::OsiTrafficSignType>>{
                             std::make_tuple("101", osi::OsiTrafficSignType::kDangerSpot),
                             std::make_tuple("101-11", osi::OsiTrafficSignType::kZebraCrossing),
                             std::make_tuple("101-21", osi::OsiTrafficSignType::kZebraCrossing),
                             std::make_tuple("350-10", osi::OsiTrafficSignType::kZebraCrossing),
                             std::make_tuple("350-20", osi::OsiTrafficSignType::kZebraCrossing),
                             std::make_tuple("293", osi::OsiTrafficSignType::kZebraCrossing),
                             std::make_tuple("101-10", osi::OsiTrafficSignType::kFlight),
                             std::make_tuple("101-20", osi::OsiTrafficSignType::kFlight),
                             std::make_tuple("101-12", osi::OsiTrafficSignType::kCattle),
                             std::make_tuple("101-22", osi::OsiTrafficSignType::kCattle),
                             std::make_tuple("101-13", osi::OsiTrafficSignType::kHorseRiders),
                             std::make_tuple("101-23", osi::OsiTrafficSignType::kHorseRiders),
                             std::make_tuple("101-14", osi::OsiTrafficSignType::kAmphibians),
                             std::make_tuple("101-24", osi::OsiTrafficSignType::kAmphibians),
                             std::make_tuple("101-15", osi::OsiTrafficSignType::kFallingRocks),
                             std::make_tuple("101-25", osi::OsiTrafficSignType::kFallingRocks),
                             std::make_tuple("101-51", osi::OsiTrafficSignType::kSnowOrIce),
                             std::make_tuple("101-52", osi::OsiTrafficSignType::kLooseGravel),
                             std::make_tuple("101-53", osi::OsiTrafficSignType::kWaterside),
                             std::make_tuple("101-54", osi::OsiTrafficSignType::kClearance),
                             std::make_tuple("101-55", osi::OsiTrafficSignType::kMovableBridge),
                             std::make_tuple("102", osi::OsiTrafficSignType::kRightBeforeLeftNextIntersection),
                             std::make_tuple("103-10", osi::OsiTrafficSignType::kTurnLeft),
                             std::make_tuple("103-20", osi::OsiTrafficSignType::kTurnRight),
                             std::make_tuple("105-10", osi::OsiTrafficSignType::kDoubleTurnLeft),
                             std::make_tuple("105-20", osi::OsiTrafficSignType::kDoubleTurnRight),
                             std::make_tuple("108", osi::OsiTrafficSignType::kHillDownwards),
                             std::make_tuple("110", osi::OsiTrafficSignType::kHillUpwards),
                             std::make_tuple("112", osi::OsiTrafficSignType::kUnevenRoad),
                             std::make_tuple("114", osi::OsiTrafficSignType::kRoadSlipperyWetOrDirty),
                             std::make_tuple("117-10", osi::OsiTrafficSignType::kSideWinds),
                             std::make_tuple("117-20", osi::OsiTrafficSignType::kSideWinds),
                             std::make_tuple("120", osi::OsiTrafficSignType::kRoadNarrowing),
                             std::make_tuple("121-10", osi::OsiTrafficSignType::kRoadNarrowingRight),
                             std::make_tuple("121-20", osi::OsiTrafficSignType::kRoadNarrowingLeft),
                             std::make_tuple("123", osi::OsiTrafficSignType::kRoadWorks),
                             std::make_tuple("124", osi::OsiTrafficSignType::kTrafficQueues),
                             std::make_tuple("125", osi::OsiTrafficSignType::kTwoWayTraffic),
                             std::make_tuple("131", osi::OsiTrafficSignType::kAttentionTrafficLight),
                             std::make_tuple("133-10", osi::OsiTrafficSignType::kPedestrians),
                             std::make_tuple("133-20", osi::OsiTrafficSignType::kPedestrians),
                             std::make_tuple("136-10", osi::OsiTrafficSignType::kChildrenCrossing),
                             std::make_tuple("136-20", osi::OsiTrafficSignType::kChildrenCrossing),
                             std::make_tuple("138-10", osi::OsiTrafficSignType::kCycleRoute),
                             std::make_tuple("138-20", osi::OsiTrafficSignType::kCycleRoute),
                             std::make_tuple("142-10", osi::OsiTrafficSignType::kDeerCrossing),
                             std::make_tuple("142-20", osi::OsiTrafficSignType::kDeerCrossing),
                             std::make_tuple("151", osi::OsiTrafficSignType::kUngatedLevelCrossing),
                             std::make_tuple("157-10", osi::OsiTrafficSignType::kLevelCrossingMarker),
                             std::make_tuple("159-10", osi::OsiTrafficSignType::kLevelCrossingMarker),
                             std::make_tuple("161-10", osi::OsiTrafficSignType::kLevelCrossingMarker),
                             std::make_tuple("157-20", osi::OsiTrafficSignType::kLevelCrossingMarker),
                             std::make_tuple("159-20", osi::OsiTrafficSignType::kLevelCrossingMarker),
                             std::make_tuple("162-20", osi::OsiTrafficSignType::kLevelCrossingMarker),
                             std::make_tuple("201-50", osi::OsiTrafficSignType::kRailwayTrafficPriority),
                             std::make_tuple("201-52", osi::OsiTrafficSignType::kRailwayTrafficPriority),
                             std::make_tuple("205", osi::OsiTrafficSignType::kGiveWay),
                             std::make_tuple("341", osi::OsiTrafficSignType::kGiveWay),
                             std::make_tuple("206", osi::OsiTrafficSignType::kStop),
                             std::make_tuple("294", osi::OsiTrafficSignType::kStop),
                             std::make_tuple("208", osi::OsiTrafficSignType::kPriorityToOppositeDirection),
                             std::make_tuple("209-10", osi::OsiTrafficSignType::kPrescribedLeftTurn),
                             std::make_tuple("209-20", osi::OsiTrafficSignType::kPrescribedRightTurn),
                             std::make_tuple("209-30", osi::OsiTrafficSignType::kPrescribedStraight),
                             std::make_tuple("211", osi::OsiTrafficSignType::kPrescribedRightWay),
                             std::make_tuple("211-10", osi::OsiTrafficSignType::kPrescribedLeftWay),
                             std::make_tuple("214", osi::OsiTrafficSignType::kPrescribedRightTurnAndStraight),
                             std::make_tuple("214-10", osi::OsiTrafficSignType::kPrescribedLeftTurnAndStraight),
                             std::make_tuple("214-30", osi::OsiTrafficSignType::kPrescribedLeftTurnAndRightTurn),
                             std::make_tuple("215", osi::OsiTrafficSignType::kRoundabout),
                             std::make_tuple("220-10", osi::OsiTrafficSignType::kOnewayLeft),
                             std::make_tuple("220-20", osi::OsiTrafficSignType::kOnewayRight),
                             std::make_tuple("222", osi::OsiTrafficSignType::kPassLeft),
                             std::make_tuple("222-10", osi::OsiTrafficSignType::kPassRight),
                             std::make_tuple("223.1-50", osi::OsiTrafficSignType::kSideLaneOpenForTraffic),
                             std::make_tuple("223.1-51", osi::OsiTrafficSignType::kSideLaneOpenForTraffic),
                             std::make_tuple("223.1-52", osi::OsiTrafficSignType::kSideLaneOpenForTraffic),
                             std::make_tuple("223.2-50", osi::OsiTrafficSignType::kSideLaneClosedForTraffic),
                             std::make_tuple("223.2-51", osi::OsiTrafficSignType::kSideLaneClosedForTraffic),
                             std::make_tuple("223.2-52", osi::OsiTrafficSignType::kSideLaneClosedForTraffic),
                             std::make_tuple("223.3-50", osi::OsiTrafficSignType::kSideLaneClosingForTraffic),
                             std::make_tuple("223.3-51", osi::OsiTrafficSignType::kSideLaneClosingForTraffic),
                             std::make_tuple("223.3-52", osi::OsiTrafficSignType::kSideLaneClosingForTraffic),
                             std::make_tuple("224", osi::OsiTrafficSignType::kBusStop),
                             std::make_tuple("237", osi::OsiTrafficSignType::kBicyclesOnly),
                             std::make_tuple("238", osi::OsiTrafficSignType::kHorseRidersOnly),
                             std::make_tuple("239", osi::OsiTrafficSignType::kPedestriansOnly),
                             std::make_tuple("240", osi::OsiTrafficSignType::kBicyclesPedestriansSharedOnly),
                             std::make_tuple("241-30", osi::OsiTrafficSignType::kBicyclesPedestriansSeparatedLeftOnly),
                             std::make_tuple("241-31", osi::OsiTrafficSignType::kBicyclesPedestriansSeparatedRightOnly),
                             std::make_tuple("242.1", osi::OsiTrafficSignType::kPedestrianZoneBegin),
                             std::make_tuple("242.2", osi::OsiTrafficSignType::kPedestrianZoneEnd),
                             std::make_tuple("244.1", osi::OsiTrafficSignType::kBicycleRoadBegin),
                             std::make_tuple("244.2", osi::OsiTrafficSignType::kBicycleRoadEnd),
                             std::make_tuple("245", osi::OsiTrafficSignType::kBusLane),
                             std::make_tuple("250", osi::OsiTrafficSignType::kAllProhibited),
                             std::make_tuple("251", osi::OsiTrafficSignType::kMotorizedMultitrackProhibited),
                             std::make_tuple("253", osi::OsiTrafficSignType::kTrucksProhibited),
                             std::make_tuple("254", osi::OsiTrafficSignType::kBicyclesProhibited),
                             std::make_tuple("255", osi::OsiTrafficSignType::kMotorcyclesProhibited),
                             std::make_tuple("257-50", osi::OsiTrafficSignType::kMopedsProhibited),
                             std::make_tuple("257-51", osi::OsiTrafficSignType::kHorseRidersProhibited),
                             std::make_tuple("257-52", osi::OsiTrafficSignType::kHorseCarriagesProhibited),
                             std::make_tuple("257-53", osi::OsiTrafficSignType::kCattleProhibited),
                             std::make_tuple("257-54", osi::OsiTrafficSignType::kBusesProhibited),
                             std::make_tuple("257-55", osi::OsiTrafficSignType::kCarsProhibited),
                             std::make_tuple("257-56", osi::OsiTrafficSignType::kCarsTrailersProhibited),
                             std::make_tuple("257-57", osi::OsiTrafficSignType::kTrucksTrailersProhibited),
                             std::make_tuple("257-58", osi::OsiTrafficSignType::kTractorsProhibited),
                             std::make_tuple("259", osi::OsiTrafficSignType::kPedestriansProhibited),
                             std::make_tuple("260", osi::OsiTrafficSignType::kMotorVehiclesProhibited),
                             std::make_tuple("261", osi::OsiTrafficSignType::kHazardousGoodsVehiclesProhibited),
                             std::make_tuple("262", osi::OsiTrafficSignType::kOverWeightVehiclesProhibited),
                             std::make_tuple("263", osi::OsiTrafficSignType::kVehiclesAxleOverWeightProhibited),
                             std::make_tuple("264", osi::OsiTrafficSignType::kVehiclesExcessWidthProhibited),
                             std::make_tuple("265", osi::OsiTrafficSignType::kVehiclesExcessHeightProhibited),
                             std::make_tuple("266", osi::OsiTrafficSignType::kVehiclesExcessLengthProhibited),
                             std::make_tuple("267", osi::OsiTrafficSignType::kDoNotEnter),
                             std::make_tuple("268", osi::OsiTrafficSignType::kSnowChainsRequired),
                             std::make_tuple("269", osi::OsiTrafficSignType::kWaterPollutantVehiclesProhibited),
                             std::make_tuple("270.1", osi::OsiTrafficSignType::kEnvironmentalZoneBegin),
                             std::make_tuple("270.2", osi::OsiTrafficSignType::kEnvironmentalZoneEnd),
                             std::make_tuple("272", osi::OsiTrafficSignType::kNoUTurnLeft),
                             std::make_tuple("273", osi::OsiTrafficSignType::kMinimumDistanceForTrucks),
                             std::make_tuple("274", osi::OsiTrafficSignType::kSpeedLimitBegin),
                             std::make_tuple("274.1", osi::OsiTrafficSignType::kSpeedLimitZoneBegin),
                             std::make_tuple("274.2", osi::OsiTrafficSignType::kSpeedLimitZoneEnd),
                             std::make_tuple("275", osi::OsiTrafficSignType::kMinimumSpeedBegin),
                             std::make_tuple("276", osi::OsiTrafficSignType::kOvertakingBanBegin),
                             std::make_tuple("277", osi::OsiTrafficSignType::kOvertakingBanForTrucksBegin),
                             std::make_tuple("278", osi::OsiTrafficSignType::kSpeedLimitEnd),
                             std::make_tuple("279", osi::OsiTrafficSignType::kMinimumSpeedEnd),
                             std::make_tuple("280", osi::OsiTrafficSignType::kOvertakingBanEnd),
                             std::make_tuple("281", osi::OsiTrafficSignType::kOvertakingBanForTrucksEnd),
                             std::make_tuple("282", osi::OsiTrafficSignType::kAllRestrictionsEnd),
                             std::make_tuple("283", osi::OsiTrafficSignType::kNoStopping),
                             std::make_tuple("286", osi::OsiTrafficSignType::kNoParking),
                             std::make_tuple("299", osi::OsiTrafficSignType::kNoParking),
                             std::make_tuple("290.1", osi::OsiTrafficSignType::kNoParkingZoneBegin),
                             std::make_tuple("290.2", osi::OsiTrafficSignType::kNoParkingZoneEnd),
                             std::make_tuple("301", osi::OsiTrafficSignType::kRightOfWayNextIntersection),
                             std::make_tuple("306", osi::OsiTrafficSignType::kRightOfWayBegin),
                             std::make_tuple("307", osi::OsiTrafficSignType::kRightOfWayEnd),
                             std::make_tuple("308", osi::OsiTrafficSignType::kPriorityOverOppositeDirection),
                             std::make_tuple("310", osi::OsiTrafficSignType::kTownBegin),
                             std::make_tuple("311", osi::OsiTrafficSignType::kTownEnd),
                             std::make_tuple("314", osi::OsiTrafficSignType::kCarParking),
                             std::make_tuple("314-50", osi::OsiTrafficSignType::kCarParking),
                             std::make_tuple("316", osi::OsiTrafficSignType::kCarParking),
                             std::make_tuple("317", osi::OsiTrafficSignType::kCarParking),
                             std::make_tuple("318", osi::OsiTrafficSignType::kCarParking),
                             std::make_tuple("314-10", osi::OsiTrafficSignType::kCarParking),
                             std::make_tuple("314-20", osi::OsiTrafficSignType::kCarParking),
                             std::make_tuple("314-30", osi::OsiTrafficSignType::kCarParking),
                             std::make_tuple("314.1", osi::OsiTrafficSignType::kCarParkingZoneBegin),
                             std::make_tuple("314.2", osi::OsiTrafficSignType::kCarParkingZoneEnd),
                             std::make_tuple("315-50", osi::OsiTrafficSignType::kSidewalkHalfParkingLeft),
                             std::make_tuple("315-55", osi::OsiTrafficSignType::kSidewalkHalfParkingRight),
                             std::make_tuple("315-60", osi::OsiTrafficSignType::kSidewalkParkingLeft),
                             std::make_tuple("315-65", osi::OsiTrafficSignType::kSidewalkParkingRight),
                             std::make_tuple("315-70", osi::OsiTrafficSignType::kSidewalkPerpendicularHalfParkingLeft),
                             std::make_tuple("315-75", osi::OsiTrafficSignType::kSidewalkPerpendicularHalfParkingRight),
                             std::make_tuple("315-80", osi::OsiTrafficSignType::kSidewalkPerpendicularParkingLeft),
                             std::make_tuple("315-85", osi::OsiTrafficSignType::kSidewalkPerpendicularParkingRight),
                             std::make_tuple("325.1", osi::OsiTrafficSignType::kLivingStreetBegin),
                             std::make_tuple("325.2", osi::OsiTrafficSignType::kLivingStreetEnd),
                             std::make_tuple("327", osi::OsiTrafficSignType::kTunnel),
                             std::make_tuple("328", osi::OsiTrafficSignType::kEmergencyStoppingRight),
                             std::make_tuple("330.1", osi::OsiTrafficSignType::kHighwayBegin),
                             std::make_tuple("330.2", osi::OsiTrafficSignType::kHighwayEnd),
                             std::make_tuple("331.1", osi::OsiTrafficSignType::kExpresswayBegin),
                             std::make_tuple("331.2", osi::OsiTrafficSignType::kExpresswayEnd),
                             std::make_tuple("332", osi::OsiTrafficSignType::kNamedHighwayExit),
                             std::make_tuple("332.1", osi::OsiTrafficSignType::kNamedExpresswayExit),
                             std::make_tuple("332.1-20", osi::OsiTrafficSignType::kNamedRoadExit),
                             std::make_tuple("333", osi::OsiTrafficSignType::kHighwayExit),
                             std::make_tuple("333.1", osi::OsiTrafficSignType::kExpresswayExit),
                             std::make_tuple("353", osi::OsiTrafficSignType::kOnewayStreet),
                             std::make_tuple("356", osi::OsiTrafficSignType::kCrossingGuards),
                             std::make_tuple("357", osi::OsiTrafficSignType::kDeadend),
                             std::make_tuple("357-50", osi::OsiTrafficSignType::kDeadendExcludingDesignatedActors),
                             std::make_tuple("357-51", osi::OsiTrafficSignType::kDeadendExcludingDesignatedActors),
                             std::make_tuple("357-52", osi::OsiTrafficSignType::kDeadendExcludingDesignatedActors),
                             std::make_tuple("358", osi::OsiTrafficSignType::kFirstAidStation),
                             std::make_tuple("363", osi::OsiTrafficSignType::kPoliceStation),
                             std::make_tuple("365-50", osi::OsiTrafficSignType::kTelephone),
                             std::make_tuple("365-51", osi::OsiTrafficSignType::kTelephone),
                             std::make_tuple("365-52", osi::OsiTrafficSignType::kFillingStation),
                             std::make_tuple("365-53", osi::OsiTrafficSignType::kFillingStation),
                             std::make_tuple("365-54", osi::OsiTrafficSignType::kFillingStation),
                             std::make_tuple("365-65", osi::OsiTrafficSignType::kFillingStation),
                             std::make_tuple("365-66", osi::OsiTrafficSignType::kFillingStation),
                             std::make_tuple("365-55", osi::OsiTrafficSignType::kHotel),
                             std::make_tuple("365-56", osi::OsiTrafficSignType::kInn),
                             std::make_tuple("365-57", osi::OsiTrafficSignType::kKiosk),
                             std::make_tuple("365-58", osi::OsiTrafficSignType::kToilet),
                             std::make_tuple("365-59", osi::OsiTrafficSignType::kChapel),
                             std::make_tuple("365-61", osi::OsiTrafficSignType::kTouristInfo),
                             std::make_tuple("365-62", osi::OsiTrafficSignType::kRepairService),
                             std::make_tuple("365-63", osi::OsiTrafficSignType::kPedestrianUnderpass),
                             std::make_tuple("365-64", osi::OsiTrafficSignType::kPedestrianBridge),
                             std::make_tuple("365-67", osi::OsiTrafficSignType::kCamperPlace),
                             std::make_tuple("365-68", osi::OsiTrafficSignType::kCamperPlace),
                             std::make_tuple("380", osi::OsiTrafficSignType::kAdvisorySpeedLimitBegin),
                             std::make_tuple("381", osi::OsiTrafficSignType::kAdvisorySpeedLimitEnd),
                             std::make_tuple("385", osi::OsiTrafficSignType::kPlaceName),
                             std::make_tuple("386.1", osi::OsiTrafficSignType::kTouristAttraction),
                             std::make_tuple("386.2", osi::OsiTrafficSignType::kTouristRoute),
                             std::make_tuple("386.3", osi::OsiTrafficSignType::kTouristArea),
                             std::make_tuple("388", osi::OsiTrafficSignType::kShoulderNotPassableMotorVehicles),
                             std::make_tuple("389", osi::OsiTrafficSignType::kShoulderUnsafeTrucksTractors),
                             std::make_tuple("390", osi::OsiTrafficSignType::kTollBegin),
                             std::make_tuple("390.2", osi::OsiTrafficSignType::kTollEnd),
                             std::make_tuple("391", osi::OsiTrafficSignType::kTollRoad),
                             std::make_tuple("392", osi::OsiTrafficSignType::kCustoms),
                             std::make_tuple("393", osi::OsiTrafficSignType::kInternationalBorderInfo),
                             std::make_tuple("401", osi::OsiTrafficSignType::kFederalHighwayRouteNumber),
                             std::make_tuple("405", osi::OsiTrafficSignType::kHighwayRouteNumber),
                             std::make_tuple("410", osi::OsiTrafficSignType::kEuropeanRouteNumber),
                             std::make_tuple("415-10", osi::OsiTrafficSignType::kFederalHighwayDirectionLeft),
                             std::make_tuple("415-20", osi::OsiTrafficSignType::kFederalHighwayDirectionRight),
                             std::make_tuple("418-10", osi::OsiTrafficSignType::kPrimaryRoadDirectionLeft),
                             std::make_tuple("418-20", osi::OsiTrafficSignType::kPrimaryRoadDirectionRight),
                             std::make_tuple("419-10", osi::OsiTrafficSignType::kSecondaryRoadDirectionLeft),
                             std::make_tuple("419-20", osi::OsiTrafficSignType::kSecondaryRoadDirectionRight),
                             std::make_tuple("421-20", osi::OsiTrafficSignType::kDirectionDesignatedActorsRight),
                             std::make_tuple("421-21", osi::OsiTrafficSignType::kDirectionDesignatedActorsRight),
                             std::make_tuple("421-22", osi::OsiTrafficSignType::kDirectionDesignatedActorsRight),
                             std::make_tuple("422-20", osi::OsiTrafficSignType::kRoutingDesignatedActors),
                             std::make_tuple("422-22", osi::OsiTrafficSignType::kRoutingDesignatedActors),
                             std::make_tuple("422-24", osi::OsiTrafficSignType::kRoutingDesignatedActors),
                             std::make_tuple("422-26", osi::OsiTrafficSignType::kRoutingDesignatedActors),
                             std::make_tuple("422-21", osi::OsiTrafficSignType::kRoutingDesignatedActors),
                             std::make_tuple("422-23", osi::OsiTrafficSignType::kRoutingDesignatedActors),
                             std::make_tuple("422-25", osi::OsiTrafficSignType::kRoutingDesignatedActors),
                             std::make_tuple("422-27", osi::OsiTrafficSignType::kRoutingDesignatedActors),
                             std::make_tuple("442-20", osi::OsiTrafficSignType::kRoutingDesignatedActors),
                             std::make_tuple("442-22", osi::OsiTrafficSignType::kRoutingDesignatedActors),
                             std::make_tuple("442-23", osi::OsiTrafficSignType::kRoutingDesignatedActors),
                             std::make_tuple("430-10", osi::OsiTrafficSignType::kDirectionToHighwayLeft),
                             std::make_tuple("430-20", osi::OsiTrafficSignType::kDirectionToHighwayRight),
                             std::make_tuple("432-10", osi::OsiTrafficSignType::kDirectionToLocalDestinationLeft),
                             std::make_tuple("432-20", osi::OsiTrafficSignType::kDirectionToLocalDestinationRight),
                             std::make_tuple("437", osi::OsiTrafficSignType::kStreetName),
                             std::make_tuple("438", osi::OsiTrafficSignType::kDirectionPreannouncement),
                             std::make_tuple("439", osi::OsiTrafficSignType::kDirectionPreannouncementLaneConfig),
                             std::make_tuple("440", osi::OsiTrafficSignType::kDirectionPreannouncementHighwayEntries),
                             std::make_tuple("448", osi::OsiTrafficSignType::kHighwayAnnouncement),
                             std::make_tuple("448-50", osi::OsiTrafficSignType::kOtherRoadAnnouncement),
                             std::make_tuple("448.1", osi::OsiTrafficSignType::kHighwayAnnouncementTruckStop),
                             std::make_tuple("449", osi::OsiTrafficSignType::kHighwayPreannouncementDirections),
                             std::make_tuple("450", osi::OsiTrafficSignType::kPoleExit),
                             std::make_tuple("454-10", osi::OsiTrafficSignType::kDetourLeft),
                             std::make_tuple("454-20", osi::OsiTrafficSignType::kDetourRight),
                             std::make_tuple("455.1", osi::OsiTrafficSignType::kNumberedDetour),
                             std::make_tuple("457.1", osi::OsiTrafficSignType::kDetourBegin),
                             std::make_tuple("457.2", osi::OsiTrafficSignType::kDetourEnd),
                             std::make_tuple("458", osi::OsiTrafficSignType::kDetourRoutingBoard),
                             std::make_tuple("460-50", osi::OsiTrafficSignType::kOptionalDetour),
                             std::make_tuple("460-10", osi::OsiTrafficSignType::kOptionalDetour),
                             std::make_tuple("460-11", osi::OsiTrafficSignType::kOptionalDetour),
                             std::make_tuple("460-12", osi::OsiTrafficSignType::kOptionalDetour),
                             std::make_tuple("460-20", osi::OsiTrafficSignType::kOptionalDetour),
                             std::make_tuple("460-21", osi::OsiTrafficSignType::kOptionalDetour),
                             std::make_tuple("460-22", osi::OsiTrafficSignType::kOptionalDetour),
                             std::make_tuple("460-30", osi::OsiTrafficSignType::kOptionalDetour),
                             std::make_tuple("466", osi::OsiTrafficSignType::kOptionalDetourRouting),
                             std::make_tuple("467.1-10", osi::OsiTrafficSignType::kRouteRecommendation),
                             std::make_tuple("467.1-20", osi::OsiTrafficSignType::kRouteRecommendation),
                             std::make_tuple("467.2", osi::OsiTrafficSignType::kRouteRecommendationEnd),
                             std::make_tuple("501-10", osi::OsiTrafficSignType::kAnnounceLaneTransitionLeft),
                             std::make_tuple("501-11", osi::OsiTrafficSignType::kAnnounceLaneTransitionLeft),
                             std::make_tuple("501-12", osi::OsiTrafficSignType::kAnnounceLaneTransitionLeft),
                             std::make_tuple("501-20", osi::OsiTrafficSignType::kAnnounceLaneTransitionRight),
                             std::make_tuple("501-21", osi::OsiTrafficSignType::kAnnounceLaneTransitionRight),
                             std::make_tuple("501-22", osi::OsiTrafficSignType::kAnnounceLaneTransitionRight),
                             std::make_tuple("531-10", osi::OsiTrafficSignType::kAnnounceRightLaneEnd),
                             std::make_tuple("297.1-21", osi::OsiTrafficSignType::kAnnounceRightLaneEnd),
                             std::make_tuple("531-20", osi::OsiTrafficSignType::kAnnounceLeftLaneEnd),
                             std::make_tuple("551-20", osi::OsiTrafficSignType::kAnnounceLaneConsolidation),
                             std::make_tuple("551-21", osi::OsiTrafficSignType::kAnnounceLaneConsolidation),
                             std::make_tuple("551-22", osi::OsiTrafficSignType::kAnnounceLaneConsolidation),
                             std::make_tuple("551-23", osi::OsiTrafficSignType::kAnnounceLaneConsolidation),
                             std::make_tuple("551-24", osi::OsiTrafficSignType::kAnnounceLaneConsolidation),
                             std::make_tuple("590-10", osi::OsiTrafficSignType::kDetourCityBlock),
                             std::make_tuple("590-11", osi::OsiTrafficSignType::kDetourCityBlock),
                             std::make_tuple("600", osi::OsiTrafficSignType::kGate),
                             std::make_tuple("605", osi::OsiTrafficSignType::kPoleWarning),
                             std::make_tuple("610", osi::OsiTrafficSignType::kTrafficCone),
                             std::make_tuple("615", osi::OsiTrafficSignType::kMobileLaneClosure),
                             std::make_tuple("616-30", osi::OsiTrafficSignType::kMobileLaneClosure),
                             std::make_tuple("616-31", osi::OsiTrafficSignType::kMobileLaneClosure),
                             std::make_tuple("620-40", osi::OsiTrafficSignType::kReflectorPost),
                             std::make_tuple("621-40", osi::OsiTrafficSignType::kReflectorPost),
                             std::make_tuple("625-1", osi::OsiTrafficSignType::kDirectionalBoardWarning),
                             std::make_tuple("625-2", osi::OsiTrafficSignType::kDirectionalBoardWarning),
                             std::make_tuple("626-10", osi::OsiTrafficSignType::kGuidingPlate),
                             std::make_tuple("626-20", osi::OsiTrafficSignType::kGuidingPlate),
                             std::make_tuple("626-30", osi::OsiTrafficSignType::kGuidingPlateWedges),
                             std::make_tuple("630-10", osi::OsiTrafficSignType::kParkingHazard),
                             std::make_tuple("630-20", osi::OsiTrafficSignType::kParkingHazard),
                             std::make_tuple("720", osi::OsiTrafficSignType::kTrafficLightGreenArrow)}));

TEST_P(GetStvoSignsTypeTest, GivenIdentifier_WhenGet_ThenCorrectStvoSignType)
{
    std::string identifier = std::get<0>(GetParam());
    auto expected_sign_type = std::get<1>(GetParam());
    auto properties = TrafficSignPropertiesProvider::Get(identifier);
    auto traffic_sign_properties = dynamic_cast<mantle_ext::TrafficSignProperties*>(properties.get());
    ASSERT_TRUE(traffic_sign_properties != nullptr);

    EXPECT_EQ(expected_sign_type, traffic_sign_properties->sign_type);
}

}  // namespace gtgen::core::environment::static_objects
