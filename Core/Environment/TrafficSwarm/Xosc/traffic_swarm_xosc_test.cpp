/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/TrafficSwarm/Xosc/traffic_swarm_xosc.h"

#include "Core/Tests/TestUtils/MapCatalogue/lane_catalogue.h"
#include "Core/Tests/TestUtils/MapCatalogue/raw_map_builder.h"
#include "Core/Tests/TestUtils/expect_extensions.h"

#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Traffic/entity_properties.h>

namespace gtgen::core::environment::traffic_swarm
{
using units::literals::operator""_m;
using units::literals::operator""_mps;
using units::literals::operator""_rad;

std::unique_ptr<environment::map::GtGenMap> CreateMapWithSevenParallelStraightLanes()
{
    const std::size_t number_of_parallel_lanes{7};
    const units::length::meter_t lane_width{4.0};
    test_utils::RawMapBuilder builder;
    builder.AddNewLaneGroup();

    for (std::size_t i{0}; i < number_of_parallel_lanes; i++)
    {
        const mantle_api::Vec3<units::length::meter_t> origin{0.0_m, lane_width * static_cast<double>(i), 0.0_m};
        builder.AddFromCatalogue(
            test_utils::LaneCatalogue::LongStraightEastingLaneWithThreePoints({origin.x, origin.y, origin.z}));
    }

    return builder.Build();
}

class TrafficSwarmTest : public testing::Test
{
  protected:
    TrafficSwarmTest()
    {
        map_ = CreateMapWithSevenParallelStraightLanes();
        lane_location_provider_ = std::make_unique<map::LaneLocationProvider>(*map_);

        mantle_api::VehicleProperties properties;
        properties.is_host = true;

        auto& host{entity_repository_.Create(host_name, properties)};
        host.SetPosition(host_position);
        host.SetOrientation(host_orientation);
        host.SetVelocity(host_velocity);
    }

    void Init(size_t maximum_number_of_vehicles)
    {
        mantle_api::TrafficSwarmParameters parameters;
        parameters.central_entity_name = host_name;
        parameters.exclusion_radius = 0.0_m;
        parameters.maximum_number_of_vehicles = maximum_number_of_vehicles;
        parameters.semi_major_spawning_radius = 0.0_m;
        parameters.semi_minor_spawning_radius = spawning_radius;
        parameters.spawning_area_longitudinal_offset = 0.0_m;
        parameters.speed_range = {10.0_mps, 15.0_mps};

        traffic_swarm_ =
            std::make_unique<TrafficSwarmXosc>(*lane_location_provider_, entity_repository_, parameters, 50.0_m);
    }

    const mantle_api::Vec3<units::length::meter_t> mini_bb_center_to_front_axle_center{1.1615_m, 0_m, -0.413_m};
    const mantle_api::Vec3<units::length::meter_t> mini_bb_center_to_rear_axle_center{-1.3335_m, 0_m, -0.413_m};
    const mantle_api::Vec3<units::length::meter_t> host_position{0_m, 12_m, 0_m};
    const mantle_api::Orientation3<units::angle::radian_t> host_orientation{units::angle::radian_t{M_PI_2},
                                                                            0_rad,
                                                                            0_rad};
    const units::length::meter_t spawning_radius{50.0_m};
    std::unique_ptr<map::LaneLocationProvider> lane_location_provider_{nullptr};
    std::unique_ptr<TrafficSwarmXosc> traffic_swarm_{nullptr};

  private:
    const std::string host_name{"host"};
    const mantle_api::Vec3<units::velocity::meters_per_second_t> host_velocity{0_mps, 16.667_mps, 0_mps};
    std::unique_ptr<environment::map::GtGenMap> map_{nullptr};
    service::utility::UniqueIdProvider unique_id_provider_{};
    environment::api::EntityRepository entity_repository_{&unique_id_provider_};
};

TEST_F(TrafficSwarmTest,
       GivenTrafficSwarmAction_WhenGettingSmallCarVehicleProperties_ThenVehiclePropertiesAreAsExpected)
{
    Init(0);

    const auto properties{traffic_swarm_->GetVehicleProperties(mantle_api::VehicleClass::kSmall_car)};

    EXPECT_FALSE(properties.is_host);
    EXPECT_TRUE(properties.is_controlled_externally);
    EXPECT_EQ(properties.type, mantle_api::EntityType::kVehicle);
    EXPECT_EQ(properties.classification, mantle_api::VehicleClass::kSmall_car);
    EXPECT_EQ(properties.model, "BMWMINI2014");
    EXPECT_TRIPLE(properties.front_axle.bb_center_to_axle_center, mini_bb_center_to_front_axle_center);
    EXPECT_TRIPLE(properties.rear_axle.bb_center_to_axle_center, mini_bb_center_to_rear_axle_center);
    EXPECT_EQ(properties.front_axle.wheel_diameter, 0.588_m);
    EXPECT_EQ(properties.rear_axle.wheel_diameter, 0.588_m);
    EXPECT_EQ(properties.bounding_box.dimension.length, 3.821_m);
    EXPECT_EQ(properties.bounding_box.dimension.width, 1.727_m);
    EXPECT_EQ(properties.bounding_box.dimension.height, 1.414_m);
}

TEST_F(TrafficSwarmTest,
       GivenTrafficSwarmAction_WhenGettingMotorbikeVehicleProperties_ThenVehiclePropertiesAreSmallCarProperties)
{
    Init(0);

    const auto properties{traffic_swarm_->GetVehicleProperties(mantle_api::VehicleClass::kMotorbike)};

    EXPECT_FALSE(properties.is_host);
    EXPECT_TRUE(properties.is_controlled_externally);
    EXPECT_EQ(properties.type, mantle_api::EntityType::kVehicle);
    EXPECT_EQ(properties.classification, mantle_api::VehicleClass::kSmall_car);
    EXPECT_EQ(properties.model, "BMWMINI2014");
    EXPECT_TRIPLE(properties.front_axle.bb_center_to_axle_center, mini_bb_center_to_front_axle_center);
    EXPECT_TRIPLE(properties.rear_axle.bb_center_to_axle_center, mini_bb_center_to_rear_axle_center);
    EXPECT_EQ(properties.front_axle.wheel_diameter, 0.588_m);
    EXPECT_EQ(properties.rear_axle.wheel_diameter, 0.588_m);
    EXPECT_EQ(properties.bounding_box.dimension.length, 3.821_m);
    EXPECT_EQ(properties.bounding_box.dimension.width, 1.727_m);
    EXPECT_EQ(properties.bounding_box.dimension.height, 1.414_m);
}

TEST_F(TrafficSwarmTest, GivenNoRequestedVehicle_WhenGettingAvailableSpawningPoses_ThenSpawningPosesSizeIsZero)
{
    const size_t expected_vehicle_count{0};

    Init(expected_vehicle_count);

    const auto spawning_poses{traffic_swarm_->GetAvailableSpawningPoses()};

    EXPECT_EQ(expected_vehicle_count, spawning_poses.size());
}

TEST_F(TrafficSwarmTest, GivenFiveRequestedVehicle_WhenGettingAvailableSpawningPoses_ThenSpawningPosesSizeIsFive)
{
    const size_t expected_vehicle_count{5};

    Init(expected_vehicle_count);

    const auto spawning_poses{traffic_swarm_->GetAvailableSpawningPoses()};

    EXPECT_EQ(expected_vehicle_count, spawning_poses.size());
}

TEST_F(
    TrafficSwarmTest,
    GivenFiveRequestedVehicle_WhenGettingAvailableSpawningPoses_ThenSpawningPosesAreOnSpawningRadiusAndOrientedToLane)
{
    const size_t expected_vehicle_count{5};

    Init(expected_vehicle_count);

    const auto spawning_poses{traffic_swarm_->GetAvailableSpawningPoses()};

    ASSERT_EQ(expected_vehicle_count, spawning_poses.size());

    for (const auto& [pose, _] : spawning_poses)
    {
        const auto lane_orientation{lane_location_provider_->GetLaneOrientation(pose.position)};

        EXPECT_EQ(pose.position.x(), spawning_radius());
        EXPECT_TRIPLE(pose.orientation, lane_orientation);
    }
}

}  // namespace gtgen::core::environment::traffic_swarm
