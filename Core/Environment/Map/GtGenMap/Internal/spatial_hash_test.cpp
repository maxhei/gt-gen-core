/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/GtGenMap/Internal/spatial_hash.h"

#include "Core/Environment/Map/GtGenMap/Internal/lane_utility.h"

#include <gtest/gtest.h>

namespace gtgen::core::environment::map
{
using units::literals::operator""_m;

class SpatialHashTest : public testing::Test
{
  protected:
    std::unique_ptr<SpatialHash> spatial_hash_;

    void CreateSpatialHash(double cell_size,
                           double bbox_width,
                           double bbox_height,
                           glm::dvec2 bbox_bottom_left = glm::dvec2{})
    {
        glm::dvec2 min_pos = bbox_bottom_left;
        glm::dvec2 max_pos = bbox_bottom_left + glm::dvec2{bbox_width, bbox_height};
        BoundingBox2d bbox{min_pos, max_pos};

        spatial_hash_ = std::make_unique<SpatialHash>(bbox, cell_size);
    }

    static Lanes CreateTwoOverlappingLanes()
    {
        Lane lane_1{1};
        AddPointsToLanesPolygonAndUpdateBoundingBox(lane_1, {{0, 0}, {100, 0}, {100, 20}, {0, 20}});

        Lane lane_2{2};
        AddPointsToLanesPolygonAndUpdateBoundingBox(lane_2, {{0, 0}, {20, 0}, {20, 100}, {0, 100}});

        return {lane_1, lane_2};
    }

    static Lanes CreateTwoLanesWithGap(const units::length::meter_t gap_size)
    {
        Lane lane_1{1};
        AddPointsToLanesPolygonAndUpdateBoundingBox(lane_1, {{0, 0}, {100, 0}, {100, 20}, {0, 20}});
        lane_1.center_line = {{0_m, 10_m, 0_m}, {100_m, 10_m, 0_m}};

        Lane lane_2{2};
        AddPointsToLanesPolygonAndUpdateBoundingBox(
            lane_2, {{100 + gap_size(), 0}, {200 + gap_size(), 0}, {200 + gap_size(), 20}, {100 + gap_size(), 20}});
        lane_2.center_line = {{100_m + gap_size, 10_m, 0_m}, {200_m + gap_size, 10_m, 0_m}};

        return {lane_1, lane_2};
    }
};

// Ctor ----------------------------------------------------------------------------------------------------------------

TEST_F(SpatialHashTest, Given0x0BoundingBox_WhenCreatingSpatialGridWithCellSize1000x1000_ThenGridHasSize1000x1000)
{
    CreateSpatialHash(1000, 0, 0);

    EXPECT_EQ(1000, spatial_hash_->GetWidth());
    EXPECT_EQ(1000, spatial_hash_->GetHeight());
}

TEST_F(SpatialHashTest, Given999x1000BoundingBox_WhenCreatingSpatialGridWithCellSize1000x1000_ThenGridSizeIs1000x2000)
{
    CreateSpatialHash(1000, 999, 1000);

    EXPECT_EQ(1000, spatial_hash_->GetWidth());
    EXPECT_EQ(2000, spatial_hash_->GetHeight());
}

TEST_F(SpatialHashTest, Given999x999BoundingBox_WhenCreatingSpatialGridWithCellSize1000x1000_ThenGridHasOneCell)
{
    CreateSpatialHash(1000, 999, 999);

    EXPECT_EQ(1, spatial_hash_->GetNumberOfCells());
}

TEST_F(SpatialHashTest, Given1000x1000BoundingBox_WhenCreatingSpatialGridWithCellSize1000x1000_ThenGridIs2x2)
{
    CreateSpatialHash(1000, 1000, 1000);

    EXPECT_EQ(2 * 2, spatial_hash_->GetNumberOfCells());
}

TEST_F(SpatialHashTest, Given999x1000BoundingBox_WhenCreatingSpatialGridWithCellSize1000x1000_ThenGridIs1x2)
{
    CreateSpatialHash(1000, 999, 1000);

    EXPECT_EQ(1 * 2, spatial_hash_->GetNumberOfCells());
}

TEST_F(SpatialHashTest,
       GivenBoundingBoxWihNonZeroBottomLeft_WhenCreatingGrid_ThenOffsetIsEqualToBottomLeftOfBoundingBox)
{
    const glm::dvec2 offset{42, 42};

    CreateSpatialHash(1000, 999, 1000, offset);

    EXPECT_EQ(offset, spatial_hash_->GetWorldSpaceOffset());
}

// Hashing -------------------------------------------------------------------------------------------------------------

TEST_F(SpatialHashTest, GivenOutOfBoundsPositivePosition_WhenHashing_ThenReturnInvalidCellId)
{
    CreateSpatialHash(1000, 999, 999);

    mantle_api::Vec3<units::length::meter_t> non_existing_point{1000_m, 1000_m, 0_m};

    ASSERT_EQ(1, spatial_hash_->GetNumberOfCells());
    EXPECT_FALSE(SpatialHash::IsValidCellId(spatial_hash_->Hash(non_existing_point)));
}

TEST_F(SpatialHashTest, GivenOutOfBoundsNegativePosition_WhenHashing_ThenReturnInvalidCellId)
{
    CreateSpatialHash(1000, 999, 999);

    mantle_api::Vec3<units::length::meter_t> non_existing_point{-1_m, -1_m, 0_m};

    ASSERT_EQ(1, spatial_hash_->GetNumberOfCells());
    EXPECT_FALSE(SpatialHash::IsValidCellId(spatial_hash_->Hash(non_existing_point)));
}

TEST_F(SpatialHashTest, GivenGridWithOne1000x1000Cell_WhenHashingMinimalPosition_ThenReturnCell0)
{
    CreateSpatialHash(1000, 999, 999);

    ASSERT_EQ(1, spatial_hash_->GetNumberOfCells());
    EXPECT_EQ(0, spatial_hash_->Hash({0_m, 0_m, 0_m}));
}

TEST_F(SpatialHashTest, Given1x1GridWith1000x1000Cell_WhenHashingMaximalPosition_ThenReturnCell0)
{
    CreateSpatialHash(1000, 999, 999);

    ASSERT_EQ(1, spatial_hash_->GetNumberOfCells());
    EXPECT_EQ(0, spatial_hash_->Hash({999_m, 999_m, 0_m}));
}

TEST_F(SpatialHashTest, Given1x2GridWith1000x1000Cells_WhenHashingPos999x1000_ThenReturnCell1)
{
    CreateSpatialHash(1000, 999, 1001);

    ASSERT_EQ(2, spatial_hash_->GetNumberOfCells());
    EXPECT_EQ(1, spatial_hash_->Hash({999_m, 1000_m, 0_m}));
}

TEST_F(SpatialHashTest, Given2x1GridWith1000x1000Cells_WhenHashingPos1000x1000_ThenReturnCell1)
{
    CreateSpatialHash(1000, 1001, 999);

    ASSERT_EQ(2, spatial_hash_->GetNumberOfCells());
    EXPECT_EQ(1, spatial_hash_->Hash({1000_m, 999_m, 0_m}));
}

TEST_F(SpatialHashTest, Given2x1GridWith1000x1000CellsAndNonZeroBottomLeft_WhenHashingPos1000x1000_ThenReturnCell1)
{
    glm::dvec2 offset{4200, 4200};

    CreateSpatialHash(1000, 1001, 999, offset);

    ASSERT_EQ(2, spatial_hash_->GetNumberOfCells());
    EXPECT_EQ(1,
              spatial_hash_->Hash(mantle_api::Vec3<units::length::meter_t>{1000_m, 999_m, 0_m} +
                                  mantle_api::Vec3<units::length::meter_t>{
                                      units::length::meter_t(offset.x), units::length::meter_t(offset.y), 0_m}));
}

// Inserting -----------------------------------------------------------------------------------------------------------

TEST_F(SpatialHashTest, GivenLaneContainedInSingleCell_WhenLaneIsInserted_ThenLaneIsAssignedToSingleCell)
{
    CreateSpatialHash(500, 999, 999);

    Lane lane{0};
    AddPointsToLanesPolygonAndUpdateBoundingBox(lane, {{0, 0}, {100, 0}, {0, 100}, {100, 100}});

    spatial_hash_->InsertLanes({lane});

    ASSERT_EQ(1, spatial_hash_->GetHashTable().size());
    EXPECT_EQ(1, spatial_hash_->GetHashTable().at(0).size());
}

TEST_F(SpatialHashTest, GivenLaneOutsideHashBoundaries_WhenLaneIsInserted_ThenAssertFails)
{
    CreateSpatialHash(500, 999, 999);

    Lane lane{0};
    AddPointsToLanesPolygonAndUpdateBoundingBox(lane, {{0, 0}, {-100, 0}, {0, -100}, {-100, -100}});

    EXPECT_DEATH(spatial_hash_->InsertLanes({lane}), ".*");
}

TEST_F(SpatialHashTest, GivenLaneStrechingOver2x2Cells_WhenLaneIsInserted_ThenLaneIsAssignedToFourCells)
{
    CreateSpatialHash(500, 999, 999);

    Lane lane{0};
    AddPointsToLanesPolygonAndUpdateBoundingBox(lane, {{250, 250}, {750, 250}, {750, 750}, {250, 750}});

    spatial_hash_->InsertLanes({lane});

    ASSERT_EQ(4, spatial_hash_->GetHashTable().size());
    EXPECT_EQ(1, spatial_hash_->GetHashTable().at(0).size());
    EXPECT_EQ(1, spatial_hash_->GetHashTable().at(1).size());
    EXPECT_EQ(1, spatial_hash_->GetHashTable().at(2).size());
    EXPECT_EQ(1, spatial_hash_->GetHashTable().at(3).size());
}

TEST_F(SpatialHashTest,
       GivenLaneIntersectingCellWithoutHavingPointInIt_WhenLaneIsInserted_ThenLaneIsAssignedToAllIntersectingCells)
{
    CreateSpatialHash(500, 1499, 499);

    Lane lane{0};
    AddPointsToLanesPolygonAndUpdateBoundingBox(lane, {{0, 0}, {1400, 0}, {1400, 400}, {0, 400}});

    spatial_hash_->InsertLanes({lane});

    ASSERT_EQ(3, spatial_hash_->GetHashTable().size());
    EXPECT_EQ(1, spatial_hash_->GetHashTable().at(0).size());
    EXPECT_EQ(1, spatial_hash_->GetHashTable().at(1).size());
    EXPECT_EQ(1, spatial_hash_->GetHashTable().at(2).size());
}

TEST_F(
    SpatialHashTest,
    GivenGridWithNonZeroOriginAndLaneIntersectingCellWithoutHavingPointInIt_WhenLaneIsInserted_ThenLaneIsAssignedToAllIntersectingCells)
{
    glm::dvec2 offset{4200, -1234};

    CreateSpatialHash(500, 1499, 499, offset);

    Lane lane{0};
    AddPointsToLanesPolygonAndUpdateBoundingBox(lane,
                                                {glm::dvec2{0, 0} + offset,
                                                 glm::dvec2{1400, 0} + offset,
                                                 glm::dvec2{1400, 400} + offset,
                                                 glm::dvec2{0, 400} + offset});

    spatial_hash_->InsertLanes({lane});

    ASSERT_EQ(3, spatial_hash_->GetHashTable().size());
    EXPECT_EQ(1, spatial_hash_->GetHashTable().at(0).size());
    EXPECT_EQ(1, spatial_hash_->GetHashTable().at(1).size());
    EXPECT_EQ(1, spatial_hash_->GetHashTable().at(2).size());
}

TEST_F(SpatialHashTest, GivenFourEqualLanesInTwoLaneGroups_WhenInserting_ThenAllLanesWillBeAssignedToTheSameCell)
{
    CreateSpatialHash(500, 999, 999);

    Lanes lanes;
    for (mantle_api::UniqueId i = 0; i < 2; i++)
    {
        Lane lane{0};
        AddPointsToLanesPolygonAndUpdateBoundingBox(lane, {{0, 0}, {200, 0}, {200, 200}, {0, 200}});
        lanes.push_back(lane);
        lanes.push_back(lane);
    }

    spatial_hash_->InsertLanes(lanes);

    ASSERT_EQ(1, spatial_hash_->GetHashTable().size());
    EXPECT_EQ(4, spatial_hash_->GetHashTable().at(0).size());
}

// Querying ------------------------------------------------------------------------------------------------------------

TEST_F(SpatialHashTest, GivenQueryPositionOutsideHashBoundaries_WhenQueryingMultipleLanes_ThenEmptyListIsReturned)
{
    CreateSpatialHash(500, 999, 999);

    const mantle_api::Vec3<units::length::meter_t> query_position{-1_m, -1_m, 0.0_m};

    const auto lanes = spatial_hash_->GetLanes(query_position);

    EXPECT_EQ(lanes.size(), 0);
}

TEST_F(SpatialHashTest, GivenQueryPositionWithinEmptyCell_WhenQueryingMultipleLanes_ThenEmptyListIsReturned)
{
    CreateSpatialHash(500, 999, 999);

    const mantle_api::Vec3<units::length::meter_t> query_position{100_m, 100_m, 0.0_m};

    const auto lanes = spatial_hash_->GetLanes(query_position);

    EXPECT_EQ(lanes.size(), 0);
}

TEST_F(SpatialHashTest,
       GivenQueryPositionWithinMultipleLanes_WhenQueryingMultipleLanes_ThenAllLanesContainingQueryPositionAreReturned)
{
    CreateSpatialHash(500, 999, 999);
    const auto lanes = CreateTwoOverlappingLanes();
    spatial_hash_->InsertLanes(lanes);
    const mantle_api::Vec3<units::length::meter_t> position_within_both_lanes{10_m, 10_m, 0_m};

    const auto found_lanes = spatial_hash_->GetLanes(position_within_both_lanes);

    ASSERT_EQ(found_lanes.size(), 2);
    EXPECT_EQ(found_lanes[0], &lanes[0]);
    EXPECT_EQ(found_lanes[1], &lanes[1]);
}

TEST_F(SpatialHashTest,
       GivenQueryPositionWithinGapOfLanes_WhenQueryingMultipleLanes_ThenAllLanesCloseToQueryPositionAreReturned)
{
    CreateSpatialHash(500, 999, 999);
    const units::length::meter_t max_possible_gap_size{0.005_m};
    const auto lanes = CreateTwoLanesWithGap(max_possible_gap_size);
    spatial_hash_->InsertLanes(lanes);
    const mantle_api::Vec3<units::length::meter_t> position_within_both_lanes{
        100_m + max_possible_gap_size / 2, 10_m, 0_m};

    const auto found_lanes = spatial_hash_->GetLanes(position_within_both_lanes);

    ASSERT_EQ(found_lanes.size(), 2);
    EXPECT_EQ(found_lanes[0], &lanes[0]);
    EXPECT_EQ(found_lanes[1], &lanes[1]);
}

TEST_F(SpatialHashTest, GivenQueryPositionWithinTooLargeGapOfLanes_WhenQueryingMultipleLanes_ThenEmptyListIsReturned)
{
    CreateSpatialHash(500, 999, 999);
    const units::length::meter_t gap_size{0.011_m};
    const auto lanes = CreateTwoLanesWithGap(gap_size);
    spatial_hash_->InsertLanes(lanes);
    const mantle_api::Vec3<units::length::meter_t> position_within_both_lanes{100_m + gap_size / 2, 10_m, 0_m};

    const auto found_lanes = spatial_hash_->GetLanes(position_within_both_lanes);

    ASSERT_EQ(found_lanes.size(), 0);
}

}  // namespace gtgen::core::environment::map
