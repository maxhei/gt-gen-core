/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_GTGENMAP_GTGENMAP_H
#define GTGEN_CORE_ENVIRONMENT_MAP_GTGENMAP_GTGENMAP_H

#include "Core/Environment/Map/Common/converter_utility.h"
#include "Core/Environment/Map/GtGenMap/Internal/spatial_hash.h"
#include "Core/Environment/Map/GtGenMap/Internal/traffic_light.h"
#include "Core/Environment/Map/GtGenMap/lane.h"
#include "Core/Environment/Map/GtGenMap/lane_boundary.h"
#include "Core/Environment/Map/GtGenMap/lane_group.h"
#include "Core/Environment/Map/GtGenMap/patch.h"
#include "Core/Environment/Map/GtGenMap/road_objects.h"
#include "Core/Environment/Map/GtGenMap/signs.h"
#include "Core/Service/Logging/logging.h"

#include <MantleAPI/Common/position.h>

#include <string>
#include <unordered_map>
#include <variant>

namespace gtgen::core::environment::map
{
class SpatialHash;

class GtGenMap
{
  public:
    /// @brief The supported underlying map types
    enum class SourceMapType
    {
        kUnknown,
        kOpenDrive,
        kNDS
    };

    void ClearMap();

    /// @brief Convenience function to evaluate if the map is of type OpenDrive
    bool IsOpenDrive() const;

    /// @brief Convenience function to evaluate if the map is of type NDS
    bool IsNDS() const;

    void DoForAllLanes(const std::function<void(Lane&)>& func);

    void DoForAllLanes(const std::function<void(const Lane&)>& func) const;

    void DoForAllBoundaries(const std::function<void(LaneBoundary&)>& func);

    void DoForAllBoundaries(const std::function<void(const LaneBoundary&)>& func) const;

    void SetSpatialHash(std::unique_ptr<SpatialHash> spatial_hash);
    const environment::map::SpatialHash& GetSpatialHash() const;

    /// @breif Simple setter
    void SetSourceMapType(const SourceMapType map_type) { source_map_type_ = map_type; }

    /// @brief Returns all mounted signs
    MountedSigns GetMountedSigns() const;

    /// @brief Returns all ground signs
    GroundSigns GetGroundSigns() const;

    /// @brief Returns all fixed friction patches
    FixedFrictionPatches GetFixedFrictionPatches() const;

    const LaneGroups& GetLaneGroups() const { return lane_groups_; }

    const Lanes& GetLanes() const { return lanes_; }

    Lanes& GetLanes() { return lanes_; }

    std::vector<const LaneBoundary*> GetLeftLaneBoundaries(const Lane* lane) const;
    std::vector<const LaneBoundary*> GetRightLaneBoundaries(const Lane* lane) const;

    const LaneBoundaries& GetLaneBoundaries() const { return lane_boundaries_; }

    Lane& GetLane(mantle_api::UniqueId lane_id);

    const Lane& GetLane(mantle_api::UniqueId lane_id) const;

    Lane* FindLane(mantle_api::UniqueId lane_id);

    const Lane* FindLane(mantle_api::UniqueId lane_id) const;

    LaneBoundary& GetLaneBoundary(mantle_api::UniqueId lane_boundary_id);

    const LaneBoundary& GetLaneBoundary(mantle_api::UniqueId lane_boundary_id) const;

    LaneBoundary* FindLaneBoundary(mantle_api::UniqueId lane_boundary_id);

    const LaneBoundary* FindLaneBoundary(mantle_api::UniqueId lane_boundary_id) const;

    LaneGroup& GetLaneGroup(mantle_api::UniqueId lane_group_id);

    bool ContainsLane(mantle_api::UniqueId lane_id) const;

    bool ContainsLaneBoundary(mantle_api::UniqueId lane_boundary_id) const;

    bool ContainsLaneGroup(mantle_api::UniqueId lane_group_id) const;

    /// @brief Helper function to search a given lane group by id, used in all Get/Find-Lane/LaneBoundary functions
    const LaneGroup* FindLaneGroup(mantle_api::UniqueId id) const;

    /// @copydoc FindLaneGroup
    LaneGroup* FindLaneGroup(mantle_api::UniqueId id);

    LaneGroup& AddLaneGroup(const LaneGroup& lane_group);

    void AddLane(mantle_api::UniqueId lane_group_id, const Lane& lane);

    void AddLanes(mantle_api::UniqueId lane_group_id, const Lanes& lanes);

    /// @brief Adds lane boundary to GTGen map. Lane boundary can be added to only one lane.
    /// @param lane_group_id Lane id to which boundary will be assigned to
    /// @param lane_boundary GTGen lane boundary to be added
    void AddLaneBoundary(mantle_api::UniqueId lane_group_id, const LaneBoundary& lane_boundary);

    /// @brief Adds barrier lane boundary. Barrier lane boundaries are allowed to be assigned to multiple GTGen lanes.
    /// @param lane_ids Lane ids to which boundary will be assigned to
    /// @param barrier_lane_boundary GTGen lane boundary to be added
    /// @param is_left_boundary True if left boundary, false if right boundary
    void AddBarrierLaneBoundary(const std::vector<mantle_api::UniqueId>& lane_ids,
                                const LaneBoundary& barrier_lane_boundary,
                                bool is_left_boundary);

    void AddLaneBoundaries(mantle_api::UniqueId lane_group_id, const LaneBoundaries& lane_boundaries);

    double GetFrictionAt(const mantle_api::Vec3<units::length::meter_t>& position) const;

    /// @section Map specific objects
    RoadObjects road_objects{};
    TrafficSigns traffic_signs{};
    TrafficLights traffic_lights{};
    Patches patches{};

    std::string path{};
    std::string projection_string{};
    double reference_point_altitude{0.0};

    BoundingBox2d axis_aligned_world_bounding_box{};

    std::unique_ptr<IConverter<UnderlyingMapCoordinate, mantle_api::Vec3<units::length::meter_t>>> coordinate_converter{
        nullptr};

  private:
    SourceMapType source_map_type_{SourceMapType::kUnknown};
    LaneGroups lane_groups_{};
    Lanes lanes_{};
    LaneBoundaries lane_boundaries_{};
    std::unordered_map<mantle_api::UniqueId, std::size_t> id_lane_index_map_;
    std::unordered_map<mantle_api::UniqueId, std::size_t> id_boundary_index_map_;
    std::unique_ptr<environment::map::SpatialHash> spatial_hash_{nullptr};
};

// TODO: only used in conversion_request_processor - remove this function
inline UnderlyingMapCoordinate GetUnderlyingMapCoordinate(const mantle_api::Position& position)
{
    if (auto odr_pos = std::get_if<mantle_api::OpenDriveLanePosition>(&position))
    {
        return mantle_api::OpenDriveLanePosition{odr_pos->road, odr_pos->lane, odr_pos->s_offset, odr_pos->t_offset};
    }
    else if (auto nds_pos = std::get_if<mantle_api::LatLonPosition>(&position))
    {
        return mantle_api::LatLonPosition{nds_pos->latitude, nds_pos->longitude};
    }

    ASSERT(false && "Could not get underlying map coordinate")
}

}  // namespace gtgen::core::environment::map

namespace fmt
{
template <>
struct formatter<gtgen::core::environment::map::UnderlyingMapCoordinate>
{
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        return ctx.begin();
    }

    template <typename FormatContext>
    // NOLINTNEXTLINE(readability-identifier-naming)
    auto format(const gtgen::core::environment::map::UnderlyingMapCoordinate& coord, FormatContext& ctx)
    {
        if (auto nds_pos = std::get_if<mantle_api::LatLonPosition>(&coord))
        {
            return format_to(ctx.out(), "{}", *nds_pos);
        }
        else if (auto odr_pos = std::get_if<mantle_api::OpenDriveLanePosition>(&coord))
        {
            return format_to(ctx.out(), "{}", *odr_pos);
        }
        return format_to(ctx.out(), "{}", "<unknown position type>");
    }
};

}  // namespace fmt

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_GTGENMAP_GTGENMAP_H
