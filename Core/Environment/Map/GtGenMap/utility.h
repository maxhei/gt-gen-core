/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_GTGENMAP_UTILITY_H
#define GTGEN_CORE_ENVIRONMENT_MAP_GTGENMAP_UTILITY_H

#include <MantleAPI/Common/dimension.h>
#include <MantleAPI/Common/pose.h>

#include <optional>
#include <vector>

namespace gtgen::core::environment::map
{
class GtGenMap;
struct LaneBoundary;

/// @brief Gets extremes of given polyline points.
/// @param polyline_points polyline points which extremes will be calculated on
/// @return polyline points extremes
std::pair<mantle_api::Vec3<units::length::meter_t>, mantle_api::Vec3<units::length::meter_t>> GetPolylinePointsExtremes(
    const std::vector<mantle_api::Vec3<units::length::meter_t>>& polyline_points);

/// @brief Given a line segment and a query point, compute the closest point to the query point that lays on the line
/// segment.
/// @param query_point point for which the closest point on the segment shall be returned
/// @param first point of a line segment
/// @param second point of a line segment
/// @return closest point on line segment
mantle_api::Vec3<units::length::meter_t> ClosestPointOnLineSegment(
    const mantle_api::Vec3<units::length::meter_t>& query_point,
    const mantle_api::Vec3<units::length::meter_t>& start_3d,
    const mantle_api::Vec3<units::length::meter_t>& end_3d);

/// @brief Given a lane boundary and a query point, compute the closest point to the query point that lays on the lane
/// boundary.
/// @param query_point point for which the closest point on the boundary shall be returned
/// @param boundary lane boundary to find the closest point on
/// @return closest point on lane boundary
mantle_api::Vec3<units::length::meter_t> ClosestPointOnBoundary(
    const mantle_api::Vec3<units::length::meter_t>& query_point,
    const LaneBoundary& boundary);

/// @brief Sets z-coordinate of every point to a given value.
/// @param z_value value that will be set as z value for all points
/// @param points vector of coordinate points
void SetZValueForPoints(double z_value, std::vector<mantle_api::Vec3<units::length::meter_t>>& points);

/// @brief Connect right and left polylines in counterclockwise direction. Right polyline will be placed first and left
/// polyline will be reversed.
/// @param right_polyline right polyline
/// @param left_polyline left polyline
/// @return Connected polylines
std::vector<mantle_api::Vec3<units::length::meter_t>> ConnectPolylinesCounterclockwise(
    std::vector<mantle_api::Vec3<units::length::meter_t>> right_polyline,
    std::vector<mantle_api::Vec3<units::length::meter_t>> left_polyline);

/// @brief Returns yaw of a vector defined by two points
/// @param start_point start point of a vector
/// @param end_point  end point of a vector
/// @return Calculated yaw of a given vector
double CalculateVectorYaw(const mantle_api::Vec3<units::length::meter_t>& start_point,
                          const mantle_api::Vec3<units::length::meter_t>& end_point);

/// @brief Projects start and end points to polyline and trims polyline to fit in range [projected_start, projected_end]
/// @param polyline polyline to trim
/// @param start_point point from which to trim
/// @param end_point point to trim to
/// @return Trimmed polyline
std::vector<mantle_api::Vec3<units::length::meter_t>> TrimPolylineToRange(
    const std::vector<mantle_api::Vec3<units::length::meter_t>>& polyline,
    const mantle_api::Vec3<units::length::meter_t>& start_point,
    const mantle_api::Vec3<units::length::meter_t>& end_point);

/// @brief @brief calculates object dimensions from given 3D shape extremes
/// @param shape_extremes extremes of a points that outline an object
/// @return dimensions of a object
mantle_api::Dimension3 CalculateObjectDimensions(
    const std::pair<mantle_api::Vec3<units::length::meter_t>, mantle_api::Vec3<units::length::meter_t>>&
        shape_extremes);

/// @brief calculates object dimensions from given 2D shape extremes, sets height to a given value
/// @param shape_extremes extremes of a points that outline an object
/// @param height that will be set directly
/// @return dimensions of a object
mantle_api::Dimension3 CalculateObjectDimensions(
    const std::pair<mantle_api::Vec3<units::length::meter_t>, mantle_api::Vec3<units::length::meter_t>>& shape_extremes,
    double height);

}  // namespace gtgen::core::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_GTGENMAP_UTILITY_H
