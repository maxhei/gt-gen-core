/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_COMMON_CONVERTERUTILITY_H
#define GTGEN_CORE_ENVIRONMENT_MAP_COMMON_CONVERTERUTILITY_H

#include "Core/Environment/Map/Common/coordinate_converter.h"

#include <MantleAPI/Common/position.h>

#include <variant>

namespace gtgen::core::environment::map
{

using UnderlyingMapCoordinate = std::variant<mantle_api::OpenDriveLanePosition, mantle_api::LatLonPosition>;

/// @brief Converts an GTGen coordinate vector into the corresponding map coordinate and returns it as formatted string
/// @return Formatted map coordinate if available, "not available" otherwise
std::string GetMapCoordinateString(
    const mantle_api::Vec3<units::length::meter_t>& world_coordinate,
    const IConverter<UnderlyingMapCoordinate, mantle_api::Vec3<units::length::meter_t>>* coordinate_converter);

}  // namespace gtgen::core::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_COMMON_CONVERTERUTILITY_H
