/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_GEOMETRY_SCALARPROJECTION2D_H
#define GTGEN_CORE_ENVIRONMENT_MAP_GEOMETRY_SCALARPROJECTION2D_H

#include <MantleAPI/Common/vector.h>

#include <optional>

namespace gtgen::core::environment::map
{

std::optional<double> ScalarProjection2d(mantle_api::Vec3<units::length::meter_t> query_point_2d,
                                         mantle_api::Vec3<units::length::meter_t> start_2d,
                                         mantle_api::Vec3<units::length::meter_t> end_2d);

}  // namespace gtgen::core::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_GEOMETRY_SCALARPROJECTION2D_H
