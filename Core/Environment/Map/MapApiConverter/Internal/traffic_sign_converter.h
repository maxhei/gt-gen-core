/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_INTERNAL_TRAFFICSIGNCONVERTER_H
#define GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_INTERNAL_TRAFFICSIGNCONVERTER_H

#include "Core/Environment/Map/GtGenMap/gtgen_map.h"

#include <MapAPI/traffic_sign.h>

namespace gtgen::core::environment::map
{

SignValueInformation ConvertTrafficSignValue(const map_api::TrafficSignValue& from);
TrafficSign ConvertMainSign(const map_api::MainSign& from, const map_api::Identifier id);
MountedSign::SupplementarySign ConvertSupplementarySign(const map_api::SupplementarySign& from,
                                                        MountedSign* main_sign = nullptr);

void FillTrafficSign(const map_api::TrafficSign& from, GtGenMap& map);

}  // namespace gtgen::core::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_INTERNAL_TRAFFICSIGNCONVERTER_H
