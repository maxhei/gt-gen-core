/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_INTERNAL_ROADMARKINGCONVERTER_H
#define GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_INTERNAL_ROADMARKINGCONVERTER_H

#include "Core/Environment/Map/GtGenMap/gtgen_map.h"
#include "Core/Environment/Map/MapApiConverter/Internal/traffic_sign_types_converter.h"

#include <MapAPI/road_marking.h>

namespace gtgen::core::environment::map
{

GroundSign ConvertRoadMarking(const map_api::RoadMarking& from);

}  // namespace gtgen::core::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_INTERNAL_ROADMARKINGCONVERTER_H
