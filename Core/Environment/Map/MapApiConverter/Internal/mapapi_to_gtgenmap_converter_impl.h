/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_INTERNAL_MAPAPITOGTGENMAPCONVERTERIMPL_H
#define GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_INTERNAL_MAPAPITOGTGENMAPCONVERTERIMPL_H

#include "Core/Environment/Map/Common/i_any_to_gtgenmap_converter.h"
#include "Core/Environment/Map/GtGenMap/gtgen_map.h"
#include "Core/Environment/Map/GtGenMap/gtgen_map_finalizer.h"
#include "Core/Service/Utility/unique_id_provider.h"

#include <MapAPI/map.h>

#include <memory>
#include <vector>

namespace gtgen::core::environment::map
{

class MapApiToGtGenMapConverterImpl : public IAnyToGtGenMapConverter
{
  public:
    MapApiToGtGenMapConverterImpl(service::utility::UniqueIdProvider& id_provider,
                                  const map_api::Map& data,
                                  GtGenMap& gtgen_map);

    MapApiToGtGenMapConverterImpl() = delete;
    MapApiToGtGenMapConverterImpl(const MapApiToGtGenMapConverterImpl&) = delete;
    MapApiToGtGenMapConverterImpl(MapApiToGtGenMapConverterImpl&&) = delete;
    MapApiToGtGenMapConverterImpl& operator=(const MapApiToGtGenMapConverterImpl&) = delete;
    MapApiToGtGenMapConverterImpl& operator=(MapApiToGtGenMapConverterImpl&&) = delete;
    ~MapApiToGtGenMapConverterImpl() override;

    /// @copydoc IAnyToGtGenMapConverter::Convert()
    void Convert() override;

    /// @copydoc IAnyToGtGenMapConverter::GetNativeToGtGenTrafficLightIdMap()
    std::map<mantle_api::UniqueId, mantle_api::UniqueId> GetNativeToGtGenTrafficLightIdMap() const override;

  private:
    service::utility::UniqueIdProvider& unique_id_provider_;
    const map_api::Map& data_;
    environment::map::GtGenMap& gtgen_map_;
};

}  // namespace gtgen::core::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_INTERNAL_MAPAPITOGTGENMAPCONVERTERIMPL_H
