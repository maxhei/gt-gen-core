/*******************************************************************************
 * Copyright (c) 2022-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_GTGENENVIRONMENT_INTERNAL_ENTITYPRODUCER_H
#define GTGEN_CORE_ENVIRONMENT_GTGENENVIRONMENT_INTERNAL_ENTITYPRODUCER_H

#include "Core/Service/Osi/traffic_sign_types.h"

#include <MantleAPI/Traffic/i_entity_repository.h>

namespace gtgen::core::environment::api
{

class EntityProducer
{
  public:
    std::unique_ptr<mantle_api::IEntity> Produce(mantle_api::UniqueId id,
                                                 const std::string& name,
                                                 const mantle_api::VehicleProperties& properties) const;
    std::unique_ptr<mantle_api::IEntity> Produce(mantle_api::UniqueId id,
                                                 const std::string& name,
                                                 const mantle_api::PedestrianProperties& properties) const;
    std::unique_ptr<mantle_api::IEntity> Produce(
        mantle_api::UniqueId id,
        const std::string& name,
        const mantle_api::StaticObjectProperties& static_object_properties) const;

  private:
    void SetUserDefinedProperties(mantle_api::StaticObjectProperties* static_object_properties,
                                  const mantle_api::StaticObjectProperties& user_defined_object_properties) const;

    std::optional<osi::OsiTrafficSignValueUnit> GetUnitFromString(const std::string& unit_string) const;
};

}  // namespace gtgen::core::environment::api

#endif  // GTGEN_CORE_ENVIRONMENT_GTGENENVIRONMENT_INTERNAL_ENTITYPRODUCER_H
