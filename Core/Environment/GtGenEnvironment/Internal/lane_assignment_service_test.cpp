/*******************************************************************************
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GtGenEnvironment/Internal/lane_assignment_service.h"

#include "Core/Environment/Controller/Internal/ControlUnits/i_abstract_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/traffic_participant_control_unit.h"
#include "Core/Environment/Controller/composite_controller.h"
#include "Core/Environment/Controller/external_controller_config.h"
#include "Core/Environment/Entities/Internal/base_entity.h"
#include "Core/Environment/Exception/exception.h"
#include "Core/Environment/GtGenEnvironment/Internal/active_controller_repository.h"
#include "Core/Environment/GtGenEnvironment/entity_repository.h"
#include "Core/Environment/Map/LaneLocationProvider/lane_location_provider.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"

#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Test/test_utils.h>
#include <MantleAPI/Traffic/entity_properties.h>
#include <MantleAPI/Traffic/i_controller.h>
#include <gtest/gtest.h>

using units::literals::operator""_m;

namespace gtgen::core::environment::api
{

using namespace gtgen::core::environment::controller;

// Doesn't implement BaseEntity.
// Should cause an exception.
using MockIEntity = mantle_api::MockPedestrian;

class MockBaseEntity : public gtgen::core::environment::entities::BaseEntity
{
  public:
    MockBaseEntity(mantle_api::UniqueId id, std::string name)
        : gtgen::core::environment::entities::BaseEntity{id, name} {};
    MOCK_METHOD(bool, AreAssignedLanesDirty, (), (override));
    MOCK_METHOD(void, SetAssignedLaneIds, (const std::vector<std::uint64_t>&), (override));
};

// Need to avoid CompositeController::AddControlUnit.
class FakeCompositeController : public CompositeController
{
  public:
    FakeCompositeController(mantle_api::UniqueId id,
                            mantle_api::ILaneLocationQueryService* lane_location_query_service,
                            CompositeController::Type type = CompositeController::Type::kUndefined)
        : CompositeController(id, lane_location_query_service, type){};

    // Create fake TrafficParticipantControlUnit.
    void AddTrafficParticipantControlUnit()
    {
        std::map<std::string, std::string> tpm_params{{"velocity", "22"}};

        controller::TrafficParticipantControlUnitConfig tpm_control_unit_config{};
        gtgen_map = std::move(test_utils::MapCatalogue::MapWithOneNorthEastingLaneWithNPoints(2, 1));
        tpm_control_unit_config.gtgen_map = gtgen_map.get();
        tpm_control_unit_config.name = "traffic_participant_model_example";

        std::unique_ptr<TrafficParticipantControlUnit> traffic_participant_control_unit =
            std::make_unique<TrafficParticipantControlUnit>(tpm_params, tpm_control_unit_config);
        control_units_.push_back(std::move(traffic_participant_control_unit));
    };

    void AddAbstractControlUnit()
    {
        // Function dynamic_cast returns nullptr if null pointer is input.
        std::unique_ptr<IAbstractControlUnit> traffic_participant_control_unit(nullptr);
        control_units_.push_back(std::move(traffic_participant_control_unit));
    };

    void SetEntity(mantle_api::IEntity& entity) override { CompositeController::entity_ = &entity; };

  protected:
    std::unique_ptr<map::GtGenMap> gtgen_map;
};

class FakeEntityRepository : public EntityRepository
{
  public:
    FakeEntityRepository(service::utility::UniqueIdProvider* id_provider,
                         ActiveControllerRepository& active_controller_repository)
        : EntityRepository{id_provider},
          id_provider_{id_provider},
          active_controller_repository_{active_controller_repository} {};

    void CreateFakeBaseEntity()
    {
        fake_entities_.push_back(std::make_unique<gtgen::core::environment::entities::BaseEntity>(
            id_provider_->GetUniqueId(), "fake_entity"));
        std::unique_ptr<mantle_api::EntityProperties> properties = std::make_unique<mantle_api::VehicleProperties>();
        fake_entities_.back()->SetProperties(std::move(properties));
    };

    void CreateMockIEntity()
    {
        fake_entities_.push_back(std::make_unique<MockIEntity>());
        std::unique_ptr<mantle_api::EntityProperties> properties = std::make_unique<mantle_api::VehicleProperties>();
        fake_entities_.back()->SetProperties(std::move(properties));
    };

    void CreateMockBaseEntity()
    {
        fake_entities_.push_back(std::make_unique<MockBaseEntity>(id_provider_->GetUniqueId(), "mock_entity"));
        std::unique_ptr<mantle_api::EntityProperties> properties = std::make_unique<mantle_api::VehicleProperties>();
        fake_entities_.back()->SetProperties(std::move(properties));
    }

    std::optional<std::reference_wrapper<mantle_api::IEntity>> Get(mantle_api::UniqueId id) override
    {
        std::ignore = id;
        return *fake_entities_.back();
    };

    const std::vector<std::unique_ptr<mantle_api::IEntity>>& GetEntities() const override { return fake_entities_; };

  private:
    std::vector<std::unique_ptr<mantle_api::IEntity>> fake_entities_;
    service::utility::UniqueIdProvider* id_provider_;
    ActiveControllerRepository& active_controller_repository_;
};

class LaneAssignmentServiceWithMockEntityTest : public testing::Test
{
  public:
    LaneAssignmentServiceWithMockEntityTest() : fake_entitiy_repository_{&id_provider_, active_controller_repository_}
    {
        lane_location_provider_ = std::make_unique<map::LaneLocationProvider>(*map_);
    }

  protected:
    std::unique_ptr<environment::map::GtGenMap> map_{
        test_utils::MapCatalogue::MapWithOneEastingLaneWithHundredPoints()};
    std::unique_ptr<map::LaneLocationProvider> lane_location_provider_;
    service::utility::UniqueIdProvider id_provider_{};
    ActiveControllerRepository active_controller_repository_{};
    FakeEntityRepository fake_entitiy_repository_;
};

TEST_F(LaneAssignmentServiceWithMockEntityTest, GivenLaneAssignmentServiceAndBaseEntity_WhenStep_ThenNoException)
{
    fake_entitiy_repository_.CreateFakeBaseEntity();
    LaneAssignmentService lane_assignment_service{fake_entitiy_repository_, active_controller_repository_};
    lane_assignment_service.SetIsEntityAllowedToLeaveLane(true);
    lane_assignment_service.SetLaneLocationProvider(lane_location_provider_.get());
    EXPECT_NO_THROW(lane_assignment_service.Step());
}

TEST_F(LaneAssignmentServiceWithMockEntityTest, GivenLaneAssignmentServiceAndNotBaseEntity_WhenStep_ThenException)
{
    fake_entitiy_repository_.CreateMockIEntity();
    LaneAssignmentService lane_assignment_service{fake_entitiy_repository_, active_controller_repository_};
    lane_assignment_service.SetLaneLocationProvider(lane_location_provider_.get());
    EXPECT_THROW(lane_assignment_service.Step(), std::runtime_error);
}

TEST_F(LaneAssignmentServiceWithMockEntityTest,
       GivenLaneAssignmentServiceAndAssignedLanesDirty_WhenStep_ThenSetAssignedLaneIdsCalled)
{
    fake_entitiy_repository_.CreateMockBaseEntity();
    LaneAssignmentService lane_assignment_service{fake_entitiy_repository_, active_controller_repository_};
    lane_assignment_service.SetLaneLocationProvider(lane_location_provider_.get());
    mantle_api::IEntity& entity = fake_entitiy_repository_.Get(0).value().get();
    MockBaseEntity& mock_base_entity = dynamic_cast<MockBaseEntity&>(entity);
    mock_base_entity.SetPosition({50_m, 0_m, 0_m});
    EXPECT_CALL(mock_base_entity, AreAssignedLanesDirty()).WillRepeatedly(testing::Return(true));
    EXPECT_CALL(mock_base_entity, SetAssignedLaneIds(testing::_));
    lane_assignment_service.Step();
}

TEST_F(LaneAssignmentServiceWithMockEntityTest,
       GivenLaneAssignmentServiceAndAssignedLanesNotDirty_WhenStep_ThenSetAssignedLaneIdsNotCalled)
{
    fake_entitiy_repository_.CreateMockBaseEntity();
    LaneAssignmentService lane_assignment_service{fake_entitiy_repository_, active_controller_repository_};
    lane_assignment_service.SetLaneLocationProvider(lane_location_provider_.get());
    mantle_api::IEntity& entity = fake_entitiy_repository_.Get(0).value().get();
    MockBaseEntity& mock_base_entity = dynamic_cast<MockBaseEntity&>(entity);
    mock_base_entity.SetPosition({50_m, 0_m, 0_m});
    EXPECT_CALL(mock_base_entity, AreAssignedLanesDirty()).WillRepeatedly(testing::Return(false));
    EXPECT_CALL(mock_base_entity, SetAssignedLaneIds(testing::_)).Times(0);
    lane_assignment_service.Step();
}

class LaneAssignmentServiceTest : public testing::Test
{
  public:
    enum class TestSets
    {
        EntityOutOfLane,
        LaneAssignmentPosition,
        LaneAssignmentParameter,
    };

    struct EntityInfo
    {
        std::string name;
        mantle_api::EntityProperties entitiy_property;
        mantle_api::Vec3<units::length::meter_t> position;
        bool assigned_lanes_dirty;
        std::vector<std::uint64_t> expected_lane_ids;
        std::optional<std::string> lane_assignment_parameter;
    };

    LaneAssignmentServiceTest() : entity_repository_{&id_provider_} {};

    void SetUp(TestSets set,
               std::optional<CompositeController::Type> controller_type = std::nullopt,
               bool is_tpc = false)
    {
        std::unique_ptr<environment::map::GtGenMap> map;
        switch (set)
        {
            case TestSets::EntityOutOfLane:
            {
                map = std::move(test_utils::MapCatalogue::MapWithThreeConnectedEastingLanesWithHundredPointsEach());
                FillEntitiesOutOfLane();
                break;
            }
            case TestSets::LaneAssignmentPosition:
            {
                map = std::move(test_utils::MapCatalogue::MapWithThreeConnectedEastingLanesWithHundredPointsEach());
                FillEntitiesInfoPosition();
                break;
            }
            case TestSets::LaneAssignmentParameter:
            {
                map = std::move(test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints());
                FillEntitiesInfoParams();
                break;
            }
        }

        map_ = std::move(map);
        lane_location_provider_ = std::make_unique<map::LaneLocationProvider>(*map_);
        SetEntities();

        if (controller_type.has_value())
        {
            for (auto& entity : entity_repository_.GetEntities())
            {
                std::unique_ptr<FakeCompositeController> composite_controller =
                    std::make_unique<FakeCompositeController>(entity->GetUniqueId(), nullptr, controller_type.value());
                composite_controller->ChangeState(mantle_api::IController::LateralState::kActivate,
                                                  mantle_api::IController::LongitudinalState::kActivate);
                if (controller_type.value() == CompositeController::Type::kExternal)
                {
                    if (is_tpc)
                    {
                        composite_controller->AddTrafficParticipantControlUnit();
                    }
                    else
                    {
                        composite_controller->AddAbstractControlUnit();
                    }
                }
                active_controller_repository_.AddEntityToController(
                    &entity_repository_.Get(entity->GetUniqueId()).value().get(), std::move(composite_controller));
            }
        }
    }

    void SetEntities()
    {
        for (const auto& info : entities_infos_)
        {
            auto& entity = entity_repository_.Create(info.name, mantle_api::VehicleProperties{});
            entity.SetAssignedLaneIds(default_lane_ids_);
            std::unique_ptr<mantle_api::EntityProperties> properties =
                std::make_unique<mantle_api::VehicleProperties>();
            if (info.lane_assignment_parameter.has_value())
            {
                properties->properties["lane_assignment"] = info.lane_assignment_parameter.value();
            }
            if (info.assigned_lanes_dirty)
            {
                entity.SetPosition(info.position);
            }

            entity.SetProperties(std::move(properties));
        }
    }

    void FillEntitiesInfoPosition()
    {
        // name, entitiy_property, position, assigned_lanes_dirty, expected_lane_ids,
        // lane_assignment_parameter (see MapWithThreeConnectedEastingLanesWithHundredPointsEach)
        entities_infos_.push_back(
            EntityInfo{"entity1", mantle_api::VehicleProperties{}, {50_m, 0_m, 0_m}, true, {3}, std::nullopt});
        entities_infos_.push_back(EntityInfo{
            "entity2", mantle_api::VehicleProperties{}, {1000_m, 1000_m, 0_m}, true, {1000, 2000, 3000}, std::nullopt});
        entities_infos_.push_back(EntityInfo{
            "entity3", mantle_api::PedestrianProperties{}, {150_m, 0_m, 0_m}, false, {1000, 2000, 3000}, std::nullopt});
        entities_infos_.push_back(
            EntityInfo{"entity4", mantle_api::StaticObjectProperties{}, {250_m, 0_m, 0_m}, true, {11}, std::nullopt});
        entities_infos_.push_back(
            EntityInfo{"entity5", mantle_api::StaticObjectProperties{}, {250_m, 0_m, 0_m}, true, {11}, {""}});
    }

    void FillEntitiesInfoParams()
    {
        // name, entitiy_property, position, assigned_lanes_dirty, expected_lane_ids,
        // lane_assignment_parameter (see Map3x3StraightEastingLanesWithHundredPoints)
        entities_infos_.push_back(
            EntityInfo{"entity1", mantle_api::VehicleProperties{}, {50_m, 0_m, 0_m}, true, {5, 8}, {"-2, -1, -4"}});
        entities_infos_.push_back(EntityInfo{
            "entity2", mantle_api::PedestrianProperties{}, {150_m, 0_m, 0_m}, false, {1000, 2000, 3000}, {"-3, -1"}});
        entities_infos_.push_back(
            EntityInfo{"entity3", mantle_api::StaticObjectProperties{}, {250_m, 0_m, 0_m}, true, {25, 28}, {"-2, -1"}});
        entities_infos_.push_back(EntityInfo{
            "entity4", mantle_api::StaticObjectProperties{}, {250_m, 0_m, 0_m}, true, {1000, 2000, 3000}, {"-4, -5"}});
        entities_infos_.push_back(
            EntityInfo{"entity5", mantle_api::StaticObjectProperties{}, {250_m, 0_m, 0_m}, true, {22}, {""}});
    }

    void FillEntitiesOutOfLane()
    {
        // name, entitiy_property, position, assigned_lanes_dirty, expected_lane_ids,
        // lane_assignment_parameter (see MapWithThreeConnectedEastingLanesWithHundredPointsEach)
        entities_infos_.push_back(EntityInfo{
            "entity1", mantle_api::VehicleProperties{}, {1000_m, 1000_m, 0_m}, true, {1000, 2000, 3000}, std::nullopt});
    }

  protected:
    std::unique_ptr<environment::map::GtGenMap> map_;
    std::unique_ptr<map::LaneLocationProvider> lane_location_provider_;
    EntityRepository entity_repository_;
    ActiveControllerRepository active_controller_repository_{};
    std::vector<EntityInfo> entities_infos_;

    service::utility::UniqueIdProvider id_provider_{};
    std::vector<std::uint64_t> default_lane_ids_{1000, 2000, 3000};
};

TEST_F(LaneAssignmentServiceTest,
       GivenLaneAssignmentServiceAndNoActiveControllersAndNotAllowedToLeaveLane_WhenStep_ThenNoException)
{
    SetUp(TestSets::EntityOutOfLane);
    LaneAssignmentService lane_assignment_service{entity_repository_, active_controller_repository_};
    lane_assignment_service.SetLaneLocationProvider(lane_location_provider_.get());
    lane_assignment_service.SetIsEntityAllowedToLeaveLane(false);
    EXPECT_NO_THROW(lane_assignment_service.Step());
}

TEST_F(LaneAssignmentServiceTest, GivenLaneAssignmentServiceAndNoActiveControllers_WhenStep_ThenLaneIdsAreCorrect)
{
    SetUp(TestSets::LaneAssignmentPosition);
    LaneAssignmentService lane_assignment_service{entity_repository_, active_controller_repository_};
    lane_assignment_service.SetLaneLocationProvider(lane_location_provider_.get());
    lane_assignment_service.Step();
    for (const auto& info : entities_infos_)
    {
        const auto entity = entity_repository_.Get(info.name);
        EXPECT_EQ(entity.value().get().GetAssignedLaneIds(), info.expected_lane_ids);
    }
}

TEST_F(LaneAssignmentServiceTest, GivenLaneAssignmentServiceAndNoActiveControllers_WhenStep_ThenAssignedLanesDirtyReset)
{
    SetUp(TestSets::LaneAssignmentPosition);
    LaneAssignmentService lane_assignment_service{entity_repository_, active_controller_repository_};
    lane_assignment_service.SetLaneLocationProvider(lane_location_provider_.get());
    lane_assignment_service.Step();
    for (const auto& info : entities_infos_)
    {
        mantle_api::IEntity& entity = entity_repository_.Get(info.name).value().get();
        gtgen::core::environment::entities::BaseEntity& base_entitiy =
            dynamic_cast<gtgen::core::environment::entities::BaseEntity&>(entity);
        EXPECT_FALSE(base_entitiy.AreAssignedLanesDirty());
    }
}

TEST_F(LaneAssignmentServiceTest, GivenLaneAssignmentServiceAndLaneAssignmentProperty_WhenStep_ThenLaneIdsAreCorrect)
{
    SetUp(TestSets::LaneAssignmentParameter);
    LaneAssignmentService lane_assignment_service{entity_repository_, active_controller_repository_};
    lane_assignment_service.SetLaneLocationProvider(lane_location_provider_.get());
    lane_assignment_service.Step();
    for (const auto& info : entities_infos_)
    {
        const auto entity = entity_repository_.Get(info.name);
        EXPECT_EQ(entity.value().get().GetAssignedLaneIds(), info.expected_lane_ids);
    }
}

TEST_F(
    LaneAssignmentServiceTest,
    GivenLaneAssignmentServiceAndInternalControllerAndIsNotAllowedToLeaveLaneAndEntityOutOfLane_WhenStep_ThenException)
{
    SetUp(TestSets::EntityOutOfLane, CompositeController::Type::kInternal);
    LaneAssignmentService lane_assignment_service{entity_repository_, active_controller_repository_};
    lane_assignment_service.SetIsEntityAllowedToLeaveLane(false);
    lane_assignment_service.SetLaneLocationProvider(lane_location_provider_.get());
    EXPECT_THROW(lane_assignment_service.Step(), gtgen::core::environment::EnvironmentException);
}

TEST_F(
    LaneAssignmentServiceTest,
    GivenLaneAssignmentServiceAndInternalControllerAndIsAllowedToLeaveLaneAndEntityOutOfLane_WhenStep_ThenNoException)
{
    SetUp(TestSets::EntityOutOfLane, CompositeController::Type::kInternal);
    LaneAssignmentService lane_assignment_service{entity_repository_, active_controller_repository_};
    lane_assignment_service.SetIsEntityAllowedToLeaveLane(true);
    lane_assignment_service.SetLaneLocationProvider(lane_location_provider_.get());
    EXPECT_NO_THROW(lane_assignment_service.Step());
}

TEST_F(LaneAssignmentServiceTest,
       GivenLaneAssignmentServiceAndExternalControllerAndNotTPCAndEntityOutOfLane_WhenStep_ThenException)
{
    SetUp(TestSets::EntityOutOfLane, CompositeController::Type::kExternal);
    LaneAssignmentService lane_assignment_service{entity_repository_, active_controller_repository_};
    lane_assignment_service.SetLaneLocationProvider(lane_location_provider_.get());
    lane_assignment_service.SetIsEntityAllowedToLeaveLane(true);
    EXPECT_THROW(lane_assignment_service.Step(), gtgen::core::environment::EnvironmentException);
}

TEST_F(LaneAssignmentServiceTest,
       GivenLaneAssignmentServiceAndExternalControllerAndTPCAndEntityOutOfLane_WhenStep_ThenNoException)
{
    SetUp(TestSets::EntityOutOfLane, CompositeController::Type::kExternal, true);
    LaneAssignmentService lane_assignment_service{entity_repository_, active_controller_repository_};
    lane_assignment_service.SetLaneLocationProvider(lane_location_provider_.get());
    EXPECT_NO_THROW(lane_assignment_service.Step());
}

}  // namespace gtgen::core::environment::api
