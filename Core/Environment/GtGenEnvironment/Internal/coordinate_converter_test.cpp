/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GtGenEnvironment/Internal/coordinate_converter.h"

#include "Core/Environment/Map/Common/coordinate_converter.h"
#include "Core/Tests/TestUtils/expect_extensions.h"

#include <MantleAPI/Common/position.h>
#include <gtest/gtest.h>
#include <units.h>

namespace gtgen::core::environment::api
{
using units::literals::operator""_m;
using units::literals::operator""_deg;

mantle_api::Vec3<units::length::meter_t>& GetExpectedWorldPosition()
{
    static mantle_api::Vec3<units::length::meter_t> expected_world_position{1.0_m, 2.0_m, 3.0_m};
    return expected_world_position;
}

class DummyOdrCoordConverter : public Base
{
  public:
    using Base::Convert;

    std::optional<mantle_api::Vec3<units::length::meter_t>> Convert(
        [[maybe_unused]] const UnderlyingMapCoordinate& coordinate) const override
    {
        if (std::holds_alternative<mantle_api::OpenDriveLanePosition>(coordinate))
        {
            return GetExpectedWorldPosition();
        }
        else
        {
            return std::nullopt;
        }
    };

    std::optional<UnderlyingMapCoordinate> Convert(
        [[maybe_unused]] const mantle_api::Vec3<units::length::meter_t>& coordinate) const override
    {
        return UnderlyingMapCoordinate{};
    };
};

TEST(OdrCoordinateConverterTest, GivenOdrCoordConverter_WhenConvertingFromOdrToWorldPos_ThenConversionSucceeds)
{
    mantle_api::OpenDriveLanePosition odr_pos{"0", 1, 1.028_m, -0.061_m};
    const auto expected_world_position = GetExpectedWorldPosition();

    auto underlying_converter = std::make_unique<DummyOdrCoordConverter>();
    auto coordinate_converter = std::make_unique<CoordinateConverter>(underlying_converter.get());

    auto converted_world_pos = coordinate_converter->Convert(odr_pos);

    EXPECT_EQ(expected_world_position.x, converted_world_pos.x);
    EXPECT_EQ(expected_world_position.y, converted_world_pos.y);
    EXPECT_EQ(expected_world_position.z, converted_world_pos.z);
}

TEST(OdrCoordinateConverterTest, GivenOdrCoordConverter_WhenConvertingFromWorldToWorldPos_ThenTheInputValueReturned)
{
    mantle_api::Vec3<units::length::meter_t> expected_world_position{4.0_m, 5.0_m, 6.0_m};

    auto underlying_converter = std::make_unique<DummyOdrCoordConverter>();
    auto coordinate_converter = std::make_unique<CoordinateConverter>(underlying_converter.get());

    auto converted_world_pos = coordinate_converter->Convert(expected_world_position);

    EXPECT_EQ(expected_world_position.x, converted_world_pos.x);
    EXPECT_EQ(expected_world_position.y, converted_world_pos.y);
    EXPECT_EQ(expected_world_position.z, converted_world_pos.z);
}

TEST(OdrCoordinateConverterTest, GivenOdrCoordConverter_WhenConvertingFromLatLonToWorldPos_ThenDefaultValueReturned)
{
    mantle_api::LatLonPosition lat_lon_pos{48.2210194599_deg, 11.7314353120_deg};
    mantle_api::Vec3<units::length::meter_t> expected_world_position{-1.0_m, -1.0_m, -1.0_m};

    auto underlying_converter = std::make_unique<DummyOdrCoordConverter>();
    auto coordinate_converter = std::make_unique<CoordinateConverter>(underlying_converter.get());

    auto converted_world_pos = coordinate_converter->Convert(lat_lon_pos);

    EXPECT_EQ(expected_world_position.x, converted_world_pos.x);
    EXPECT_EQ(expected_world_position.y, converted_world_pos.y);
    EXPECT_EQ(expected_world_position.z, converted_world_pos.z);
}

class DummyLatLonCoordConverter : public Base
{
  public:
    using Base::Convert;

    std::optional<mantle_api::Vec3<units::length::meter_t>> Convert(
        [[maybe_unused]] const UnderlyingMapCoordinate& coordinate) const override
    {
        if (std::holds_alternative<mantle_api::LatLonPosition>(coordinate))
        {
            return GetExpectedWorldPosition();
        }
        else
        {
            return std::nullopt;
        }
    };

    std::optional<UnderlyingMapCoordinate> Convert(
        [[maybe_unused]] const mantle_api::Vec3<units::length::meter_t>& coordinate) const override
    {
        return UnderlyingMapCoordinate{};
    };
};

TEST(LatLonCoordinateConverterTest, GivenLatLonConverter_WhenConvertingFromLatLonToWorldPos_ThenConversionSucceeds)
{
    mantle_api::LatLonPosition lat_lon_pos{48.2210194599_deg, 11.7314353120_deg};
    const auto expected_world_position = GetExpectedWorldPosition();

    auto underlying_converter = std::make_unique<DummyLatLonCoordConverter>();
    auto coordinate_converter = std::make_unique<CoordinateConverter>(underlying_converter.get());

    auto converted_world_pos = coordinate_converter->Convert(lat_lon_pos);

    EXPECT_EQ(expected_world_position.x, converted_world_pos.x);
    EXPECT_EQ(expected_world_position.y, converted_world_pos.y);
    EXPECT_EQ(expected_world_position.z, converted_world_pos.z);
}

TEST(LatLonCoordinateConverterTest, GivenLatLonConverter_WhenConvertingFromWorldToWorldPos_ThenTheInputValueReturned)
{
    mantle_api::Vec3<units::length::meter_t> expected_world_position{4.0_m, 5.0_m, 6.0_m};

    auto underlying_converter = std::make_unique<DummyLatLonCoordConverter>();
    auto coordinate_converter = std::make_unique<CoordinateConverter>(underlying_converter.get());

    auto converted_world_pos = coordinate_converter->Convert(expected_world_position);

    EXPECT_EQ(expected_world_position.x, converted_world_pos.x);
    EXPECT_EQ(expected_world_position.y, converted_world_pos.y);
    EXPECT_EQ(expected_world_position.z, converted_world_pos.z);
}

TEST(LatLonCoordinateConverterTest, GivenLatLonConverter_WhenConvertingFromOdrToWorldPos_ThenDefaultValueReturned)
{
    mantle_api::OpenDriveLanePosition odr_pos{"0", 1, 1.028_m, -0.061_m};
    mantle_api::Vec3<units::length::meter_t> expected_world_position{-1.0_m, -1.0_m, -1.0_m};

    auto underlying_converter = std::make_unique<DummyLatLonCoordConverter>();
    auto coordinate_converter = std::make_unique<CoordinateConverter>(underlying_converter.get());

    auto converted_world_pos = coordinate_converter->Convert(odr_pos);

    EXPECT_EQ(expected_world_position.x, converted_world_pos.x);
    EXPECT_EQ(expected_world_position.y, converted_world_pos.y);
    EXPECT_EQ(expected_world_position.z, converted_world_pos.z);
}

}  // namespace gtgen::core::environment::api
