/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Host/vehicle_model_converter.h"

#include "Core/Environment/Exception/exception.h"
#include "Core/Tests/TestUtils/expect_extensions.h"

#include <MantleAPI/Common/time_utils.h>
#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Traffic/entity_properties.h>
#include <gtest/gtest.h>

namespace gtgen::core::environment::host
{
using units::literals::operator""_m;
using units::literals::operator""_mps;
using units::literals::operator""_mps_sq;
using units::literals::operator""_rad;
using units::literals::operator""_rad_per_s;
using units::literals::operator""_ms;

using HVD_VADF_State = osi3::HostVehicleData_VehicleAutomatedDrivingFunction_State;

TEST(ConvertProtoToVehicleModelOut, GivenProtoTrafficUpdate_WhenConvertingFromProto_ThenAllValuesCorrectlyConverted)
{
    mantle_api::Vec3<units::velocity::meters_per_second_t> expected_velocity{10.0_mps, 20.0_mps, 30.0_mps};
    mantle_api::Vec3<units::acceleration::meters_per_second_squared_t> expected_acceleration{
        1.0_mps_sq, 2.0_mps_sq, 3.0_mps_sq};
    mantle_api::Vec3<units::length::meter_t> expected_position{5_m, 2_m, 1.5_m};
    mantle_api::Orientation3<units::angle::radian_t> expected_orientation{3.1214_rad, 0.001_rad, 0.002_rad};
    mantle_api::Orientation3<units::angular_velocity::radians_per_second_t> expected_orientation_rate{
        1.234_rad_per_s, 2.345_rad_per_s, 3.456_rad_per_s};

    osi3::TrafficUpdate proto_traffic_update;

    proto_traffic_update.mutable_timestamp()->set_seconds(0);
    proto_traffic_update.mutable_timestamp()->set_nanos(100000000);

    osi3::MovingObject* proto_moving_object = proto_traffic_update.add_update();
    osi3::BaseMoving* base_moving = proto_moving_object->mutable_base();
    base_moving->mutable_position()->set_x(expected_position.x());
    base_moving->mutable_position()->set_y(expected_position.y());
    base_moving->mutable_position()->set_z(expected_position.z());

    base_moving->mutable_orientation()->set_yaw(expected_orientation.yaw());
    base_moving->mutable_orientation()->set_pitch(expected_orientation.pitch());
    base_moving->mutable_orientation()->set_roll(expected_orientation.roll());

    base_moving->mutable_velocity()->set_x(expected_velocity.x());
    base_moving->mutable_velocity()->set_y(expected_velocity.y());
    base_moving->mutable_velocity()->set_z(expected_velocity.z());

    base_moving->mutable_acceleration()->set_x(expected_acceleration.x());
    base_moving->mutable_acceleration()->set_y(expected_acceleration.y());
    base_moving->mutable_acceleration()->set_z(expected_acceleration.z());

    base_moving->mutable_orientation_rate()->set_yaw(expected_orientation_rate.yaw());
    base_moving->mutable_orientation_rate()->set_pitch(expected_orientation_rate.pitch());
    base_moving->mutable_orientation_rate()->set_roll(expected_orientation_rate.roll());

    proto_moving_object->mutable_vehicle_classification()->mutable_light_state()->set_indicator_state(
        osi3::MovingObject_VehicleClassification_LightState_IndicatorState::
            MovingObject_VehicleClassification_LightState_IndicatorState_INDICATOR_STATE_WARNING);

    VehicleModelOut vehicle_model_out;
    EXPECT_NO_THROW(vehicle_model_out = ConvertProtoToVehicleModelOut(proto_traffic_update));

    EXPECT_EQ(mantle_api::ExternalControlState::kOff, vehicle_model_out.had_control_state);

    EXPECT_TRIPLE(expected_position, vehicle_model_out.position)
    EXPECT_TRIPLE(expected_orientation, vehicle_model_out.orientation)
    EXPECT_TRIPLE(expected_velocity, vehicle_model_out.velocity)
    EXPECT_TRIPLE(expected_acceleration, vehicle_model_out.acceleration)
    EXPECT_TRIPLE(expected_orientation_rate, vehicle_model_out.orientation_rate)

    EXPECT_EQ(mantle_api::IndicatorState::kWarning, vehicle_model_out.indicator_state);
    EXPECT_EQ(100_ms, vehicle_model_out.time_stamp);
}

class VehicleAutomatedDrivingFunctionFixture
    : public testing::TestWithParam<std::tuple<HVD_VADF_State, mantle_api::ExternalControlState>>
{
  public:
    explicit VehicleAutomatedDrivingFunctionFixture() { proto_traffic_update_.add_update(); }

  protected:
    osi3::TrafficUpdate proto_traffic_update_;
};

INSTANTIATE_TEST_SUITE_P(
    VehicleAutomatedDrivingFunctionStatesToExternalControlStates,
    VehicleAutomatedDrivingFunctionFixture,
    testing::Values(
        std::tuple<HVD_VADF_State, mantle_api::ExternalControlState>{
            HVD_VADF_State::HostVehicleData_VehicleAutomatedDrivingFunction_State_STATE_UNKNOWN,
            mantle_api::ExternalControlState::kOff},
        std::tuple<HVD_VADF_State, mantle_api::ExternalControlState>{
            HVD_VADF_State::HostVehicleData_VehicleAutomatedDrivingFunction_State_STATE_OTHER,
            mantle_api::ExternalControlState::kOff},
        std::tuple<HVD_VADF_State, mantle_api::ExternalControlState>{
            HVD_VADF_State::HostVehicleData_VehicleAutomatedDrivingFunction_State_STATE_ERRORED,
            mantle_api::ExternalControlState::kOff},
        std::tuple<HVD_VADF_State, mantle_api::ExternalControlState>{
            HVD_VADF_State::HostVehicleData_VehicleAutomatedDrivingFunction_State_STATE_UNAVAILABLE,
            mantle_api::ExternalControlState::kOff},
        std::tuple<HVD_VADF_State, mantle_api::ExternalControlState>{
            HVD_VADF_State::HostVehicleData_VehicleAutomatedDrivingFunction_State_STATE_AVAILABLE,
            mantle_api::ExternalControlState::kOff},
        std::tuple<HVD_VADF_State, mantle_api::ExternalControlState>{
            HVD_VADF_State::HostVehicleData_VehicleAutomatedDrivingFunction_State_STATE_STANDBY,
            mantle_api::ExternalControlState::kOff},
        std::tuple<HVD_VADF_State, mantle_api::ExternalControlState>{
            HVD_VADF_State::HostVehicleData_VehicleAutomatedDrivingFunction_State_STATE_ACTIVE,
            mantle_api::ExternalControlState::kFull}));

TEST_P(
    VehicleAutomatedDrivingFunctionFixture,
    GivenVehicleAutomatedDrivingFunctionStateAndProtoTrafficUpdate_WhenConvertingFromProto_ThenConvertedMappingIsCorrect)
{
    const auto input_state = std::get<0>(GetParam());
    const auto expected_converted_state = std::get<1>(GetParam());

    proto_traffic_update_.add_internal_state()->add_vehicle_automated_driving_function()->set_state(input_state);

    VehicleModelOut vehicle_model_out;
    EXPECT_NO_THROW(vehicle_model_out = ConvertProtoToVehicleModelOut(proto_traffic_update_));

    EXPECT_EQ(expected_converted_state, vehicle_model_out.had_control_state);
}

TEST(ConvertProtoToVehicleModelOut, GivenEmptyProtoTrafficUpdate_WhenConvertingFromProto_ThenThrows)
{
    osi3::TrafficUpdate proto_traffic_update;
    EXPECT_THROW(ConvertProtoToVehicleModelOut(proto_traffic_update), environment::EnvironmentException);
}

}  // namespace gtgen::core::environment::host
