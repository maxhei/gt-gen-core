/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Tests/TestUtils/resolve_dir_paths.h"

#include "tools/cpp/runfiles/runfiles.h"

#include <filesystem>
#include <stdexcept>

using bazel::tools::cpp::runfiles::Runfiles;

namespace gtgen::core::test_utils
{

std::string ResolveDirPath(const std::string& path)
{
    if (std::filesystem::is_directory(path) && std::filesystem::exists(path))
    {
        return path;
    }

    std::string error{};
    std::unique_ptr<Runfiles> runfiles(Runfiles::CreateForTest(&error));

    if (runfiles == nullptr)
    {
        throw std::runtime_error("Resolve directory failed: " + error);
    }

    auto full_path = runfiles->Rlocation(path);
    if (std::filesystem::is_directory(full_path) && std::filesystem::exists(full_path))
    {
        return full_path;
    }

    full_path = runfiles->Rlocation("gt_gen_core/" + path);
    if (std::filesystem::is_directory(full_path) && std::filesystem::exists(full_path))
    {
        return full_path;
    }

    throw std::runtime_error("Directory not found: " + path);
}

}  // namespace gtgen::core::test_utils
