/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Tests/TestUtils/ProtoUtils/proto_utilities.h"

#include "Core/Environment/Entities/vehicle_entity.h"
#include "Core/Environment/Map/Geometry/project_query_point_on_polyline.h"
#include "Core/Service/GlmWrapper/glm_basic_vector_utils.h"
#include "Core/Service/GroundTruthConversions/proto_to_mantle.h"
#include "Core/Service/Logging/logging.h"

#include <cassert>

namespace gtgen::core::test_utils
{

using units::literals::operator""_m;
using units::literals::operator""_deg;
using units::literals::operator""_rad;
using units::literals::operator""_rad_per_s;
using units::literals::operator""_rad_per_s_sq;
using units::literals::operator""_mps_sq;
using units::literals::operator""_mps;

void SetupEntity(mantle_api::IEntity* entity)
{
    entity->SetPosition({1_m, 2_m, 3_m});
    entity->SetOrientation({1_rad, 2_rad, 3_rad});
    entity->SetOrientationRate({4_rad_per_s, 5_rad_per_s, 6_rad_per_s});
    entity->SetOrientationAcceleration({7_rad_per_s_sq, 8_rad_per_s_sq, 9_rad_per_s_sq});
    entity->SetVelocity({1_mps, 2_mps, 3_mps});
    entity->SetAcceleration({1_mps_sq, 2_mps_sq, 3_mps_sq});
    entity->SetAssignedLaneIds({1, 2});
}

void SetupBasicLaneGroup(const mantle_api::UniqueId lane_group_id, environment::map::GtGenMap& gtgen_map)
{
    environment::map::LaneGroup lane_group{lane_group_id, environment::map::LaneGroup::Type::kJunction};
    environment::map::Lane dummy_lane{0};

    gtgen_map.AddLaneGroup(lane_group);
    dummy_lane.id = 0;
    gtgen_map.AddLane(lane_group.id, dummy_lane);
    dummy_lane.id = 1;
    gtgen_map.AddLane(lane_group.id, dummy_lane);
}

void AddArtificialPedestrian(mantle_api::UniqueId id,
                             const std::string& model_reference,
                             environment::api::EntityRepository& entity_repository)
{
    auto properties = std::make_unique<mantle_api::PedestrianProperties>();
    properties->model = model_reference;
    properties->bounding_box.dimension = {4_m, 2_m, 1.5_m};

    mantle_api::IEntity& entity = entity_repository.Create(id, "Pedestrian" + std::to_string(id), *properties);
    SetupEntity(&entity);
}

void AddArtificialPedestrians(int number, environment::api::EntityRepository& entity_repository)
{
    for (int i = 0; i < number; ++i)
    {
        auto properties = std::make_unique<mantle_api::PedestrianProperties>();
        properties->bounding_box.dimension = {4_m, 2_m, 1.5_m};

        mantle_api::IEntity& entity = entity_repository.Create("Pedestrian" + std::to_string(i), *properties);
        SetupEntity(&entity);
    }
}

void AddArtificialVehicles(int number, environment::api::EntityRepository& entity_repository)
{
    for (int i = 0; i < number; ++i)
    {
        auto properties = std::make_unique<mantle_api::VehicleProperties>();
        properties->bounding_box.dimension = {4_m, 2_m, 1.5_m};

        mantle_api::IEntity& entity = entity_repository.Create("Vehicle" + std::to_string(i), *properties);
        SetupEntity(&entity);
    }
}

void AddArtificialTrafficVehicle(mantle_api::UniqueId id,
                                 mantle_api::IndicatorState indicator,
                                 const std::string& model_reference,
                                 environment::api::EntityRepository& entity_repository)
{
    auto properties = std::make_unique<mantle_api::VehicleProperties>();
    properties->model = model_reference;
    properties->bounding_box.dimension = {4_m, 2_m, 1.5_m};

    mantle_api::IEntity& entity = entity_repository.Create(id, "Vehicle" + std::to_string(id), *properties);
    SetupEntity(&entity);

    dynamic_cast<environment::entities::VehicleEntity&>(entity).SetIndicatorState(indicator);
}

void AddArtificialHostVehicle(mantle_api::IndicatorState indicator,
                              const std::string& model_reference,
                              mantle_api::ExternalControlState had_control_state,
                              environment::api::EntityRepository& entity_repository)
{
    auto properties = std::make_unique<mantle_api::VehicleProperties>();
    properties->model = model_reference;
    properties->bounding_box.dimension = {4_m, 2_m, 1.5_m};
    properties->is_host = true;

    mantle_api::IEntity& entity = entity_repository.Create(0, "Host", *properties);
    SetupEntity(&entity);

    dynamic_cast<environment::entities::VehicleEntity&>(entity).SetIndicatorState(indicator);
    dynamic_cast<environment::entities::VehicleEntity&>(entity).SetHADControlState(had_control_state);
}

osi3::Vector3d GetDelta(const osi3::Vector3d& a, const osi3::Vector3d& b)
{
    osi3::Vector3d res{};
    res.set_x(a.x() - b.x());
    res.set_y(a.y() - b.y());
    res.set_z(a.z() - b.z());
    return res;
}

osi3::Vector3d CreatePositionVector(double x, double y, double z)
{
    osi3::Vector3d position;
    position.set_x(x);
    position.set_y(y);
    position.set_z(z);

    return position;
}

std::optional<osi3::MovingObject> GetHostVehicle(const osi3::GroundTruth& gt)
{
    return GetTrafficVehicleById(gt, 0);
}

std::optional<osi3::MovingObject> GetTrafficVehicleById(const osi3::GroundTruth& gt, std::uint64_t id)
{
    for (const auto& object : gt.moving_object())
    {
        if (object.id().value() == id)
        {
            return object;
        }
    }
    return std::nullopt;
}

std::optional<osi3::MovingObject> GetMovingObjectByName(const osi3::GroundTruth& gt, const std::string& name)
{
    for (const auto& object : gt.moving_object())
    {
        if (object.source_reference(0).identifier(1) == name)
        {
            return object;
        }
    }
    return std::nullopt;
}

std::vector<const osi3::MovingObject*> GetObjectsByType(const osi3::GroundTruth& proto_gt,
                                                        osi3::MovingObject::Type type)
{
    std::vector<const osi3::MovingObject*> objects;
    for (const osi3::MovingObject& entity : proto_gt.moving_object())
    {
        if (entity.type() == type)
        {
            objects.push_back(&entity);
        }
    }
    return objects;
}

const osi3::MovingObject* GetObjectById(const osi3::GroundTruth& proto_gt, std::uint64_t id)
{
    for (const osi3::MovingObject& entity : proto_gt.moving_object())
    {
        if (entity.id().value() == id)
        {
            return &entity;
        }
    }
    return nullptr;
}

const osi3::Lane* GetLaneById(const osi3::GroundTruth& proto_gt, mantle_api::UniqueId id)
{
    for (const auto& lane : proto_gt.lane())
    {
        if (lane.id().value() == id)
        {
            return &lane;
        }
    }
    return nullptr;
}

std::vector<const osi3::LaneBoundary*> GetLaneBoundariesByIds(
    const osi3::GroundTruth& proto_gt,
    const google::protobuf::RepeatedPtrField<osi3::Identifier>& ids)
{
    std::vector<const osi3::LaneBoundary*> lane_boundaries;
    for (const auto& id : ids)
    {
        for (const auto& lane_boundary : proto_gt.lane_boundary())
        {
            if (lane_boundary.id().value() == id.value())
            {
                lane_boundaries.push_back(&lane_boundary);
                break;
            }
        }
    }
    return lane_boundaries;
}

std::vector<mantle_api::UniqueId> GetAllAntecessorIds(const osi3::Lane& gt_lane)
{
    std::vector<std::uint64_t> antecessors;
    std::unordered_set<std::uint64_t> antecessors_set;
    for (const auto& pairing : gt_lane.classification().lane_pairing())
    {
        if (antecessors_set.count(pairing.antecessor_lane_id().value()) == 0)
        {
            antecessors_set.insert(pairing.antecessor_lane_id().value());
            antecessors.push_back(pairing.antecessor_lane_id().value());
        }
    }
    return antecessors;
}

std::vector<mantle_api::UniqueId> GetAllSuccessorIds(const osi3::Lane& gt_lane)
{
    std::vector<std::uint64_t> successors;
    std::unordered_set<std::uint64_t> successors_set;
    for (const auto& pairing : gt_lane.classification().lane_pairing())
    {
        if (successors_set.count(pairing.successor_lane_id().value()) == 0)
        {
            successors_set.insert(pairing.successor_lane_id().value());
            successors.push_back(pairing.successor_lane_id().value());
        }
    }
    return successors;
}

mantle_api::Vec3<units::length::meter_t> GetVehiclePosition(const std::optional<osi3::MovingObject>& vehicle)
{
    return service::gt_conversion::ToVec3Length(vehicle->base().position());
}

mantle_api::Vec3<units::velocity::meters_per_second_t> GetVehicleVelocity(
    const std::optional<osi3::MovingObject>& vehicle)
{
    return service::gt_conversion::ToVec3Velocity(vehicle->base().velocity());
}

mantle_api::Vec3<units::acceleration::meters_per_second_squared_t> GetVehicleAcceleration(
    const std::optional<osi3::MovingObject>& vehicle)
{
    return service::gt_conversion::ToVec3Acceleration(vehicle->base().acceleration());
}

mantle_api::Orientation3<units::angle::radian_t> GetVehicleOrientation(const std::optional<osi3::MovingObject>& vehicle)
{
    return service::gt_conversion::ToOrientation3(vehicle->base().orientation());
}

mantle_api::Orientation3<units::angular_velocity::radians_per_second_t> GetVehicleOrientationRate(
    const std::optional<osi3::MovingObject>& vehicle)
{
    return service::gt_conversion::ToOrientation3Rate(vehicle->base().orientation_rate());
}

mantle_api::Dimension3 GetVehicleDimension(const std::optional<osi3::MovingObject>& vehicle)
{
    return service::gt_conversion::ToDimension3(vehicle->base().dimension());
}

std::vector<mantle_api::Vec3<units::length::meter_t>> GetLaneCenterline(
    const google::protobuf::RepeatedPtrField<osi3::Lane>& lanes,
    std::uint64_t lane_id)
{
    const auto& lane{
        std::find_if(lanes.begin(), lanes.end(), [lane_id](auto lane) { return lane.id().value() == lane_id; })};

    assert((lane != std::end(lanes)) &&
           ("Groundtruth doesn not contain any lane with id " + std::to_string(lane_id)).c_str());
    assert(lane->has_classification() && ("Lane #" + std::to_string(lane_id) + " has no classification").c_str());
    assert((lane->classification().centerline().size() > 0) &&
           ("Lane #" + std::to_string(lane_id) + " has no centerline").c_str());

    std::vector<mantle_api::Vec3<units::length::meter_t>> centerline_points;

    for (const auto& centerline_point : lane->classification().centerline())
    {
        centerline_points.push_back(service::gt_conversion::ToVec3Length(centerline_point));
    }

    return centerline_points;
}

std::vector<mantle_api::Vec3<units::length::meter_t>> GetObjectConcatenatedCenterline(
    const osi3::MovingObject& object,
    const google::protobuf::RepeatedPtrField<osi3::Lane>& lanes)
{
    const auto assigned_lane_ids{object.moving_object_classification().assigned_lane_id()};
    std::vector<mantle_api::Vec3<units::length::meter_t>> object_concatenated_centerline;

    for (const auto& assigned_lane_id : assigned_lane_ids)
    {
        const auto centerline{GetLaneCenterline(lanes, assigned_lane_id.value())};

        for (const auto& point : centerline)
        {
            object_concatenated_centerline.push_back(point);
        }
    }

    return object_concatenated_centerline;
}

double GetPositionDistanceToLaneCenterline(const mantle_api::Vec3<units::length::meter_t>& position,
                                           const google::protobuf::RepeatedPtrField<osi3::Lane>& lanes,
                                           std::uint64_t lane_id)
{
    const auto centerline{GetLaneCenterline(lanes, lane_id)};

    const auto centerline_projected_position{environment::map::ProjectQueryPointOnPolyline(position, centerline)};

    const double distance_to_centerline{
        service::glmwrapper::Distance(position, std::get<0>(centerline_projected_position))};

    return distance_to_centerline;
}

}  // namespace gtgen::core::test_utils
