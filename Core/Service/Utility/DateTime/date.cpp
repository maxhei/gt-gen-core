/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Service/Utility/DateTime/date.h"

#include <ctime>

namespace gtgen::core::service::utility
{
Date Date::Today()
{
    auto d = Date();
    d.SetToday();
    return d;
}

void Date::SetToday()
{
    time_t now = time(nullptr);
    tm* localtm = localtime(&now);

    year = localtm->tm_year + 1900;
    month = localtm->tm_mon + 1;
    day = localtm->tm_mday;
}

}  // namespace gtgen::core::service::utility
