/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Service/Utility/DateTime/time.h"

#include <ctime>

namespace gtgen::core::service::utility
{
Time Time::Now()
{
    auto t = Time();
    t.SetNow();
    return t;
}

void Time::SetNow()
{
    time_t now = time(nullptr);
    tm* localtm = localtime(&now);  // NOLINT(bmw-banned-function)

    hour = localtm->tm_hour;
    min = localtm->tm_min;
    sec = localtm->tm_sec;
}

}  // namespace gtgen::core::service::utility
