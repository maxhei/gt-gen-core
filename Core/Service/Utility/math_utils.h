/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_UTILITY_MATHUTILS_H
#define GTGEN_CORE_SERVICE_UTILITY_MATHUTILS_H

#include <units.h>

#include <algorithm>
#include <cmath>

namespace gtgen::core::service::utility
{

template <typename T>
constexpr int GetSign(T val)
{
    return val > 0 ? 1 : -1;
}

template <typename T>
constexpr T AvoidZero(T value)
{
    constexpr double small_number{1.0E-8};
    return static_cast<T>(GetSign(value) * std::max(small_number, std::fabs(value)));
}

template <typename T>
constexpr T CalculateOtherLegInARectangledTriangle(T c, T a)
{
    T b = c;
    if ((a > 0.0 || a < 0.0) && b > std::fabs(a))
    {
        b = std::sqrt(c * c - a * a);
    }
    return b;
}

units::angle::radian_t AbsAngle(const units::angle::radian_t alpha, const units::angle::radian_t beta);

}  // namespace gtgen::core::service::utility

#endif  // GTGEN_CORE_SERVICE_UTILITY_MATHUTILS_H
