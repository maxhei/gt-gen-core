/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Service/Utility/string_utils.h"

#include "Core/Service/Exception/exception.h"

#include <gtest/gtest.h>

namespace gtgen::core::service::utility
{
/// @test Tests that the CreateDirectoryIfNotExisting creates the directory if it was not existing before
TEST(StringSplitTest, GivenStringWithCommas_WhenSplitByDefaultDelimiter_ThenStringSplitToThreeElements)
{
    std::vector<std::string> splitted = Split("a,b,c");
    EXPECT_EQ(splitted.size(), std::size_t(3));
    EXPECT_EQ(splitted.at(0), "a");
    EXPECT_EQ(splitted.at(1), "b");
    EXPECT_EQ(splitted.at(2), "c");
}

TEST(StringSplitTest, GivenStringWithPlus_WhenSplitByPlusAndIgnoreEmptyElements_ThenStringSplitToThreeElements)
{
    std::vector<std::string> splitted = Split("a+b+c+", '+');
    EXPECT_EQ(splitted.size(), std::size_t(3));
    EXPECT_EQ(splitted.at(0), "a");
    EXPECT_EQ(splitted.at(1), "b");
    EXPECT_EQ(splitted.at(2), "c");
}

TEST(StringSplitTest, GivenStringWithPlus_WhenSplitByPlusAndDoNotIgnoreEmptyElements_ThenStringSplitToThreeElements)
{
    std::vector<std::string> splitted = Split("a++c", '+', false);
    EXPECT_EQ(splitted.size(), std::size_t(3));
    EXPECT_EQ(splitted.at(0), "a");
    EXPECT_EQ(splitted.at(1), "");
    EXPECT_EQ(splitted.at(2), "c");
}

TEST(StringSplitTest,
     GivenStringToBeSplittedByComaAndRemoveEmptyElements_WhenSplit_ThenStringisSplitWithoutEmptyElements)
{
    std::vector<std::string> splitted = Split(",,,a,,,,,");
    EXPECT_EQ(splitted.size(), std::size_t(1));
    EXPECT_EQ(splitted.at(0), "a");
}

TEST(StringSplitTest,
     GivenStringToBeSplittedByComaAndKeepEmptyElements_WhenSplit_ThenStringisSplitAndEmptyElementsContained)
{
    std::vector<std::string> splitted = Split("a,,", ',', false);
    EXPECT_EQ(splitted.size(), std::size_t(3));
    EXPECT_EQ(splitted.at(0), "a");
    EXPECT_EQ(splitted.at(1), "");
    EXPECT_EQ(splitted.at(2), "");
}

TEST(StringTrimTest, GivenString_WhenTrim_ThenTrimmedStringReturned)
{
    std::string whitespaces("    this-shall-be-left   ");
    Trim(whitespaces, " ");
    EXPECT_EQ(whitespaces, "this-shall-be-left");

    std::string trim_multiple_chars("{ { , , . .  this-shall-be-left  . . , , } }");
    Trim(whitespaces, " ,.{}");
    EXPECT_EQ(whitespaces, "this-shall-be-left");
}

TEST(EndsWithTest, GivenStringEndingWithTheDesiredSubString_WhenEndsWithCaseSensitive_ThenReturnTrue)
{
    EXPECT_TRUE(EndsWith("some_random_scenario.json", ".json"));
    EXPECT_TRUE(EndsWith("/full/path/to/UserSettings.ini", ".ini"));
    EXPECT_TRUE(EndsWith("just a string", "ng"));
    EXPECT_TRUE(EndsWith("just a string", "g"));
    EXPECT_TRUE(EndsWith("just a string", ""));
    EXPECT_TRUE(EndsWith("", ""));
    EXPECT_TRUE(EndsWith("String with Cases", "Cases"));
}

TEST(EndsWithTest, GivenStringNotEndingWithTheDesiredSubString_WhenEndsWithCaseSensitive_ThenReturnFalse)
{
    EXPECT_FALSE(EndsWith("some_random_scenario.json", ".jso"));
    EXPECT_FALSE(EndsWith("/full/path/to/UserSettings.ini", "User"));
    EXPECT_FALSE(EndsWith("just a string", "asdf"));
    EXPECT_FALSE(EndsWith("just a string", "g "));
    EXPECT_FALSE(EndsWith("just a string", " "));
    EXPECT_FALSE(EndsWith("", "hello, World!"));
    EXPECT_FALSE(EndsWith("String with Cases", "cases"));
}

TEST(EndsWithTest, GivenStringEndingWithTheDesiredSubString_WhenEndsWithCaseInSensitive_ThenReturnTrue)
{
    EXPECT_TRUE(EndsWith("some/path/to/scenario.JSON", ".json", true));
    EXPECT_TRUE(EndsWith("Random string", "StrInG", true));
}

TEST(EndsWithTest, GivenStringNotEndingWithTheDesiredSubString_WhenEndsWithCaseInSensitive_ThenReturnFalse)
{
    EXPECT_FALSE(EndsWith("some/path/to/scenario.JSON", ".jso", true));
    EXPECT_FALSE(EndsWith("Random string", "StInG", true));
}

TEST(IsDouble, GivenCharacter_WhenIsDouble_ThenDoublesCorrectlyDetected)
{
    EXPECT_FALSE(IsDouble("A"));
    EXPECT_FALSE(IsDouble(""));
    EXPECT_TRUE(IsDouble("2"));
    EXPECT_TRUE(IsDouble("2."));
    EXPECT_TRUE(IsDouble("2.1"));
}

TEST(ToLower, GivenString_WhenToLower_ThenStringContainsOnlyLowerCaseCharacters)
{
    EXPECT_EQ("lower", ToLower("Lower"));
    EXPECT_EQ("lower", ToLower("LoWeR"));
    EXPECT_EQ("lower", ToLower("LOWER"));
    EXPECT_EQ("lower", ToLower("lower"));
}

TEST(RemoveAllWhitespaces, GivenString_WhenRemoveAllWhitespaces_ThenAllWhitespacesRemoved)
{
    std::string input("     T E S T ALL WHITE spaces REMO V E D        !      ");
    std::string expected("TESTALLWHITEspacesREMOVED!");

    RemoveAllWhitespaces(input);
    EXPECT_EQ(expected, input);
}

TEST(StringListToVector, GivenString_WhenInputIsCommaSeperatedAndCurlyBraced_ThenOutputIsTheElements)
{
    std::string input("{abc,def,g}");
    std::vector<std::string> expected{"abc", "def", "g"};

    auto actual = StringListToVector(input);
    EXPECT_EQ(expected, actual);
}

TEST(StringListToVector,
     GivenString_WhenInputIsCommaSeperatedAndCurlyBraced_ThenOutputIsTheElementsWithoutExtraWhiteSpace)
{
    std::string input("     { abc,   def , g}    ");
    std::vector<std::string> expected{"abc", "def", "g"};

    auto actual = StringListToVector(input);
    EXPECT_EQ(expected, actual);
}

TEST(StringListToVector, GivenString_WhenInputWithoutOpenCurly_ThenExceptionIsThrown)
{
    EXPECT_THROW(StringListToVector(" oh no } "), ServiceException);
}
TEST(StringListToVector, GivenString_WhenInputWithoutCloseCurly_ThenExceptionIsThrown)
{
    EXPECT_THROW(StringListToVector(" { oh no "), ServiceException);
}

TEST(StringListToVector, GivenString_WhenInputIsInvalid_ThenExceptionIsThrown)
{
    EXPECT_THROW(StringListToVector(" oh no "), ServiceException);
}

TEST(StringVectorToStringTest,
     GivenVectorWithThreeIntElements_WhenJoinedByDefaultDelimiter_ThenVectorJoinedToStringWithDefaultDelimiter)
{
    std::vector<int> list{1, 2, 3};
    std::string joined = VectorToString(list);
    EXPECT_EQ(joined, "1,2,3");
}

TEST(StringVectorToStringTest,
     GivenVectorWithThreeStringElements_WhenJoinedByDefaultDelimiter_ThenVectorJoinedToStringWithDefaultDelimiter)
{
    std::vector<std::string> list{"aa", "bb", "cc"};
    std::string joined = VectorToString(list);
    EXPECT_EQ(joined, "aa,bb,cc");
}

TEST(StringVectorToStringTest,
     GivenVectorWithThreeCharElements_WhenJoinedByDefaultDelimiter_ThenVectorJoinedToStringWithDefaultDelimiter)
{
    std::vector<char> list{'a', 'b', 'c'};
    std::string joined = VectorToString(list);
    EXPECT_EQ(joined, "a,b,c");
}

TEST(StringVectorToStringTest, GivenEmptyIntVector_WhenJoinedByDefaultDelimiter_ThenStringIsEmpty)
{
    std::vector<int> list{};
    std::string joined = VectorToString(list);
    EXPECT_EQ(joined, "");
}

TEST(StringVectorToStringTest, GivenEmptyStringVector_WhenJoinedByDefaultDelimiter_ThenStringIsEmpty)
{
    std::vector<std::string> list{};
    std::string joined = VectorToString(list);
    EXPECT_EQ(joined, "");
}

TEST(StringVectorToStringTest,
     GivenVectorWithThreeIntElements_WhenJoinedByCustomDelimiter_ThenVectorJoinedToStringWithCustomDelimiter)
{
    std::vector<int> list{1, 2, 3};
    std::string joined = VectorToString(list, '.');
    EXPECT_EQ(joined, "1.2.3");
}

TEST(StringVectorToStringTest,
     GivenVectorWithThreeStringElements_WhenJoinedByCustomDelimiter_ThenVectorJoinedToStringWithCustomDelimiter)
{
    std::vector<std::string> list{"aa", "bb", "cc"};
    std::string joined = VectorToString(list, '.');
    EXPECT_EQ(joined, "aa.bb.cc");
}

class StringToVectorIntTestNotValidCases : public testing::TestWithParam<std::string>
{
};

INSTANTIATE_TEST_CASE_P(StringToVectorIntNotValidParams,
                        StringToVectorIntTestNotValidCases,
                        testing::Values(",",
                                        ", 1, 2",
                                        "aaa",
                                        "12, 45, a",
                                        "-, 76, 38",
                                        "+10, 76, 38",
                                        "9, 8, 7 6 ,5",
                                        "1, 2, 3, 4, 5, ",
                                        "1, 2, 3, 4, 5,",
                                        "1.1, 2.0, 3.2, 4.5"));

TEST_P(StringToVectorIntTestNotValidCases, GivenInvalidStrings_WhenStringToVectorInt_ThenInvalidArgumentExceptionThrown)
{
    EXPECT_THROW(StringToVectorInt(GetParam()), std::invalid_argument);
}

TEST(StringToVectorIntTestNotValidCases, GivenTooLargeNumber_WhenStringToVectorInt_ThenOutOfRangeExceptionThrown)
{
    EXPECT_THROW(StringToVectorInt("18446744073709551619"), std::out_of_range);
}

class StringToVectorIntTestValidCases
    : public testing::TestWithParam<std::tuple<std::string, std::vector<std::int64_t>>>
{
};

INSTANTIATE_TEST_CASE_P(StringToVectorIntValidParams,
                        StringToVectorIntTestValidCases,
                        testing::Values(std::make_tuple("20 , 39, 54,61,  1234 , 287",
                                                        std::vector<std::int64_t>{20, 39, 54, 61, 1234, 287}),
                                        std::make_tuple("123", std::vector<std::int64_t>{123}),
                                        std::make_tuple("3,2", std::vector<std::int64_t>{3, 2}),
                                        std::make_tuple("30,20,1", std::vector<std::int64_t>{30, 20, 1}),
                                        std::make_tuple("19, -2, 76, 38", std::vector<std::int64_t>{19, -2, 76, 38}),
                                        std::make_tuple("", std::vector<std::int64_t>()),
                                        std::make_tuple("9223372036854775807",
                                                        std::vector<std::int64_t>{9223372036854775807})));

TEST_P(StringToVectorIntTestValidCases, GivenValidStrings_WhenStringToVectorInt_ThenVectorIsCorrect)
{
    std::vector<std::int64_t> result_vector = StringToVectorInt(std::get<0>(GetParam()));
    EXPECT_EQ(result_vector, std::get<1>(GetParam()));
}

}  // namespace gtgen::core::service::utility
