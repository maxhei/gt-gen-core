cc_library(
    name = "algorithm_utils",
    srcs = ["algorithm_utils.h"],
    visibility = ["//visibility:public"],
)

cc_test(
    name = "algorithm_utils_test",
    timeout = "short",
    srcs = ["algorithm_utils_test.cpp"],
    deps = [
        ":algorithm_utils",
        "@googletest//:gtest_main",
        "@mantle_api//:test_utils",
    ],
)

cc_library(
    name = "clock",
    srcs = ["clock.cpp"],
    hdrs = ["clock.h"],
    visibility = ["//visibility:public"],
    deps = [
        "@mantle_api",
        "@units_nhh",
    ],
)

cc_test(
    name = "clock_test",
    timeout = "short",
    srcs = ["clock_test.cpp"],
    visibility = ["//visibility:public"],
    deps = [
        ":clock",
        "//Core/Service/Logging:logging",
        "//Core/Service/MantleApiExtension:formatting",
        "@googletest//:gtest_main",
        "@mantle_api",
        "@units_nhh",
    ],
)

cc_library(
    name = "derivative_utils",
    hdrs = ["derivative_utils.h"],
    visibility = [
        "//Core/Environment/Controller/Internal/ControlUnits:__subpackages__",
        "//Core/Environment/Controller/Internal/Utilities/PlausibilityCheck:__subpackages__",
    ],
    deps = [
        "@mantle_api",
        "@units_nhh",
    ],
)

cc_test(
    name = "derivative_utils_test",
    srcs = ["derivative_utils_test.cpp"],
    deps = [
        ":derivative_utils",
        "//Core/Tests/TestUtils:expect_extensions",
        "@googletest//:gtest_main",
        "@mantle_api",
        "@units_nhh",
    ],
)

cc_library(
    name = "exceptions_includes",
    hdrs = ["exceptions.h"],
    visibility = [
        "//:__pkg__",
        "//Core/Communication/Common:__subpackages__",
        "//Core/Environment/Map/Common:__subpackages__",
        "//Core/Simulation:__subpackages__",
    ],
    deps = ["//Core/Service/MantleApiExtension:formatting"],
)

cc_library(
    name = "math_utils",
    srcs = ["math_utils.cpp"],
    hdrs = ["math_utils.h"],
    visibility = ["//visibility:public"],
    deps = ["@units_nhh"],
)

cc_test(
    name = "math_utils_test",
    timeout = "short",
    srcs = ["math_utils_test.cpp"],
    deps = [
        ":math_utils",
        "@googletest//:gtest_main",
        "@units_nhh",
    ],
)

cc_library(
    name = "orientation_utils",
    srcs = ["orientation_utils.cpp"],
    hdrs = ["orientation_utils.h"],
    visibility = ["//Core/Environment/Controller/Internal/Utilities:__subpackages__"],
    deps = [
        "//Core/Service/GlmWrapper:glm_basic_orientation_utils",
        "//Core/Service/GlmWrapper:glm_mantle_conversion",
        "@mantle_api",
        "@units_nhh",
    ],
)

cc_test(
    name = "orientation_utils_test",
    timeout = "short",
    srcs = ["orientation_utils_test.cpp"],
    deps = [
        ":orientation_utils",
        "//Core/Tests/TestUtils:expect_extensions",
        "@googletest//:gtest_main",
        "@mantle_api",
        "@units_nhh",
    ],
)

cc_library(
    name = "position_utils",
    srcs = ["position_utils.cpp"],
    hdrs = ["position_utils.h"],
    visibility = [
        "//:__pkg__",
        "//Core/Environment/Controller:__subpackages__",
        "//Core/Environment/Map/Common:__subpackages__",
        "//Core/Environment/Map/LaneLocationProvider:__subpackages__",
        "//Core/Simulation:__subpackages__",
    ],
    deps = [
        "//Core/Service/GlmWrapper:glm_basic_vector_utils",
        "@mantle_api",
    ],
)

cc_test(
    name = "position_utils_test",
    srcs = ["position_utils_test.cpp"],
    deps = [
        ":position_utils",
        "//Core/Tests/TestUtils:expect_extensions",
        "@googletest//:gtest_main",
        "@mantle_api",
    ],
)

cc_library(
    name = "string_utils",
    srcs = ["string_utils.cpp"],
    hdrs = ["string_utils.h"],
    visibility = ["//visibility:public"],
    deps = ["//Core/Service/Exception:exception"],
)

cc_test(
    name = "string_utils_test",
    timeout = "short",
    srcs = ["string_utils_test.cpp"],
    deps = [
        ":string_utils",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "unique_id_provider",
    srcs = ["unique_id_provider.cpp"],
    hdrs = ["unique_id_provider.h"],
    visibility = [
        "//:__pkg__",
        "//Core/Communication/Dispatchers:__subpackages__",
        "//Core/Environment/GtGenEnvironment:__subpackages__",
        "//Core/Environment/Map:__subpackages__",
    ],
    deps = [
        ":exceptions_includes",
        "//Core/Service/Logging:logging",
    ],
)

cc_test(
    name = "unique_id_provider_test",
    timeout = "short",
    srcs = ["unique_id_provider_test.cpp"],
    deps = [
        ":unique_id_provider",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "version",
    hdrs = ["version.h"],
    visibility = [
        "//:__pkg__",
        "//Core/Service/UserSettings:__subpackages__",
        "//Core/Service/Version:__subpackages__",
    ],
    deps = ["@fmt"],
)

cc_test(
    name = "version_test",
    timeout = "short",
    srcs = ["version_test.cpp"],
    deps = [
        ":version",
        "@googletest//:gtest_main",
    ],
)
