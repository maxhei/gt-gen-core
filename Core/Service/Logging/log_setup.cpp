/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/Logging/log_setup.h"

#include "Core/Service/FileSystem/file_system_utils.h"
#include "Core/Service/Logging/spdlog_wrapper.h"
#include "Core/Service/Utility/DateTime/date.h"
#include "Core/Service/Utility/DateTime/time.h"
#include "Core/Service/Utility/string_utils.h"

#include <fmt/format.h>
#include <spdlog/details/console_globals.h>
#include <spdlog/sinks/sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>

namespace gtgen::core::service::logging
{
namespace detail
{
spdlog::level::level_enum StringToLogLevel(const std::string& log_level)
{
    auto lower_log_level = utility::ToLower(log_level);
    if (lower_log_level == "trace")
    {
        return spdlog::level::trace;
    }
    else if (lower_log_level == "debug")
    {
        return spdlog::level::debug;
    }
    else if (lower_log_level == "info")
    {
        return spdlog::level::info;
    }
    else if (lower_log_level == "warn" || lower_log_level == "warning")
    {
        return spdlog::level::warn;
    }
    else if (lower_log_level == "error")
    {
        return spdlog::level::err;
    }
    else if (lower_log_level == "off")
    {
        return spdlog::level::off;
    }
    return spdlog::level::info;
}

}  // namespace detail

void LogSetup::AddConsoleLogger(const std::string& log_level)
{
    if (!console_logger_added_)
    {
        AddConsoleLoggerInternal(log_level);
        console_logger_added_ = true;
    }
}
void LogSetup::AddConsoleLoggerInternal(const std::string& log_level)
{
    auto spdlog_log_level = detail::StringToLogLevel(log_level);
    auto logger = std::make_shared<spdlog::logger>(gtgen_logger_name);
    spdlog::set_default_logger(logger);
    spdlog::default_logger()->set_level(spdlog::level::trace);

    spdlog::default_logger()->sinks().emplace_back(std::make_shared<spdlog::sinks::stdout_color_sink_mt>());
    spdlog::default_logger()->sinks().back()->set_level(spdlog_log_level);
}

void LogSetup::AddFileLogger(const std::string& log_file_path,
                             FileLoggingOption file_logging_options,
                             const std::string& file_log_level)
{
    auto spdlog_file_log_level = detail::StringToLogLevel(file_log_level);

    if (file_logging_options != FileLoggingOption::kNoFileLogging && spdlog_file_log_level != spdlog::level::off)
    {
        fs::path full_log_file_path{log_file_path};
        file_system::CreateDirectoryIfNotExisting(full_log_file_path);
        full_log_file_path = full_log_file_path / GetLogFileName(file_logging_options);

        auto file_sink = std::make_shared<spdlog::sinks::basic_file_sink_st>(full_log_file_path, true);
        file_sink->set_level(spdlog_file_log_level);
        spdlog::default_logger()->sinks().emplace_back(file_sink);

        spdlog::default_logger()->info(
            "[0.000s] Logging '{}'-level to file: '{}'", file_log_level, full_log_file_path.c_str());
    }
}

std::string LogSetup::GetLogFileName(FileLoggingOption file_logging_options)
{
    if (file_logging_options == FileLoggingOption::kAlwaysLogToNewFile)
    {
        return fmt::format("{}-{}_gtgen_core.log", utility::Date::Today(), utility::Time::Now());
    }
    else
    {
        return fmt::format("{}.log", "gtgen_core");
    }
}

void LogSetup::CleanupLogging()
{
    if (console_logger_added_)
    {
        spdlog::details::registry::instance().apply_all(
            [](const std::shared_ptr<spdlog::logger>& logger) { logger->flush(); });

        spdlog::shutdown();

        console_logger_added_ = false;
    }
}

}  // namespace gtgen::core::service::logging
