/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Service/GlmWrapper/glm_basic_orientation_utils.h"

#include "Core/Service/GlmWrapper/glm_basic_vector_utils.h"
#include "Core/Service/GlmWrapper/glm_mantle_conversion.h"

#include <glm/gtx/euler_angles.hpp>

namespace gtgen::core::service::glmwrapper
{

glm::dmat3 CreateRotationMatrix(const mantle_api::Orientation3<units::angle::degree_t>& orientation)
{
    mantle_api::Orientation3<units::angle::radian_t> orientation_rad;
    orientation_rad.yaw = static_cast<units::angle::radian_t>(orientation.yaw);
    orientation_rad.pitch = static_cast<units::angle::radian_t>(orientation.pitch);
    orientation_rad.roll = static_cast<units::angle::radian_t>(orientation.roll);

    return CreateRotationMatrix(orientation_rad);
}

glm::dmat3 CreateRotationMatrix(const mantle_api::Orientation3<units::angle::radian_t>& orientation)
{
    // Reference: https://www.redcrab-software.com/en/Calculator/3x3/Matrix/Rotation-XYZ

    const auto alpha{orientation.yaw.value()};
    const auto beta{orientation.pitch.value()};
    const auto gamma{orientation.roll.value()};

    glm::dmat3 rotation_matrix;
    rotation_matrix[0][0] = std::cos(alpha) * std::cos(beta);
    rotation_matrix[1][0] = std::cos(alpha) * std::sin(beta) * std::sin(gamma) - std::sin(alpha) * std::cos(gamma);
    rotation_matrix[2][0] = std::cos(alpha) * std::sin(beta) * std::cos(gamma) + std::sin(alpha) * std::sin(gamma);
    rotation_matrix[0][1] = std::sin(alpha) * std::cos(beta);
    rotation_matrix[1][1] = std::sin(alpha) * std::sin(beta) * std::sin(gamma) + std::cos(alpha) * std::cos(gamma);
    rotation_matrix[2][1] = std::sin(alpha) * std::sin(beta) * std::cos(gamma) - std::cos(alpha) * std::sin(gamma);
    rotation_matrix[0][2] = -std::sin(beta);
    rotation_matrix[1][2] = std::cos(beta) * std::sin(gamma);
    rotation_matrix[2][2] = std::cos(beta) * std::cos(gamma);

    return rotation_matrix;
}

glm::dmat3 CreateRotationMatrix(const glm::dvec3& forward, const glm::dvec3& up)
{
    // Reference: http://www.songho.ca/opengl/gl_anglestoaxes.html
    const glm::dvec3 left = glm::normalize(glm::cross(up, forward));
    return glm::dmat3{forward, left, up};
}

glm::dquat ToQuaternion(const mantle_api::Orientation3<units::angle::radian_t>& orientation)
{
    const double half_yaw = orientation.yaw() * 0.5;
    const double half_pitch = orientation.pitch() * 0.5;
    const double half_roll = orientation.roll() * 0.5;
    const double cos_yaw = std::cos(half_yaw);
    const double sin_yaw = std::sin(half_yaw);
    const double cos_pitch = std::cos(half_pitch);
    const double sin_pitch = std::sin(half_pitch);
    const double cos_roll = std::cos(half_roll);
    const double sin_roll = std::sin(half_roll);

    return glm::dquat{
        cos_roll * cos_pitch * cos_yaw + sin_roll * sin_pitch * sin_yaw,
        sin_roll * cos_pitch * cos_yaw - cos_roll * sin_pitch * sin_yaw,  // x
        cos_roll * sin_pitch * cos_yaw + sin_roll * cos_pitch * sin_yaw,  // y
        cos_roll * cos_pitch * sin_yaw - sin_roll * sin_pitch * cos_yaw   // z
    };
}

mantle_api::Orientation3<units::angle::radian_t> FromQuaternion(const glm::dquat& quat)
{
    const double sqx = quat[0] * quat[0];
    const double sqy = quat[1] * quat[1];
    const double sqz = quat[2] * quat[2];
    const double squ = quat[3] * quat[3];
    const double sarg = -2. * (quat[0] * quat[2] - quat[3] * quat[1]);

    double roll_x = NAN;
    double pitch_y = NAN;
    double yaw_z = NAN;
    // If the pitch angle is PI/2 or -PI/2, we can only compute
    // the sum roll + yaw.  However, any combination that gives
    // the right sum will produce the correct orientation, so we
    // set rollX = 0 and compute yawZ.
    if (sarg <= -0.99999)
    {
        pitch_y = -0.5 * glm::pi<double>();
        roll_x = 0;
        yaw_z = 2.0 * std::atan2(quat[0], -quat[1]);
    }
    else if (sarg >= 0.99999)
    {
        pitch_y = 0.5 * glm::pi<double>();
        roll_x = 0;
        yaw_z = 2.0 * std::atan2(-quat[0], quat[1]);
    }
    else
    {
        pitch_y = std::asin(sarg);
        roll_x = std::atan2(2 * (quat[1] * quat[2] + quat[3] * quat[0]), squ - sqx - sqy + sqz);
        yaw_z = std::atan2(2 * (quat[0] * quat[1] + quat[3] * quat[2]), squ + sqx - sqy - sqz);
    }

    return mantle_api::Orientation3<units::angle::radian_t>{
        units::angle::radian_t(yaw_z), units::angle::radian_t(pitch_y), units::angle::radian_t(roll_x)};
}

mantle_api::Orientation3<units::angle::radian_t> FromBasisVectors(
    const mantle_api::Vec3<units::length::meter_t>& forward,
    const mantle_api::Vec3<units::length::meter_t>& up)
{
    glm::dvec3 glm_forward = ToGlmVec3(forward);
    glm::dvec3 glm_up = ToGlmVec3(up);
    return FromRotationMatrix(CreateRotationMatrix(glm_forward, glm_up));
}

mantle_api::Orientation3<units::angle::radian_t> FromRotationMatrix(const glm::dmat3& mat)
{
    // @todo Handle singularities, i.e. gimbal lock
    // References:
    // - https://www.astro.rug.nl/software/kapteyn-beta/_downloads/attitude.pdf
    // - http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToEuler/index.htm

    const double roll = std::atan2(mat[1][2], mat[2][2]);
    const double pitch = -std::asin(mat[0][2]);
    const double yaw = std::atan2(mat[0][1], mat[0][0]);

    return mantle_api::Orientation3<units::angle::radian_t>{
        units::angle::radian_t(yaw), units::angle::radian_t(pitch), units::angle::radian_t(roll)};
}

mantle_api::Vec3<units::length::meter_t> GetWorldSpaceLeftVector(
    const mantle_api::Orientation3<units::angle::radian_t>& orientation)
{
    const glm::dvec4 local_left_vector{LeftVector().x, LeftVector().y, LeftVector().z, 0.0};
    const auto quaternion = ToQuaternion(orientation);

    return ToMantleVec3Length(glm::normalize(quaternion * local_left_vector));
}

mantle_api::Vec3<units::length::meter_t> GetWorldSpaceForwardVector(
    const mantle_api::Orientation3<units::angle::radian_t>& orientation)
{
    const glm::dvec4 local_forward_vector{ForwardVector().x, ForwardVector().y, ForwardVector().z, 0.0};
    const auto quaternion = ToQuaternion(orientation);

    return ToMantleVec3Length(glm::normalize(quaternion * local_forward_vector));
}

mantle_api::Vec3<units::length::meter_t> GetWorldSpaceUpVector(
    const mantle_api::Orientation3<units::angle::radian_t>& orientation)
{
    const glm::dvec4 local_up_vector{UpVector().x, UpVector().y, UpVector().z, 0.0};
    const auto quaternion = ToQuaternion(orientation);

    return ToMantleVec3Length(glm::normalize(quaternion * local_up_vector));
}

}  // namespace gtgen::core::service::glmwrapper
