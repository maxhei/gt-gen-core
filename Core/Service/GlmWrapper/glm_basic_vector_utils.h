/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_GLMWRAPPER_GLMBASICVECTORUTILS_H
#define GTGEN_CORE_SERVICE_GLMWRAPPER_GLMBASICVECTORUTILS_H

#include <MantleAPI/Common/dimension.h>
#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Common/vector.h>
#include <glm/ext/quaternion_double.hpp>
#include <glm/vec3.hpp>

#include <vector>

namespace gtgen::core::service::glmwrapper
{

inline glm::dvec3 UpVector()
{
    return {0, 0, 1};
}

inline glm::dvec3 ForwardVector()
{
    return {1, 0, 0};
}

inline glm::dvec3 LeftVector()
{
    return {0, 1, 0};
}

double Distance(const mantle_api::Vec3<units::length::meter_t>& pos1,
                const mantle_api::Vec3<units::length::meter_t>& pos2);
double Distance2(const mantle_api::Vec3<units::length::meter_t>& pos1,
                 const mantle_api::Vec3<units::length::meter_t>& pos2);

double Length(const mantle_api::Vec3<units::length::meter_t>& vec);
double Length2(const mantle_api::Vec3<units::length::meter_t>& vec);

mantle_api::Vec3<units::length::meter_t> Normalize(const mantle_api::Vec3<units::length::meter_t>& vec);

mantle_api::Vec3<units::length::meter_t> Cross(const mantle_api::Vec3<units::length::meter_t>& vec1,
                                               const mantle_api::Vec3<units::length::meter_t>& vec2);

double Dot(const mantle_api::Vec3<units::length::meter_t>& pt0, const mantle_api::Vec3<units::length::meter_t>& pt1);

mantle_api::Vec3<units::length::meter_t> Normal(const mantle_api::Vec3<units::length::meter_t>& pt0,
                                                const mantle_api::Vec3<units::length::meter_t>& pt1,
                                                const mantle_api::Vec3<units::length::meter_t>& direction);

mantle_api::Vec3<units::length::meter_t> Mix(const mantle_api::Vec3<units::length::meter_t>& pt0,
                                             const mantle_api::Vec3<units::length::meter_t>& pt1,
                                             double d);

}  // namespace gtgen::core::service::glmwrapper

#endif  // GTGEN_CORE_SERVICE_GLMWRAPPER_GLMBASICVECTORUTILS_H
