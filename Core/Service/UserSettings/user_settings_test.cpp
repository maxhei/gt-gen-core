/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/UserSettings/user_settings.h"

#include "Core/Service/Exception/exception.h"

#include <gmock/gmock-matchers.h>
#include <gtest/gtest.h>

namespace gtgen::core::service::user_settings
{

TEST(UserSettingsEnumsTest, GivenUserSettings_WhenLog_ThenFormatttedCorrectly)
{
    testing::internal::CaptureStdout();

    UserSettings user_settings;
    std::cout << fmt::format("UserSettings: {}", user_settings) << std::endl;

    std::string stdout = testing::internal::GetCapturedStdout();
    EXPECT_THAT(stdout, testing::HasSubstr("FileLogging"));
    EXPECT_THAT(stdout, testing::HasSubstr("GroundTruth"));
    EXPECT_THAT(stdout, testing::HasSubstr("HostVehicle"));
    EXPECT_THAT(stdout, testing::HasSubstr("Map"));
    EXPECT_THAT(stdout, testing::HasSubstr("MapChunking"));
    EXPECT_THAT(stdout, testing::HasSubstr("UserDirectories"));
}

}  // namespace gtgen::core::service::user_settings
