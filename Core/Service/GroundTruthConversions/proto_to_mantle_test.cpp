/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/GroundTruthConversions/proto_to_mantle.h"

#include <gtest/gtest.h>

namespace gtgen::core::service::gt_conversion
{
TEST(DimensionTest, GivenOsiDimension_WhenToDimension_ThenMantleDimensionReturned)
{
    osi3::Dimension3d input;
    input.set_length(1);
    input.set_width(2);
    input.set_height(3);

    auto output = ToDimension3(input);

    EXPECT_EQ(input.length(), output.length());
    EXPECT_EQ(input.width(), output.width());
    EXPECT_EQ(input.height(), output.height());
}

TEST(OrientationTest, GivenOsiOrientation_WhenToOrientation3_ThenMantleOrientationReturned)
{
    osi3::Orientation3d input;
    input.set_yaw(1);
    input.set_pitch(2);
    input.set_roll(3);

    auto output = ToOrientation3(input);

    EXPECT_EQ(input.yaw(), output.yaw());
    EXPECT_EQ(input.pitch(), output.pitch());
    EXPECT_EQ(input.roll(), output.roll());
}

TEST(VectorTest, GivenOsiVector3d_WhenToVec3Length_ThenMantleLengthVectorReturned)
{
    osi3::Vector3d input;
    input.set_x(1);
    input.set_y(2);
    input.set_z(3);

    auto output = ToVec3Length(input);

    EXPECT_EQ(input.x(), output.x());
    EXPECT_EQ(input.y(), output.y());
    EXPECT_EQ(input.z(), output.z());
}

TEST(VelocityTest, GivenOsiVector3d_WhenToVec3Velocity_ThenMantleVelocityVectorReturned)
{
    osi3::Vector3d input;
    input.set_x(1);
    input.set_y(2);
    input.set_z(3);

    auto output = ToVec3Velocity(input);

    EXPECT_EQ(input.x(), output.x());
    EXPECT_EQ(input.y(), output.y());
    EXPECT_EQ(input.z(), output.z());
}

TEST(AccelerationTest, GivenOsiVector3d_WhenToVec3Acceleration_ThenMantleAccelerationVectorReturned)
{
    osi3::Vector3d input;
    input.set_x(1);
    input.set_y(2);
    input.set_z(3);

    auto output = ToVec3Acceleration(input);

    EXPECT_EQ(input.x(), output.x());
    EXPECT_EQ(input.y(), output.y());
    EXPECT_EQ(input.z(), output.z());
}

TEST(TimeTest, GivenOsiTimestamp_WhenToTime_ThenMantleTimeReturned)
{
    osi3::Timestamp input;
    input.set_seconds(4);
    input.set_nanos(500000000);

    auto output = ToTime(input);

    EXPECT_DOUBLE_EQ(output.value(), 4500);
}

class IndicatorStateConverterTestFixture
    : public testing::TestWithParam<
          std::tuple<osi3::MovingObject_VehicleClassification_LightState_IndicatorState, mantle_api::IndicatorState>>
{
};

INSTANTIATE_TEST_SUITE_P(
    EnvironmentConverter,
    IndicatorStateConverterTestFixture,
    testing::ValuesIn(std::vector<std::tuple<osi3::MovingObject_VehicleClassification_LightState_IndicatorState,
                                             mantle_api::IndicatorState>>{
        std::make_tuple(osi3::MovingObject_VehicleClassification_LightState_IndicatorState_INDICATOR_STATE_UNKNOWN,
                        mantle_api::IndicatorState::kUnknown),
        std::make_tuple(osi3::MovingObject_VehicleClassification_LightState_IndicatorState_INDICATOR_STATE_OTHER,
                        mantle_api::IndicatorState::kOther),
        std::make_tuple(osi3::MovingObject_VehicleClassification_LightState_IndicatorState_INDICATOR_STATE_OFF,
                        mantle_api::IndicatorState::kOff),
        std::make_tuple(osi3::MovingObject_VehicleClassification_LightState_IndicatorState_INDICATOR_STATE_LEFT,
                        mantle_api::IndicatorState::kLeft),
        std::make_tuple(osi3::MovingObject_VehicleClassification_LightState_IndicatorState_INDICATOR_STATE_RIGHT,
                        mantle_api::IndicatorState::kRight),
        std::make_tuple(osi3::MovingObject_VehicleClassification_LightState_IndicatorState_INDICATOR_STATE_WARNING,
                        mantle_api::IndicatorState::kWarning)}));

TEST_P(IndicatorStateConverterTestFixture,
       GivenProtoIndicatorState_WhenConvertToMantleIndicatorState_ThenCorrectTypeReturned)
{
    osi3::MovingObject_VehicleClassification_LightState_IndicatorState proto_type = std::get<0>(GetParam());
    mantle_api::IndicatorState expected_mantle_type = std::get<1>(GetParam());

    auto actual_mantle_type = ToIndicatorState(proto_type);

    EXPECT_EQ(expected_mantle_type, actual_mantle_type);
}

}  // namespace gtgen::core::service::gt_conversion
