/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_GROUNDTRUTHCONVERSIONS_MANTLETOPROTO_H
#define GTGEN_CORE_SERVICE_GROUNDTRUTHCONVERSIONS_MANTLETOPROTO_H

#include "osi_common.pb.h"

#include <MantleAPI/Common/dimension.h>
#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Common/vector.h>

namespace gtgen::core::service::gt_conversion
{
template <typename ProtoObject>
inline void FillProtoObject(const mantle_api::Dimension3& dimension, ProtoObject* proto_dimension)
{
    proto_dimension->set_length(dimension.length());
    proto_dimension->set_width(dimension.width());
    proto_dimension->set_height(dimension.height());
}

template <typename ProtoObject, typename UnitsType>
inline void FillProtoObject(const mantle_api::Orientation3<UnitsType>& orientation, ProtoObject* proto_orientation)
{
    proto_orientation->set_yaw(orientation.yaw());
    proto_orientation->set_pitch(orientation.pitch());
    proto_orientation->set_roll(orientation.roll());
}

template <typename ProtoObject, typename UnitsType>
inline void FillProtoObject(const mantle_api::Vec3<UnitsType>& vector, ProtoObject* proto_vector)
{
    proto_vector->set_x(vector.x());
    proto_vector->set_y(vector.y());
    proto_vector->set_z(vector.z());
}

template <typename ProtoObject, typename MantleType>
inline ProtoObject CreateProtoObject(const MantleType& mantle_type)
{
    ProtoObject proto_object;
    FillProtoObject(mantle_type, &proto_object);
    return proto_object;
}

}  // namespace gtgen::core::service::gt_conversion

#endif  // GTGEN_CORE_SERVICE_GROUNDTRUTHCONVERSIONS_MANTLETOPROTO_H
