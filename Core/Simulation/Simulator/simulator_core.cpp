/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Simulation/Simulator/simulator_core.h"

#include "Core/Simulation/Simulator/simulator_core_impl.h"

namespace gtgen::core
{
namespace simulation
{
namespace simulator
{

SimulatorCore::SimulatorCore(std::unique_ptr<mantle_api::IScenarioEngine> scenario_engine,
                             std::shared_ptr<mantle_api::IEnvironment> environment,
                             SimulationParameters params,
                             const service::user_settings::UserSettings& user_settings)
    : impl_{std::make_unique<simulation::simulator::SimulatorCoreImpl>(std::move(scenario_engine),
                                                                       std::move(environment),
                                                                       std::move(params),
                                                                       user_settings)}
{
}

SimulatorCore::~SimulatorCore() = default;

void SimulatorCore::Init()
{
    impl_->Init();
}

std::shared_ptr<mantle_api::IEnvironment> SimulatorCore::GetEnvironment() const
{
    return impl_->GetEnvironment();
}

void SimulatorCore::Step()
{
    impl_->Step();
}

bool SimulatorCore::IsFinished() const
{
    return impl_->IsFinished();
}

void SimulatorCore::Shutdown()
{
    impl_->Shutdown();
}

std::chrono::milliseconds SimulatorCore::GetStepSize() const
{
    return impl_->GetStepSize();
}

double SimulatorCore::GetTimeScale() const
{
    return impl_->GetTimeScale();
}

std::chrono::milliseconds SimulatorCore::GetScenarioDuration() const
{
    return impl_->GetScenarioDuration();
}

const osi3::SensorView& SimulatorCore::GetSensorView() const
{
    return impl_->GetSensorView();
}

const std::vector<osi3::TrafficCommand>& SimulatorCore::GetTrafficCommands() const
{
    return impl_->GetTrafficCommands();
}

void SimulatorCore::SetTrafficUpdate(const osi3::TrafficUpdate& traffic_update)
{
    impl_->SetTrafficUpdate(traffic_update);
}

void SimulatorCore::SetDriverRelatedData(messages::ui::DriverRelatedData driver_related_data)
{
    impl_->SetDriverRelatedData(std::move(driver_related_data));
}

const IHostVehicleInterface& SimulatorCore::GetHostVehicleInterface() const
{
    return impl_->GetHostVehicleInterface();
}

void SimulatorCore::AddGuiMarker(double latitude, double longitude, const std::string& description, double radius)
{
    impl_->AddGuiMarker(latitude, longitude, description, radius);
}

std::uint16_t SimulatorCore::GetServerPort() const
{
    return impl_->GetServerPort();
}

}  // namespace simulator
}  // namespace simulation
}  // namespace gtgen::core
