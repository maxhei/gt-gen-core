/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SIMULATION_SIMULATOR_SIMULATORCORE_H
#define GTGEN_CORE_SIMULATION_SIMULATOR_SIMULATORCORE_H

#include "Core/Export/i_host_vehicle_interface.h"
#include "Core/Export/simulation_parameters.h"
#include "Core/Service/UserSettings/user_settings.h"
#include "driver_related_data.pb.h"
#include "osi_sensorview.pb.h"
#include "osi_trafficcommand.pb.h"
#include "osi_trafficupdate.pb.h"

#include <MantleAPI/Execution/i_environment.h>
#include <MantleAPI/Execution/i_scenario_engine.h>

#include <chrono>
#include <memory>

///
/// @attention Currently no C++17 features allowed in this file, only C++ 14 or older are allowed.
///
namespace gtgen::core
{
namespace simulation
{
namespace simulator
{

class SimulatorCoreImpl;

/// @brief GTGen SimulatorCore class
class SimulatorCore
{
  public:
    /// @brief Simulation constructor taking in the simulation parameters and the injected activity dependencies.
    /// Initialization of the logging is done here
    explicit SimulatorCore(std::unique_ptr<mantle_api::IScenarioEngine> scenario_engine,
                           std::shared_ptr<mantle_api::IEnvironment> environment,
                           SimulationParameters params,
                           const service::user_settings::UserSettings& user_settings);
    SimulatorCore(const SimulatorCore&) = delete;
    SimulatorCore(SimulatorCore&& rhs) = default;
    SimulatorCore& operator=(const SimulatorCore&) = delete;
    SimulatorCore& operator=(SimulatorCore&&) = delete;
    ~SimulatorCore();

    /// @brief Initializes the simulation
    void Init();

    /// @brief Triggers the next step of the simulation
    void Step();

    /// @brief Returns true, when the scenario has ended
    bool IsFinished() const;

    /// @brief Shutdowns the simulation and cleans up
    void Shutdown();

    /// @brief Returns the environment of simulator
    std::shared_ptr<mantle_api::IEnvironment> GetEnvironment() const;

    /// @brief Provides the configured step size after SimulatorCore::Init() has been called
    std::chrono::milliseconds GetStepSize() const;

    /// @brief Provides the configured time scale after SimulatorCore::Init() has been called
    double GetTimeScale() const;

    /// @brief Provides the scenario duration after SimulatorCore::Init() has been called
    std::chrono::milliseconds GetScenarioDuration() const;

    /// @brief Provides proto interfaces for osi sensor view
    const osi3::SensorView& GetSensorView() const;

    /// @brief Provides proto interfaces for osi traffic command
    const std::vector<osi3::TrafficCommand>& GetTrafficCommands() const;

    void SetTrafficUpdate(const osi3::TrafficUpdate& traffic_update);

    /// @brief Provides driver takeover ability proto interfaces
    void SetDriverRelatedData(messages::ui::DriverRelatedData driver_related_data);

    /// @brief Provides read access to internal host vehicle data
    /// @attention May only be called after SimulatorCore::Init(), otherwise behavior is undefined
    const IHostVehicleInterface& GetHostVehicleInterface() const;

    /// @brief Adds a position with a description to be displayed in GUI
    void AddGuiMarker(double latitude, double longitude, const std::string& description, double radius = 0.0);

    /// @brief Used by driver_related_data_test to connect test tcp client to the simulator's server
    /// @note Temporarily added. Functionality will be moved fully to Simulator level
    std::uint16_t GetServerPort() const;

  private:
    std::unique_ptr<simulation::simulator::SimulatorCoreImpl> impl_;
};

}  // namespace simulator
}  // namespace simulation
}  // namespace gtgen::core

#endif  // GTGEN_CORE_SIMULATION_SIMULATOR_SIMULATORCORE_H
