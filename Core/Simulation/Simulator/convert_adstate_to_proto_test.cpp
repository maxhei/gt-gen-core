/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Simulation/Simulator/convert_adstate_to_proto.h"

#include "ui_data.pb.h"

#include <MantleAPI/Traffic/entity_properties.h>
#include <gtest/gtest.h>

namespace gtgen::core::simulation::simulator
{

class ExternalControlStateToAdStateFixture
    : public testing::TestWithParam<std::tuple<mantle_api::ExternalControlState, messages::ui::AdState>>
{
};

INSTANTIATE_TEST_SUITE_P(
    ExternalControlStatesToAdStates,
    ExternalControlStateToAdStateFixture,
    testing::Values(
        std::tuple<mantle_api::ExternalControlState, messages::ui::AdState>{mantle_api::ExternalControlState::kOff,
                                                                            messages::ui::AdState::AD_STATE_OFF},
        std::tuple<mantle_api::ExternalControlState, messages::ui::AdState>{mantle_api::ExternalControlState::kFull,
                                                                            messages::ui::AdState::AD_STATE_ON},
        std::tuple<mantle_api::ExternalControlState, messages::ui::AdState>{
            mantle_api::ExternalControlState::kLateralOnly,
            messages::ui::AdState::AD_STATE_ON},
        std::tuple<mantle_api::ExternalControlState, messages::ui::AdState>{
            mantle_api::ExternalControlState::kLongitudinalOnly,
            messages::ui::AdState::AD_STATE_ON}));

TEST_P(ExternalControlStateToAdStateFixture, GivenExternalControlState_WhenConverting_ThenConvertedMappingIsCorrect)
{
    const auto input_state = std::get<0>(GetParam());
    const auto expected_converted_state = std::get<1>(GetParam());

    EXPECT_EQ(expected_converted_state, ConvertExternalControlStateToProto(input_state));
}

}  // namespace gtgen::core::simulation::simulator
