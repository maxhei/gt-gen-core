/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SIMULATION_SIMULATOR_SIMULATIONCONTROLLERIMPL_H
#define GTGEN_CORE_SIMULATION_SIMULATOR_SIMULATIONCONTROLLERIMPL_H

#include <atomic>
#include <functional>
#include <memory>

namespace gtgen::core::communication
{
class AsyncServer;
}

namespace gtgen::core::simulation::simulation_controller
{

/// @copydoc gtgen::core::SimulationController
class SimulationControllerImpl
{
  public:
    SimulationControllerImpl();
    SimulationControllerImpl(const SimulationControllerImpl&) = delete;
    SimulationControllerImpl(SimulationControllerImpl&& rhs) = delete;
    SimulationControllerImpl& operator=(const SimulationControllerImpl&) = delete;
    SimulationControllerImpl& operator=(SimulationControllerImpl&&) = delete;
    ~SimulationControllerImpl();

    /// @brief Launches async server and allows a connected client to pause/step/resume simulation
    /// @param console_log_level  The console log level as string (as passed in via the Simulation-Parameters)
    void StartControlServer(const std::string& console_log_level);

    /// copydoc SimulationController::ExecuteStep()
    void ExecuteStep();

    /// @brief Executes SimulationStepCallback no matter if the simulation was paused (used e.g. for client's step once)
    void StepImmediately();

    /// @brief Delays any ExecuteStep() call until simulation is resumed
    void PauseSimulation();

    /// @brief Unblocks any pending ExecuteStep() call and allows further uninterrupted simulation execution
    void ResumeSimulation();

    /// @brief Executes SimulationAbortCallback. Any subsequent calls to ExecuteStep will return with no effect
    void AbortSimulation();

    /// copydoc SimulationController::SetSimulationAbortCallback
    void SetSimulationAbortCallback(std::function<void()> func);

    /// copydoc SimulationController::SetSimulationStepCallback
    void SetSimulationStepCallback(std::function<void()> func);

    /// copydoc SimulationController::SetSleepFunction
    void SetSleepFunction(const std::function<void(const std::function<bool()>& condition)>& sleep_function);

    /// @brief Stops the async server, any subsequent calls to ExecuteStep will return with no effect
    void ShutDown();

    /// @return The port on which the launched async server listens for simulation control clients, or 0 when
    /// the server wasn't started yet / has been shut down
    std::uint16_t GetServerPort() const;

  private:
    std::unique_ptr<communication::AsyncServer> async_server_;

    std::atomic_bool paused_{false};
    std::atomic_bool aborted_{false};

    std::function<void()> step_function_{nullptr};
    std::function<void()> abort_callback_{nullptr};
    std::function<void(const std::function<bool()>& condition)> sleep_function_{nullptr};
};

}  // namespace gtgen::core::simulation::simulation_controller

#endif  // GTGEN_CORE_SIMULATION_SIMULATOR_SIMULATIONCONTROLLERIMPL_H
