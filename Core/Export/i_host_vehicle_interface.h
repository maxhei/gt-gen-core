/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_EXPORT_IHOSTVEHICLEINTERFACE_H
#define GTGEN_CORE_EXPORT_IHOSTVEHICLEINTERFACE_H

#include <cstdint>
#include <vector>

///
namespace gtgen::core
{

/// @brief Provides access to internal host vehicle related data
class IHostVehicleInterface
{
  public:
    IHostVehicleInterface() = default;
    IHostVehicleInterface(const IHostVehicleInterface& rhs) = delete;
    IHostVehicleInterface(IHostVehicleInterface&& rhs) = delete;
    IHostVehicleInterface& operator=(const IHostVehicleInterface&) = delete;
    IHostVehicleInterface& operator=(IHostVehicleInterface&&) = delete;
    virtual ~IHostVehicleInterface() = default;

    /// @brief Whether the host vehicle left the road and has been recovered to the last valid drivable lane
    /// in the last completed Step
    virtual bool HostPoseRecoveredInLastStep() const = 0;
};

}  // namespace gtgen::core

#endif  // GTGEN_CORE_EXPORT_IHOSTVEHICLEINTERFACE_H
